package com.smtc.uniconz;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

public class SingletonActivity extends Application {

    public static  SingletonActivity  _sInstane=null;
    Context context;


    public static String otp,candidatenamestr,emailstr,mobilestr,skillstr,industry_type_Id,designationstr,totalexpstr,location_autocomplete;


    private RequestQueue mRequestQueue;
    public static final String TAG = SingletonActivity.class
            .getSimpleName();
    public static String academicid,skillid;
    public static int acadqualifpos;

    public static String certificationid;
    public static int profcoursepos;

    public static int keyskillpos;
    public static String candidateskillid;

    public static int credentialpos;
    public static String credentialid;

    public static int profsummarypos;
    public static String experienceid;

    public static String salaryid;
    public static String bankid;
    public static String secondlevelchildid;
    public static String certificateuploadurl;

    public static String attachmentstr;
    public static String upload_condition_str;

    public static String jobrequirementid,titlename;

    public static String job_requirement_id;

    public static boolean fromalljobs = false;
    public static boolean fromviewalljob = false;

    public static boolean  fromrecommendedjobs = false;
    public static boolean  fromreferjobs = false;
    public static boolean  fromnewjobs = false;
    public static boolean  fromappliedjobs = false;


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        _sInstane=this;

    }
    public static SingletonActivity getInstance() {

        if(_sInstane==null)
        {
        }
        return _sInstane;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);


      /*  ACRA.init(this);
        Mint.initAndStartSession(this, "9e163101");
*/
    }

}
