package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/12/2018.
 */

public class NewReferralActivity extends AppCompatActivity  implements AdapterView.OnItemClickListener{

    Typeface bebas_font,roboto_light_font;
    TextView referraldetailstxtvw;
    EditText nameedt,emailedt,mobileedt,skillsedt,designationedt,totalexpedt,rolesandresponsibilityedt;
    AutoCompleteTextView location_autocomplete;
    MaterialBetterSpinner industry_spinner;
    RelativeLayout submitrelative;
    ImageView backiv;
    CoordinatorLayout coordinatorLayout;
    String locationstr,industry_spinner_str;
    private static final String LOG_TAG = "Uniconz";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";
    String[] IndustryList = {"IT-SOFTWARE","IT-HARDWARE","NETWORKING","TELECOM","BFSI-BANKING & FINANCE","E-COMMERCE","RESEARCH & ANALYTICS","OIL & GAS","ENERGY","REAL ESTATE","RECRUITMENT","SEMICONDUCTOR & ELECTRONICS","OTHERS"};
    String industry_type_Id,candidateidstr;
    ProgressDialog pdia;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_referral_coordinator);

        referraldetailstxtvw = (TextView)findViewById(R.id.referraldetailstxtvw);
        nameedt = (EditText) findViewById(R.id.nameedt);
        emailedt = (EditText) findViewById(R.id.emailedt);
        mobileedt = (EditText) findViewById(R.id.mobileedt);
        skillsedt = (EditText) findViewById(R.id.skillsedt);
        designationedt = (EditText) findViewById(R.id.designationedt);
        totalexpedt = (EditText) findViewById(R.id.totalexpedt);
        rolesandresponsibilityedt = (EditText) findViewById(R.id.rolesandresponsibilityedt);
        location_autocomplete = (AutoCompleteTextView) findViewById(R.id.location_autocomplete);
        industry_spinner = (MaterialBetterSpinner) findViewById(R.id.industry_spinner);
        submitrelative = (RelativeLayout)findViewById(R.id.submitrelative);
        backiv = (ImageView)findViewById(R.id.backiv);

        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");
        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        referraldetailstxtvw.setTypeface(bebas_font);
        nameedt.setTypeface(roboto_light_font);
        emailedt.setTypeface(roboto_light_font);
        mobileedt.setTypeface(roboto_light_font);
        skillsedt.setTypeface(roboto_light_font);
        designationedt.setTypeface(roboto_light_font);
        totalexpedt.setTypeface(roboto_light_font);
        rolesandresponsibilityedt.setTypeface(roboto_light_font);
        location_autocomplete.setTypeface(roboto_light_font);
        industry_spinner.setTypeface(roboto_light_font);

        location_autocomplete.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete.setOnItemClickListener(NewReferralActivity.this);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        ArrayAdapter<String> industryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, IndustryList);
        industry_spinner = (MaterialBetterSpinner)
                findViewById(R.id.industry_spinner);
        industry_spinner.setAdapter(industryAdapter);


        industry_spinner.setTypeface(roboto_light_font);

        industry_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                industry_spinner_str = parent.getItemAtPosition(position).toString();

                if(position == 0) {
                    industry_type_Id = "0";
                }

                if(position == 1) {
                    industry_type_Id = "1";
                }

                if(position == 2) {
                    industry_type_Id = "2";
                }

                if(position == 3) {
                    industry_type_Id = "3";
                }

                if(position == 4) {
                    industry_type_Id = "4";
                }

                if(position == 5) {
                    industry_type_Id = "5";
                }

                if(position == 6) {
                    industry_type_Id = "6";
                }

                if(position == 7) {
                    industry_type_Id = "7";
                }

                if(position == 8) {
                    industry_type_Id = "8";
                }

                if(position == 9) {
                    industry_type_Id = "9";
                }

                if(position == 10) {
                    industry_type_Id = "10";
                }

                if(position == 11) {
                    industry_type_Id = "11";
                }

                if(position == 12) {
                    industry_type_Id = "12";
                }

            }
        });




        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(NewReferralActivity.this,ReferralActivity.class);
                startActivity(i);


            }
        });

        submitrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean invalid = false;

                if (nameedt.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Candidate Name", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(!isValidEmail(emailedt.getText().toString()))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                else if(!isValidMobileNumber(mobileedt.getText().toString()))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile No.", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(skillsedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Skills", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                else if(designationedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Designation", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(totalexpedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Total Years of Experience", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(location_autocomplete.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Location", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(industry_spinner.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Industry", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {

                    if(NetworkUtility.checkConnectivity(NewReferralActivity.this)){
                        String addReferralURL = APIName.URL+"/candidate/addReferral";
                        System.out.println("ADD REFERRAL URL IS---"+ addReferralURL);
                        addReferralAPI(addReferralURL,nameedt.getText().toString(),emailedt.getText().toString(),mobileedt.getText().toString(),industry_type_Id,designationedt.getText().toString(),totalexpedt.getText().toString(),skillsedt.getText().toString(),rolesandresponsibilityedt.getText().toString(),location_autocomplete.getText().toString());

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }
                }

            }
        });

    }

    private void addReferralAPI(String url,final String candidatenamestr,final String emailstr,final String mobilestr,final String industry_type_Id_str,final String designationstr,final String totalexpstr,final String skillstr,final String rolesandresponsibility,final String locationstr){

        pdia = new ProgressDialog(NewReferralActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF ADD REFERRAL API IS---" + response);

                        JSONObject addreferraljson = null;
                        try {
                            addreferraljson = new JSONObject(response);

                            System.out.println("ADD REFERRAL JSON IS---" + addreferraljson);

                            String statusstr = addreferraljson.getString("status");
                            String msgstr = addreferraljson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(NewReferralActivity.this, ReferralActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_name",candidatenamestr);
                params.put("email",emailstr);
                params.put("mob_number",mobilestr);
                params.put("reffered_Id",candidateidstr);
                params.put("industry_type",industry_type_Id_str);
                params.put("designation",designationstr);
                params.put("total_experience",totalexpstr);
                params.put("ref_candidate_skill",skillstr);
                params.put("message",rolesandresponsibility);
                params.put("location",locationstr);


                System.out.println("PARAMS OF ADD REFERRAL API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(NewReferralActivity.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(NewReferralActivity.this,ReferralActivity.class);
        startActivity(i);
    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        locationstr = (String) adapterView.getItemAtPosition(position);
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            // sb.append("&components=&types=(cities)");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }



}
