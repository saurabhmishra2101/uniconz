package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import android.provider.OpenableColumns;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ApplyJobActivity extends AppCompatActivity  implements AdapterView.OnItemClickListener{

    AutoCompleteTextView location_autocomplete;
    Typeface roboto_light_font;
    private static final String LOG_TAG = "Uniconz";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";
    String locationstr;
    TextView uploadresumetxt,headertitle,uploadeddocumenttxt;
    String attachmentstr,type,upload_condition_str;
    RelativeLayout saverelative;
    EditText currentctcedt,expectedctcedt,noticeperiodedt;
    private CoordinatorLayout coordinatorLayout;
    String currentDateTimeString,candidateidstr,candidatenamestr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    ProgressDialog pdia;
    ImageView backiv,docimg;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_job_coordinator);

        location_autocomplete = (AutoCompleteTextView)findViewById(R.id.location_autocomplete);
        uploadresumetxt = (TextView)findViewById(R.id.uploadresumetxt);
        currentctcedt = (EditText)findViewById(R.id.currentctcedt);
        expectedctcedt = (EditText)findViewById(R.id.expectedctcedt);
        noticeperiodedt = (EditText)findViewById(R.id.noticeperiodedt);
        saverelative = (RelativeLayout)findViewById(R.id.saverelative);
        headertitle = (TextView)findViewById(R.id.headertitle);
        docimg = (ImageView) findViewById(R.id.docimg);
        uploadeddocumenttxt = (TextView)findViewById(R.id.uploadeddocumenttxt);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        backiv = (ImageView)findViewById(R.id.backiv);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                /*Intent i = new Intent(ApplyJobActivity.this,AllJobsActivity.class);
                startActivity(i);*/
            }
        });

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        location_autocomplete.setTypeface(roboto_light_font);
        uploadresumetxt.setTypeface(roboto_light_font);
        currentctcedt.setTypeface(roboto_light_font);
        expectedctcedt.setTypeface(roboto_light_font);
        noticeperiodedt.setTypeface(roboto_light_font);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);
        candidatenamestr = prefs.getString("name", null);

             int job_requirement_id = Integer.parseInt(SingletonActivity.job_requirement_id);
                            int new_job_requirement_id = 100 + job_requirement_id;

                            String new_job_requirement_str = "JOB"+Integer.toString(new_job_requirement_id);


        headertitle.setText(new_job_requirement_str);

        location_autocomplete.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete.setOnItemClickListener(ApplyJobActivity.this);
        
        uploadresumetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser(); 
            }
        });

        if (NetworkUtility.checkConnectivity(ApplyJobActivity.this)) {
            String candidateResumeURL = APIName.URL + "/candidate/candidateResume?candidate_Id=" + candidateidstr;
            System.out.println("CANDIDATE RESUME URL IS---" + candidateResumeURL);
            candidateResumeAPI(candidateResumeURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }



        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                if (currentctcedt.getText().toString().equals("")) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Current CTC(In LPA)", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                else  if (expectedctcedt.getText().toString().equals("")) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Expected CTC(In LPA)", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();


                }
                else  if (noticeperiodedt.getText().toString().equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Notice Period (In days)", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

            

                else  if (location_autocomplete.getText().toString().equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Location", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

              


                else if (invalid == false) {

                     if (NetworkUtility.checkConnectivity(ApplyJobActivity.this)) {
                                String ApplyJobURL = APIName.URL + "/candidate/applyjob?candidate_Id=" + candidateidstr;
                                System.out.println("APPLY JOB URL IS---" + ApplyJobURL);
                                ApplyJobAPI(ApplyJobURL,SingletonActivity.job_requirement_id,currentctcedt.getText().toString(),expectedctcedt.getText().toString(),noticeperiodedt.getText().toString(),location_autocomplete.getText().toString());

                            } else {


                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }




                }
      

    }
        });
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        finish();
    }

    private void candidateResumeAPI(String url) {

        pdia = new ProgressDialog(ApplyJobActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF CANDIDATE RESUME API IS---" + response);

                        JSONObject candidateresumejson = null;
                        try {
                            candidateresumejson = new JSONObject(response);

                            final JSONObject candidateresumeJSONObject = candidateresumejson.getJSONObject("candidateResume");

                                if(!candidateresumeJSONObject.getString("cv_upload").equalsIgnoreCase(""))
                                {
                                    docimg.setVisibility(View.VISIBLE);
                                    uploadeddocumenttxt.setVisibility(View.VISIBLE);

                                    String image_url = candidateresumeJSONObject.getString("cv_upload");
                                    String final_image_url = APIName.IMAGE_URL + image_url;

                                    Uri uri = Uri.parse(final_image_url);
                                    String token = uri.getLastPathSegment();

                                    System.out.println("token IS---" + token);

                                    uploadresumetxt.setText(token);


                                    docimg.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Intent i = new Intent(ApplyJobActivity.this,WebViewActivity.class);
                                            try {
                                                SingletonActivity.certificateuploadurl = candidateresumeJSONObject.getString("cv_upload");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            startActivity(i);

                                        }
                                    });

                                }
                                else
                                {
                                    uploadeddocumenttxt.setVisibility(View.GONE);
                                    docimg.setVisibility(View.GONE);
                                }



                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ApplyJobActivity.this);
        requestQueue.add(stringRequest);
    }



    private void ApplyJobAPI(String url, final String job_requirement_id, final String currentctcstr, final String expectedctcstr, final String noticeperiodstr, final String locationstr){

        pdia = new ProgressDialog(ApplyJobActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF APPLY JOB API IS---" + response);

                        JSONObject applyjobjson = null;
                        try {
                            applyjobjson = new JSONObject(response);

                            System.out.println("APPLY JOB JSON IS---" + applyjobjson);

                            String statusstr = applyjobjson.getString("status");
                            String msgstr = applyjobjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();

                                Intent i = new Intent(ApplyJobActivity.this,AllJobsActivity.class);
                                startActivity(i);


                              
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();



                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_Id",candidateidstr);
                params.put("job_requirement_Id",job_requirement_id);
                params.put("modified",currentDateTimeString);

                if(attachmentstr==null)
                {
                    attachmentstr = "0";
                    params.put("attachment",attachmentstr);
                    //  params.put("extension","");
                }
                else
                {
                    params.put("attachment",attachmentstr);
                    //   params.put("extension",type);
                }


                if(attachmentstr==null)
                {
                    upload_condition_str = "0";
                }
                else
                {
                    upload_condition_str = "1";
                }

                params.put("upload_Condition",upload_condition_str);
                params.put("current_ctc",currentctcstr);
                params.put("expected_ctc",expectedctcstr);
                params.put("notice_period",noticeperiodstr);
                params.put("location",locationstr);
                params.put("candidate_name",candidatenamestr);


               System.out.println("PARAMS OF ApplyJob API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ApplyJobActivity.this);
        requestQueue.add(stringRequest);
    }



    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");



        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();
                    System.out.println("SELECTED FILE PATH IS--"+ filePath);

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;



                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);

                        if(type.equals("pdf")||type.equals("doc")||type.equals("docx"))
                        {
                            attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                            System.out.println("BASE 64 STRING------"+ attachmentstr);

                            if (uriString.startsWith("content://")) {
                                Cursor cursor = null;
                                try {
                                    cursor = ApplyJobActivity.this.getContentResolver().query(uri, null, null, null, null);
                                    if (cursor != null && cursor.moveToFirst()) {
                                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        System.out.println("displayName 1 is------"+ displayName);
                                        uploadresumetxt.setText(displayName);
                                    }
                                } finally {
                                    cursor.close();
                                }
                            } else if (uriString.startsWith("file://")) {
                                displayName = myFile.getName();
                                System.out.println("displayName 2 is------"+ displayName);
                                uploadresumetxt.setText(displayName);
                            }
                        }
                        else {

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Please upload the resume in following formats .pdf,.doc,.docx only", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }



                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        locationstr = (String) adapterView.getItemAtPosition(position);
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            // sb.append("&components=&types=(cities)");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
