package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.smtc.uniconz.R.id.nootherpreferencetxt;
import static com.smtc.uniconz.R.id.otherpreferencerel;

/**
 * Created by 10161 on 3/30/2018.
 */

public class EditOtherPreferenceActivity extends AppCompatActivity {

    ImageView backiv;
    CheckBox checkBox1,checkBox2,checkBox3,checkBox4,checkBox5,checkBox6,checkBox7;
    RelativeLayout saverelative;
    CoordinatorLayout coordinatorLayout;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr;
    ProgressDialog pdia;
    ArrayList<String> additional_detail_Id_list = new ArrayList<String>();
    ArrayList<String> isAgreed_list = new ArrayList<String>();
    String statusstrs;
    String isAgreed_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_other_preference_coordinator);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);


        backiv = (ImageView)findViewById(R.id.backiv);
        checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        checkBox3 = (CheckBox) findViewById(R.id.checkBox3);
        checkBox4 = (CheckBox) findViewById(R.id.checkBox4);
        checkBox5 = (CheckBox) findViewById(R.id.checkBox5);
        checkBox6 = (CheckBox) findViewById(R.id.checkBox6);
        checkBox7 = (CheckBox) findViewById(R.id.checkBox7);

        saverelative = (RelativeLayout) findViewById(R.id.saverelative);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditOtherPreferenceActivity.this,CandidateDetailsActivity.class);
                startActivity(i);
            }
        });

        if (NetworkUtility.checkConnectivity(EditOtherPreferenceActivity.this)) {
            String GetCandAddDetailURL = APIName.URL + "/candidate/getCandAddDetail?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND ADD DETAIL URL IS---" + GetCandAddDetailURL);
            GetCandAddDetailAPI(GetCandAddDetailURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }


        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkBox1.isChecked() || checkBox2.isChecked() || checkBox3.isChecked() || checkBox4.isChecked()
                        || checkBox5.isChecked() || checkBox6.isChecked() ||  checkBox7.isChecked()) {

                    isAgreed_str = "";

                    if (checkBox1.isChecked()) {
                        isAgreed_str = "1,";
                    }else{
                        if (isAgreed_str.equalsIgnoreCase(""))
                            isAgreed_str = ",";
                        else
                            isAgreed_str = isAgreed_str+",";
                    }
                    if (checkBox2.isChecked()){
                        isAgreed_str = isAgreed_str+"2,";
                    }else{
                        isAgreed_str = isAgreed_str+",";
                    }
                    if (checkBox3.isChecked()) {
                        isAgreed_str = isAgreed_str + "3,";
                    }else{
                        isAgreed_str = isAgreed_str+",";
                    }
                    if (checkBox4.isChecked()){
                        isAgreed_str = isAgreed_str+"4,";
                    }else{
                        isAgreed_str = isAgreed_str +",";
                    }
                    if (checkBox5.isChecked()) {
                        isAgreed_str = isAgreed_str +"5,";
                    }else{
                        isAgreed_str = isAgreed_str +",";
                    }
                    if (checkBox6.isChecked()){
                        isAgreed_str = isAgreed_str +"6,";
                    }else{
                        isAgreed_str = isAgreed_str +",";
                    }
                    if (checkBox7.isChecked()){
                        isAgreed_str = isAgreed_str +"7";
                    }



                    if(statusstrs.equalsIgnoreCase("Insert")) {

                        if (NetworkUtility.checkConnectivity(EditOtherPreferenceActivity.this)) {
                            String insertUpdateAdditionalDetailsURL = APIName.URL + "/candidate/insertUpdateAdditionalDetail?otherstatus=" + statusstrs;
                            System.out.println("INSERT UPDATE ADDITIONAL DETAILS URL IS---" + insertUpdateAdditionalDetailsURL);
                            insertUpdateAdditionalDetailsAPI(insertUpdateAdditionalDetailsURL, isAgreed_str);

                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                    else
                    {
                        if (NetworkUtility.checkConnectivity(EditOtherPreferenceActivity.this)) {
                            String insertUpdateAdditionalDetailsURL = APIName.URL + "/candidate/insertUpdateAdditionalDetail?otherstatus=" + statusstrs + "&candidate_Id="+candidateidstr;
                            System.out.println("INSERT UPDATE ADDITIONAL DETAILS URL IS---" + insertUpdateAdditionalDetailsURL);
                            insertUpdateAdditionalDetailsAPI(insertUpdateAdditionalDetailsURL, isAgreed_str);

                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select atleast One Preference!!!", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });





    }


    @Override
    public void onBackPressed() {

        Intent i = new Intent(EditOtherPreferenceActivity.this,CandidateDetailsActivity.class);
        startActivity(i);
    }

    private void insertUpdateAdditionalDetailsAPI(String url, final String isAgreed_str){
        pdia = new ProgressDialog(EditOtherPreferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE ADDITIONAL DETAILS API IS---" + response);

                        JSONObject insertupdatesalarydetailsjson = null;
                        try {
                            insertupdatesalarydetailsjson = new JSONObject(response);

                            System.out.println("INSERT UPDATE ADDITIONAL DETAILS JSON IS---" + insertupdatesalarydetailsjson);

                            String statusstr = insertupdatesalarydetailsjson.getString("status");
                            String msgstr = insertupdatesalarydetailsjson.getString("message");

                            if(statusstr.equalsIgnoreCase("1"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditOtherPreferenceActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("additional_detail_Id", ",,,,,,");
                params.put("isAgreed",isAgreed_str);
                params.put("candidate_Id",candidateidstr);


                System.out.println("PARAMS OF insertUpdateAdditionalDetailsAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditOtherPreferenceActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetCandAddDetailAPI(String url) {

        pdia = new ProgressDialog(EditOtherPreferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                       additional_detail_Id_list.clear();
                       isAgreed_list.clear();


                        System.out.println("RESPONSE OF GET CAND ADD DETAILS API IS---" + response);

                        JSONObject candadddetailsjson = null;

                        try {
                            candadddetailsjson = new JSONObject(response);


                            System.out.println("LENGTH=====" + candadddetailsjson.getString("CandAdditional").length());
                            if (candadddetailsjson.getString("CandAdditional").length() == 4)
                            //your rest of codes
                            {

                                statusstrs = "Insert";


                            } else {

                                statusstrs = "Update";

                                JSONArray CandAdditional = candadddetailsjson.getJSONArray("CandAdditional");
                                System.out.println("JSONARRAY CandAdditional=====" + CandAdditional);

                                for (int i = 0; i < CandAdditional.length(); i++) {


                                    additional_detail_Id_list.add(CandAdditional.getJSONObject(i).getString("additional_detail_Id"));
                                    isAgreed_list.add(CandAdditional.getJSONObject(i).getString("isAgreed"));





                                    if (isAgreed_list.contains("1")) {
                                       checkBox1.setChecked(true);
                                    }

                                    if (isAgreed_list.contains("2")) {

                                        checkBox2.setChecked(true);


                                    }

                                    if (isAgreed_list.contains("3")) {

                                        checkBox3.setChecked(true);


                                    }

                                    if (isAgreed_list.contains("4")) {

                                        checkBox4.setChecked(true);


                                    }

                                    if (isAgreed_list.contains("5")) {

                                        checkBox5.setChecked(true);


                                    }

                                    if (isAgreed_list.contains("6")) {

                                        checkBox6.setChecked(true);
                                    }

                                    if (isAgreed_list.contains("7")) {

                                        checkBox7.setChecked(true);


                                    }



                                }





                            }
                            } catch (JSONException e) {
                            e.printStackTrace();
                        }







                     /*   JSONObject candidatesaldetailsjson = null;
                        try {
                            candidatesaldetailsjson = new JSONObject(response);

                            JSONObject CandidateSalaryDetailsJSON = candidatesaldetailsjson.getJSONObject("CandidateBankDetails");


                            nameedt.setText(CandidateSalaryDetailsJSON.getString("name"));
                            accountnoedt.setText(CandidateSalaryDetailsJSON.getString("account_number"));
                            ifsccodeedt.setText(CandidateSalaryDetailsJSON.getString("ifsc_code"));


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
*/


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        statusstrs = "Insert";
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditOtherPreferenceActivity.this);
        requestQueue.add(stringRequest);
    }

  /*  @Override
    public void onBackPressed() {
        Intent i = new Intent(EditOtherPreferenceActivity.this,CandidateDetailsActivity.class);
        startActivity(i);
    }*/
}
