package com.smtc.uniconz;


import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by 10161 on 4/10/2018.
 */


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;

import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/4/2018.
 */

public class RecommendedJobsActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr,currentDateTimeString;
    ProgressDialog pdia;
    ArrayList<String> title_list = new ArrayList<String>();
    ArrayList<String> hirer_name_list = new ArrayList<String>();
    ArrayList<String> location_list = new ArrayList<String>();
    ArrayList<String> notice_period_list = new ArrayList<String>();
    ArrayList<String> status_list = new ArrayList<String>();
    ArrayList<String> job_requirement_id_list = new ArrayList<String>();
    ArrayList<String> job_applied_status_list = new ArrayList<String>();
    CustomAdap customadap;
    ListView recommendedjobslistvw;
    TextView recommendedjobtxtvw;
    Typeface bebas_font,sf_font;
    ImageView backiv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommended_jobs_coordinator);



        backiv = (ImageView)findViewById(R.id.backiv);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RecommendedJobsActivity.this,DashboardActivity.class);
                startActivity(i);
            }
        });

        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");
        sf_font = Typeface.createFromAsset(getAssets(), "sfuidisplay/SF.ttf");

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        recommendedjobslistvw = (ListView)findViewById(R.id.recommendedjobslistvw);

        recommendedjobtxtvw = (TextView)findViewById(R.id.recommendedjobtxtvw);
        recommendedjobtxtvw.setTypeface(bebas_font);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);


         Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());

        if (NetworkUtility.checkConnectivity(RecommendedJobsActivity.this)) {
            String CandidateRecommendedJobsURL = APIName.URL + "/candidate/candidateRecommandedJobs?candidate_Id=" + candidateidstr;
            System.out.println("CANDIDATE RECOMMENDED JOBS URL IS---" + CandidateRecommendedJobsURL);
            CandidateRecommendedJobsAPI(CandidateRecommendedJobsURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(RecommendedJobsActivity.this,DashboardActivity.class);
        startActivity(i);

    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        ArrayList<String> title_lists;
        ArrayList<String> hirer_name_lists;
        ArrayList<String> location_lists;
        ArrayList<String> notice_period_lists;
        ArrayList<String> status_lists;
        ArrayList<String> job_requirement_id_lists;
        ArrayList<String> job_applied_status_lists;

        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,ArrayList<String> job_applied_status_list,ArrayList<String> job_requirement_id_list,ArrayList<String> status_list,ArrayList<String> title_list,ArrayList<String> hirer_name_list,ArrayList<String> location_list,ArrayList<String> notice_period_list)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.title_lists = title_list;
            this.hirer_name_lists = hirer_name_list;
            this.location_lists = location_list;
            this.notice_period_lists = notice_period_list;
            this.status_lists = status_list;
            this.job_requirement_id_lists = job_requirement_id_list;
            this.job_applied_status_lists = job_applied_status_list;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            // return 7;
            return title_lists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            // final Holder holder = new Holder();
            Holder holder ;


            View rowView = null;




            if(rowView==null){
               /* LayoutInflater inflater =(LayoutInflater)
                        ((Activity)context).getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
                rowView = inflater.inflate(R.layout.recommended_jobs_row, null);

                holder = new Holder();


                holder.positiontxt = (TextView)rowView.findViewById(R.id.positiontxt);
                holder.companytxt = (TextView)rowView.findViewById(R.id.companytxt);
                holder.locationdesctxt = (TextView)rowView.findViewById(R.id.locationdesctxt);
                holder.noticeperioddesctxt = (TextView) rowView.findViewById(R.id.noticeperioddesctxt);
                holder.locationtitletxt = (TextView) rowView.findViewById(R.id.locationtitletxt);
                holder.noticeperiodtitletxt = (TextView) rowView.findViewById(R.id.noticeperiodtitletxt);
                holder.jobidtitletxt = (TextView) rowView.findViewById(R.id.jobidtitletxt);
                holder.jobiddesctxt = (TextView) rowView.findViewById(R.id.jobiddesctxt);
                holder.viewtxt = (TextView) rowView.findViewById(R.id.viewtxt);
                holder.applytxt = (TextView) rowView.findViewById(R.id.applytxt);
                holder.refertxt = (TextView) rowView.findViewById(R.id.refertxt);


                holder.positiontxt.setTypeface(sf_font);
                holder.companytxt.setTypeface(sf_font);
                holder.locationdesctxt.setTypeface(sf_font);
                holder.noticeperioddesctxt.setTypeface(sf_font);
                holder.locationtitletxt.setTypeface(sf_font);
                holder.noticeperiodtitletxt.setTypeface(sf_font);
                holder.jobidtitletxt.setTypeface(sf_font);
                holder.jobiddesctxt.setTypeface(sf_font);
                //   holder.viewtxt.setTypeface(sf_font);
                //   holder.applytxt.setTypeface(sf_font);
                //   holder.refertxt.setTypeface(sf_font);

                holder.positiontxt.setText(title_lists.get(position));
                holder.companytxt.setText(hirer_name_lists.get(position));
                holder.locationdesctxt.setText(location_lists.get(position));

                if(job_applied_status_lists.get(position).equalsIgnoreCase("Applied"))
                {
                    holder.applytxt.setText("APPLIED");
                    holder.applytxt.setEnabled(false);
                    holder.applytxt.setTextColor(Color.parseColor("#E4E4E4"));
                }
                else
                {
                    holder.applytxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String job_requirement_id = job_requirement_id_lists.get(position);

                            if (NetworkUtility.checkConnectivity(RecommendedJobsActivity.this)) {
                                String ApplyJobURL = APIName.URL + "/candidate/applyjob?candidate_Id=" + candidateidstr;
                                System.out.println("APPLY JOB URL IS---" + ApplyJobURL);
                                ApplyJobAPI(ApplyJobURL,job_requirement_id);

                            } else {


                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                    });

                }
                if(!notice_period_lists.get(position).equalsIgnoreCase("")) {
                    holder.noticeperioddesctxt.setText(notice_period_lists.get(position) + " days");
                }

              //  System.out.println("STATUS LIST==="+ status_lists);


                    int job_requirement_id = Integer.parseInt(job_requirement_id_lists.get(position));
                    int new_job_requirement_id = 100 + job_requirement_id;

                    String new_job_requirement_str = "JOB"+Integer.toString(new_job_requirement_id);
                    holder.jobiddesctxt.setText(new_job_requirement_str);


                holder.viewtxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SingletonActivity.fromalljobs = false;
                        SingletonActivity.fromrecommendedjobs = true;
                        SingletonActivity.fromreferjobs = false;


                        Intent i = new Intent(RecommendedJobsActivity.this,ViewAllJobDetailsActivity.class);

                        SingletonActivity.jobrequirementid = job_requirement_id_lists.get(position);
                        startActivity(i);
                    }
                });

                holder.refertxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SingletonActivity.fromalljobs = false;
                        SingletonActivity.fromrecommendedjobs = true;
                        SingletonActivity.fromreferjobs = false;


                        Intent i = new Intent(RecommendedJobsActivity.this,ReferJobActivity.class);
                        SingletonActivity.jobrequirementid = job_requirement_id_lists.get(position);
                        SingletonActivity.titlename = title_lists.get(position);
                        startActivity(i);
                    }
                });


                rowView.setTag(holder);
            }
            holder = (Holder) rowView.getTag();





            return rowView;

        }


    }

    private void ApplyJobAPI(String url,final String job_requirement_id){

        pdia = new ProgressDialog(RecommendedJobsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF APPLY JOB API IS---" + response);

                        JSONObject applyjobjson = null;
                        try {
                            applyjobjson = new JSONObject(response);

                            System.out.println("APPLY JOB JSON IS---" + applyjobjson);

                            String statusstr = applyjobjson.getString("status");
                            String msgstr = applyjobjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                if (NetworkUtility.checkConnectivity(RecommendedJobsActivity.this)) {
                                    String CandidateRecommendedJobsURL = APIName.URL + "/candidate/candidateRecommandedJobs?candidate_Id=" + candidateidstr;
                                    System.out.println("CANDIDATE RECOMMENDED JOBS URL IS---" + CandidateRecommendedJobsURL);
                                    CandidateRecommendedJobsAPI(CandidateRecommendedJobsURL);

                                } else {


                                    Snackbar snackbars = Snackbar
                                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                                    View views = snackbars.getView();
                                    TextView tvs = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    tvs.setTextColor(Color.RED);
                                    snackbars.show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();



                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_Id",candidateidstr);
                params.put("job_requirement_Id",job_requirement_id);
                params.put("modified",currentDateTimeString);


                System.out.println("PARAMS OF ApplyJob API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RecommendedJobsActivity.this);
        requestQueue.add(stringRequest);
    }


    private void CandidateRecommendedJobsAPI(String url) {

        pdia = new ProgressDialog(RecommendedJobsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        title_list.clear();
                        hirer_name_list.clear();
                        location_list.clear();
                        notice_period_list.clear();
                        status_list.clear();
                        job_requirement_id_list.clear();
                        job_applied_status_list.clear();


                        System.out.println("RESPONSE OF CANDIDATE RECOMMENDED JOBS API IS---" + response);



                        JSONObject getjobrequirementjson = null;
                        try {
                            getjobrequirementjson = new JSONObject(response);

                            JSONArray CandidateRecommandedJobsJSONArray = getjobrequirementjson.getJSONArray("CandidateRecommandedJobs");

                            for(int i = 0 ; i < CandidateRecommandedJobsJSONArray.length(); i++)
                            {

                               // if(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("status").equalsIgnoreCase("0"))
                              //  {
                                    title_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("title"));
                                    hirer_name_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("hirer_name"));
                                    location_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("location"));
                                    notice_period_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("notice_period"));

                                   // status_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("status"));
                                    job_requirement_id_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("job_requirement_Id"));
                                    job_applied_status_list.add(CandidateRecommandedJobsJSONArray.getJSONObject(i).getString("JobApplyStatus"));
                              //  }


                            }


                            customadap = new CustomAdap(RecommendedJobsActivity.this,job_applied_status_list,job_requirement_id_list,status_list,title_list,hirer_name_list,location_list,notice_period_list);
                            recommendedjobslistvw.setAdapter(customadap);




                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RecommendedJobsActivity.this);
        requestQueue.add(stringRequest);
    }

    public class Holder {
        TextView positiontxt,companytxt,locationdesctxt,noticeperioddesctxt,locationtitletxt,noticeperiodtitletxt,jobidtitletxt,jobiddesctxt,viewtxt,applytxt,refertxt;

    }

}
