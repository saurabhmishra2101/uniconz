package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/29/2018.
 */

public class ChangePasswordActivity extends AppCompatActivity {

    TextView changepasswordtxt;
    CoordinatorLayout coordinatorLayout;
    Typeface bebas_font;
    ImageView backiv;
    EditText oldpasswordedt,newpasswordedt,confirmpasswordedt;
    TextView verifytxt,savetxt;
    String candidateidstr,pwdstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    TextInputLayout newpasswordtxt,confirmpasswordtxt;
    ProgressDialog pdia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_coordinator);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);
        pwdstr = prefs.getString("pwd", null);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        newpasswordtxt = (TextInputLayout)findViewById(R.id.newpasswordtxt);
        confirmpasswordtxt= (TextInputLayout)findViewById(R.id.confirmpasswordtxt);
        oldpasswordedt = (EditText)findViewById(R.id.oldpasswordedt);
        newpasswordedt = (EditText)findViewById(R.id.newpasswordedt);
        confirmpasswordedt = (EditText)findViewById(R.id.confirmpasswordedt);
        verifytxt = (TextView) findViewById(R.id.verifytxt);
        savetxt = (TextView)findViewById(R.id.savetxt);
        backiv = (ImageView)findViewById(R.id.backiv);

        newpasswordtxt.setVisibility(View.INVISIBLE);
        confirmpasswordtxt.setVisibility(View.INVISIBLE);
        newpasswordedt.setVisibility(View.INVISIBLE);
        confirmpasswordedt.setVisibility(View.INVISIBLE);
        savetxt.setVisibility(View.INVISIBLE);

        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");

        changepasswordtxt = (TextView)findViewById(R.id.changepasswordtxt);
        changepasswordtxt.setText("Change  Password");
        changepasswordtxt.setTypeface(bebas_font);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangePasswordActivity.this,DashboardActivity.class);
                startActivity(i);

            }
        });

        verifytxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(oldpasswordedt.getText().toString().length()>0)
                {

                    if(oldpasswordedt.getText().toString().equals(pwdstr)){


                        oldpasswordedt.setEnabled(false);
                        verifytxt.setEnabled(false);
                        newpasswordtxt.setVisibility(View.VISIBLE);
                        confirmpasswordtxt.setVisibility(View.VISIBLE);
                        newpasswordedt.setVisibility(View.VISIBLE);
                        confirmpasswordedt.setVisibility(View.VISIBLE);
                        savetxt.setVisibility(View.VISIBLE);
                        verifytxt.setTextColor(Color.parseColor("#E4E4E4"));

                    }
                    else
                    {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "Wrong Password!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }
                else
                {

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Password!!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });

        savetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean invalid = false;

                if (newpasswordedt.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter New Password", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(confirmpasswordedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Re-Enter Same Password", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {

                    if(newpasswordedt.getText().toString().equals(confirmpasswordedt.getText().toString())) {

                        if (NetworkUtility.checkConnectivity(ChangePasswordActivity.this)) {
                            String insertUpdateActPassURL = APIName.URL + "/candidate/insertUpdateActPass?candidate_Id=" + candidateidstr;
                            System.out.println("INSERT UPDATE ACT PASS URL IS---" + insertUpdateActPassURL);
                            insertUpdateActPassAPI(insertUpdateActPassURL, newpasswordedt.getText().toString());

                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                    else
                    {

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Password Not Matching!!!", Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();

                    }
                }


            
        
            }
        });




    }


    @Override
    public void onBackPressed() {


                Intent i = new Intent(ChangePasswordActivity.this,DashboardActivity.class);
                startActivity(i);

    }

    private void insertUpdateActPassAPI(String url, final String newpwd){

        pdia = new ProgressDialog(ChangePasswordActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF insertUpdateActPassAPI IS---" + response);

                        JSONObject insertUpdateActPassAPIjson = null;
                        try {
                            insertUpdateActPassAPIjson = new JSONObject(response);

                            System.out.println("insertUpdateActPassAPI JSON IS---" + insertUpdateActPassAPIjson);

                            String statusstr = insertUpdateActPassAPIjson.getString("status");
                            String msgstr = insertUpdateActPassAPIjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(ChangePasswordActivity.this, DashboardActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("password",newpwd);
                params.put("isVerified","0");



                System.out.println("PARAMS OF insertUpdateActPassAPI API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordActivity.this);
        requestQueue.add(stringRequest);
    }
}
