package com.smtc.uniconz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by 10161 on 9/25/2017.
 */

public class OTPVerifiedActivity extends Activity {

    RelativeLayout loginrelative;
    Typeface roboto_light_font,roboto_medium_font;
    TextView otpverifiedtext,wehavesharedtxtvw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verified);

        otpverifiedtext = (TextView)findViewById(R.id.otpverifiedtext);
        wehavesharedtxtvw = (TextView)findViewById(R.id.wehavesharedtxtvw);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        roboto_medium_font = Typeface.createFromAsset(getAssets(),  "roboto/Roboto-Medium.ttf");

        otpverifiedtext.setTypeface(roboto_light_font);
        wehavesharedtxtvw.setTypeface(roboto_light_font);

        loginrelative = (RelativeLayout)findViewById(R.id.loginrelative);

        loginrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(OTPVerifiedActivity.this,LoginActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
       /* super.onBackPressed();

        Intent i = new Intent(OTPVerifiedActivity.this,LoginActivity.class);
        startActivity(i);*/

    }
}
