package com.smtc.uniconz;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.DateFormat;

import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/19/2018.
 */

public class EditPersonalDetailActivity extends AppCompatActivity {


    EditText candidatenameedt,emailedt,mobileedt,alternatecontactedt,panedt,passportnumedt,aadharnumedt,currentaddressedt,permanentaddressedt,nationalregistrynumedt,linkedinlinkedt;
    CoordinatorLayout coordinatorLayout;
    static EditText dobedt;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr;
    ProgressDialog pdia;
    RelativeLayout saverelative;
    String candidatenamestr,dobstr,emailstr,mobilestr,alternatecontactstr,panstr,passportnumstr,aadharnumstr,currentaddressstr,permanentaddressstr,nationalregistrynumstr,linkedinlinkstr;
    DatePickerFragment newFragment;
    String currentDateTimeString;
    ImageView backiv;
    Typeface roboto_light_font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_personal_detail_coordinator);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());


        System.out.println("CURRENT DATE TIME STRING---"+ currentDateTimeString);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

  /*      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        backiv = (ImageView)findViewById(R.id.backiv);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditPersonalDetailActivity.this,CandidateDetailsActivity.class);
                startActivity(i);
            }
        });

        candidatenameedt = (EditText)findViewById(R.id.candidatenameedt);
        dobedt = (EditText)findViewById(R.id.dobedt);
        emailedt = (EditText)findViewById(R.id.emailedt);
        mobileedt = (EditText)findViewById(R.id.mobileedt);
        alternatecontactedt = (EditText)findViewById(R.id.alternatecontactedt);
        panedt = (EditText)findViewById(R.id.panedt);
        passportnumedt = (EditText)findViewById(R.id.passportnumedt);
        aadharnumedt = (EditText)findViewById(R.id.aadharnumedt);
        currentaddressedt = (EditText)findViewById(R.id.currentaddressedt);
        permanentaddressedt = (EditText)findViewById(R.id.permanentaddressedt);
        nationalregistrynumedt = (EditText)findViewById(R.id.nationalregistrynumedt);
        linkedinlinkedt = (EditText)findViewById(R.id.linkedinlinkedt);

        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        dobedt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dobedt.setFocusable(false);
                dobedt.setClickable(true);

                newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        if (NetworkUtility.checkConnectivity(EditPersonalDetailActivity.this)) {
            String GetCandidateAllDetailsURL = APIName.URL + "/candidate/getCandidateDetail?candidate_Id=" + candidateidstr;
            System.out.println("GET CANDIDATE DETAIL URL IS---" + GetCandidateAllDetailsURL);
            GetCandidateDetailAPI(GetCandidateAllDetailsURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                candidatenamestr = candidatenameedt.getText().toString();
                dobstr = dobedt.getText().toString();
                emailstr = emailedt.getText().toString();
                mobilestr = mobileedt.getText().toString();
                alternatecontactstr = alternatecontactedt.getText().toString();
                panstr = panedt.getText().toString();
                passportnumstr = passportnumedt.getText().toString();
                aadharnumstr = aadharnumedt.getText().toString();
                currentaddressstr = currentaddressedt.getText().toString();
                permanentaddressstr = permanentaddressedt.getText().toString();
                nationalregistrynumstr = nationalregistrynumedt.getText().toString();
                linkedinlinkstr = linkedinlinkedt.getText().toString();

                boolean invalid = false;

                if (candidatenamestr.equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Candidate Name", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(dobedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Date", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (!isValidEmail(emailedt.getText().toString())) {
                    invalid = true;
                   /* Toast.makeText(getApplicationContext(),
                            "Please Enter Valid Email", Toast.LENGTH_SHORT)
                            .show();
*/
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(!isValidMobileNumber(mobilestr))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile Number", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }
                else if(panstr.equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid PAN No.", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

                else if(!isValidAadharNumber(aadharnumstr))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Aadhar No.", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }
                else if (currentaddressstr.equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Current Address", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (permanentaddressstr.equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Permanent Address", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {

                    if(NetworkUtility.checkConnectivity(EditPersonalDetailActivity.this)){
                        String insertUpdateCandidateURL = APIName.URL+"/candidate/insertUpdateCandidate?candidate_Id="+candidateidstr;
                        System.out.println("INSERT UPDATE CANDIDATE URL IS---"+ insertUpdateCandidateURL);
                        insertUpdateCandidateAPI(insertUpdateCandidateURL,candidatenamestr,dobstr,passportnumstr,nationalregistrynumstr,panstr,aadharnumstr,currentaddressstr,permanentaddressstr,linkedinlinkstr,currentDateTimeString);

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }


                }

            }
        });

    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(EditPersonalDetailActivity.this,CandidateDetailsActivity.class);
        startActivity(i);
    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }


    public  final boolean isValidAadharNumber(String aadharnum) {
        return aadharnum.length() == 12;
    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {

           /* Toast.makeText(this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();
*/

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }

            dobedt.setText(year + "-"
                    + monthnew + "-" + daynew);

        }


    }



    private void insertUpdateCandidateAPI(String url,final String candidatenamestr,final String dobstr,final String passportnumstr,final String nationalregistrynumstr,final String panstr,final String aadharnumstr,final String currentaddressstr,final String permanentaddressstr,final String linkedinlinkstr,final String currentDateTimeString){

        pdia = new ProgressDialog(EditPersonalDetailActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE CANDIDATE API IS---" + response);

                        JSONObject insertupdatecandidatejson = null;
                        try {
                            insertupdatecandidatejson = new JSONObject(response);

                        System.out.println("INSERT UPDATE CANDIDATE JSON IS---" + insertupdatecandidatejson);

                        String statusstr = insertupdatecandidatejson.getString("status");
                        String msgstr = insertupdatecandidatejson.getString("message");

                        if(statusstr.equalsIgnoreCase("1"))
                        {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.GREEN);
                            snackbar.show();


                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    Intent i = new Intent(EditPersonalDetailActivity.this, CandidateDetailsActivity.class);
                                    startActivity(i);


                                }
                            }, 2000);
                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_name",candidatenamestr);
                params.put("dob",dobstr);
                params.put("passport_no",passportnumstr);
                params.put("nsr_no",nationalregistrynumstr);
                params.put("pan_card_no",panstr);
                params.put("aadhar_no",aadharnumstr);
                params.put("current_address",currentaddressstr);
                params.put("permanent_address",permanentaddressstr);
                params.put("linkedIn_url",linkedinlinkstr);
                params.put("modified",currentDateTimeString);
                params.put("status","1");

                System.out.println("PARAMS OF insertUpdateCandidateAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditPersonalDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetCandidateDetailAPI(String url) {

        pdia = new ProgressDialog(EditPersonalDetailActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET CANDIDATE ALL DETAILS API IS---" + response);

                        JSONObject candidatedetailsjson = null;
                        try {
                            candidatedetailsjson = new JSONObject(response);

                            JSONObject CandidateDetails = candidatedetailsjson.getJSONObject("CandidateDetails");


                            candidatenameedt.setText(CandidateDetails.getString("candidate_name"));
                            dobedt.setText(CandidateDetails.getString("dob"));
                            emailedt.setText(CandidateDetails.getString("email"));
                            mobileedt.setText(CandidateDetails.getString("mob_number"));

                            if(CandidateDetails.getString("alternate_contact").equals("0"))
                            {
                                alternatecontactedt.setText("");
                            }
                            else {
                                alternatecontactedt.setText(CandidateDetails.getString("alternate_contact"));
                            }

                            System.out.println("PAN CARD NO. LENGTH==="+ CandidateDetails.getString("pan_card_no").length());

                            if(CandidateDetails.getString("pan_card_no").length()==4)
                            {
                                panedt.setText("");
                            }
                            else
                            {
                                panedt.setText(CandidateDetails.getString("pan_card_no"));
                            }

                            if(CandidateDetails.getString("passport_no").length()==4)
                            {
                                passportnumedt.setText("");
                            }
                            else
                            {
                                passportnumedt.setText(CandidateDetails.getString("passport_no"));
                            }
                            if(CandidateDetails.getString("aadhar_no").length()==4)
                            {
                                aadharnumedt.setText("");
                            }
                            else
                            {
                                aadharnumedt.setText(CandidateDetails.getString("aadhar_no"));
                            }

                            if(CandidateDetails.getString("current_address").length()==4)
                            {
                                currentaddressedt.setText("");
                            }
                            else
                            {
                                currentaddressedt.setText(CandidateDetails.getString("current_address"));
                            }

                            if(CandidateDetails.getString("permanent_address").length()==4)
                            {
                                permanentaddressedt.setText("");
                            }
                            else
                            {
                                permanentaddressedt.setText(CandidateDetails.getString("permanent_address"));
                            }

                            if(CandidateDetails.getString("nsr_no").length()==4)
                            {
                                nationalregistrynumedt.setText("");
                            }
                            else
                            {
                                nationalregistrynumedt.setText(CandidateDetails.getString("nsr_no"));
                            }

                            if(CandidateDetails.getString("linkedIn_url").length()==4)
                            {
                                linkedinlinkedt.setText("");
                            }
                            else
                            {
                                linkedinlinkedt.setText(CandidateDetails.getString("linkedIn_url"));
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditPersonalDetailActivity.this);
        requestQueue.add(stringRequest);
    }
}
