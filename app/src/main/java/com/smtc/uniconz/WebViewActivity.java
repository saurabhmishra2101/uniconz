package com.smtc.uniconz;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WebViewActivity extends AppCompatActivity {

    String url;
    WebView webView;
    ImageView backiv;
    ProgressBar progressBar;
    TextView progresspercent;
    String saudapatraurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progresspercent = (TextView)findViewById(R.id.percent);


        //progressBar.setVisibility(View.GONE);

      /*  Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });*/

        backiv = (ImageView)findViewById(R.id.backiv);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();


            }
        });

        url = APIName.IMAGE_URL + SingletonActivity.certificateuploadurl;
        System.out.println("WEBVIEW URL:----->>"+ url);

        webView = (WebView) findViewById(R.id.webview);

        webView.setWebChromeClient(new WebChromeClient(){


    public void onProgressChanged(WebView view, int progress) {
        progressBar.setProgress(progress);
        progresspercent.setText(progress + " %");
        if (progress == 100) {
            progresspercent.setVisibility(View.GONE);

        } else {
            progresspercent.setVisibility(View.VISIBLE);

        }
    }

});
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (url.endsWith(".doc") || url.endsWith(".docx") || url.endsWith(".ppt") || url.endsWith(".pptx") || url.endsWith(".pdf") || url.endsWith(".txt")) {
           // progressBar.setVisibility(View.VISIBLE);
            System.out.println("IN IF LOOP----");
            webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
           // webView.loadUrl(url);

            webView.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent,
                                            String contentDisposition, String mimetype,
                                            long contentLength) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
        } else {

            System.out.println("IN ELSE LOOP----");
            webView.loadUrl(url);
        }

    }

    @Override
    public void onBackPressed() {
       /* Intent i = new Intent(WebViewActivity.this,EditProfessionalCourseActivity.class);
        startActivity(i);*/

           finish();
    }

    private class MyWebViewClient extends WebViewClient {
       @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
           // progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

        //    progressBar.setProgress(10);
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {


            super.onPageStarted(view, url, favicon);
          // progressBar.setVisibility(View.GONE);
        }
    }


}
