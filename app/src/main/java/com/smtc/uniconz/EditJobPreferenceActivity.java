package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/2/2018.
 */

public class EditJobPreferenceActivity extends AppCompatActivity  implements AdapterView.OnItemClickListener{

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr;
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    ArrayList<String> preflocationidlist = new ArrayList<String>();
    ArrayList<String> preferencelocationlist = new ArrayList<String>();

    ArrayList<String> prefcompanyidlist = new ArrayList<String>();
    ArrayList<String> preferencecompanylist = new ArrayList<String>();

    ArrayList<String> nonprefcompanyidlist = new ArrayList<String>();
    ArrayList<String> nonpreferencecompanylist = new ArrayList<String>();

    AutoCompleteTextView location_autocomplete1,location_autocomplete2,location_autocomplete3;
    EditText prefcompany1edt,prefcompany2edt,prefcompany3edt;
    EditText notprefcompany1edt,notprefcompany2edt,notprefcompany3edt;

    Typeface roboto_light_font;
    private static final String LOG_TAG = "Uniconz";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";
    RelativeLayout saverelative;
    String locationstr,pref_location_str,pref_company_str,non_pref_company_str,currentDateTimeString;

    ImageView backiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_job_preference_coordinator);

        backiv = (ImageView) findViewById(R.id.backiv);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditJobPreferenceActivity.this,CandidateDetailsActivity.class);
                startActivity(i);

            }
        });


        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());

        location_autocomplete1 = (AutoCompleteTextView)findViewById(R.id.location_autocomplete1);
        location_autocomplete2 = (AutoCompleteTextView)findViewById(R.id.location_autocomplete2);
        location_autocomplete3 = (AutoCompleteTextView)findViewById(R.id.location_autocomplete3);

        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        prefcompany1edt = (EditText) findViewById(R.id.prefcompany1edt);
        prefcompany2edt = (EditText) findViewById(R.id.prefcompany2edt);
        prefcompany3edt = (EditText) findViewById(R.id.prefcompany3edt);

        notprefcompany1edt = (EditText) findViewById(R.id.notprefcompany1edt);
        notprefcompany2edt = (EditText) findViewById(R.id.notprefcompany2edt);
        notprefcompany3edt = (EditText) findViewById(R.id.notprefcompany3edt);

        location_autocomplete1.setTypeface(roboto_light_font);
        location_autocomplete2.setTypeface(roboto_light_font);
        location_autocomplete3.setTypeface(roboto_light_font);

        prefcompany1edt.setTypeface(roboto_light_font);
        prefcompany2edt.setTypeface(roboto_light_font);
        prefcompany3edt.setTypeface(roboto_light_font);

        notprefcompany1edt.setTypeface(roboto_light_font);
        notprefcompany2edt.setTypeface(roboto_light_font);
        notprefcompany3edt.setTypeface(roboto_light_font);

        location_autocomplete1.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete1.setOnItemClickListener(EditJobPreferenceActivity.this);

        location_autocomplete2.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete2.setOnItemClickListener(EditJobPreferenceActivity.this);

        location_autocomplete3.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete3.setOnItemClickListener(EditJobPreferenceActivity.this);

        if (NetworkUtility.checkConnectivity(EditJobPreferenceActivity.this)) {
            String GetCandAddDetailURL = APIName.URL + "/candidate/getPreferenceDetail?candidate_Id=" + candidateidstr;
            System.out.println("GET PREFERENCE DETAIL URL IS---" + GetCandAddDetailURL);
            GetPreferenceDetailAPI(GetCandAddDetailURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {






                if(location_autocomplete1.getText().toString().length()>0 || location_autocomplete2.getText().toString().length()>0 || location_autocomplete3.getText().toString().length()>0 || prefcompany1edt.getText().toString().length()>0 || prefcompany2edt.getText().toString().length()>0 || prefcompany3edt.getText().toString().length()>0 || notprefcompany1edt.getText().toString().length()>0 || notprefcompany2edt.getText().toString().length()>0 || notprefcompany3edt.getText().toString().length()>0){


                    pref_location_str = "";

                    if (location_autocomplete1.getText().toString().length()>0) {
                        pref_location_str = location_autocomplete1.getText().toString()+",";

                    }else{

                        pref_location_str = ",";

                    }
                    if (location_autocomplete2.getText().toString().length()>0){
                        pref_location_str = pref_location_str+location_autocomplete2.getText().toString()+",";
                    }else{
                        pref_location_str = pref_location_str+",";
                    }
                    if (location_autocomplete3.getText().toString().length()>0) {
                        pref_location_str = pref_location_str+location_autocomplete3.getText().toString() + "";
                    }else{
                        pref_location_str = pref_location_str+"";
                    }

                    System.out.println("LOCATION COMMA SEPARATED loc===="+ pref_location_str);

                    //=================================



                            pref_company_str = "";

                    if (prefcompany1edt.getText().toString().length()>0) {
                        pref_company_str = prefcompany1edt.getText().toString()+",";

                    }else{

                        pref_company_str = ",";

                    }
                    if (prefcompany2edt.getText().toString().length()>0){
                        pref_company_str = pref_company_str+prefcompany2edt.getText().toString()+",";
                    }else{
                        pref_company_str = pref_company_str+",";
                    }
                    if (prefcompany3edt.getText().toString().length()>0) {
                        pref_company_str = pref_company_str+prefcompany3edt.getText().toString() + "";
                    }else{
                        pref_company_str = pref_company_str+"";
                    }

                    System.out.println("LOCATION COMMA SEPARATED company===="+ pref_company_str);

                    //=============================

                    //=================================



                    non_pref_company_str = "";

                    notprefcompany1edt = (EditText) findViewById(R.id.notprefcompany1edt);
                    notprefcompany2edt = (EditText) findViewById(R.id.notprefcompany2edt);
                    notprefcompany3edt = (EditText) findViewById(R.id.notprefcompany3edt);

                    if (prefcompany1edt.getText().toString().length()>0) {
                        non_pref_company_str = notprefcompany1edt.getText().toString()+",";

                    }else{

                        non_pref_company_str = ",";

                    }
                    if (prefcompany2edt.getText().toString().length()>0){
                        non_pref_company_str = non_pref_company_str+notprefcompany2edt.getText().toString()+",";
                    }else{
                        non_pref_company_str = non_pref_company_str+",";
                    }
                    if (prefcompany3edt.getText().toString().length()>0) {
                        non_pref_company_str = non_pref_company_str+notprefcompany3edt.getText().toString() + "";
                    }else{
                        non_pref_company_str = non_pref_company_str+"";
                    }

                    System.out.println("LOCATION COMMA SEPARATED not pref company===="+ pref_company_str);

                    //=============================



                    if(NetworkUtility.checkConnectivity(EditJobPreferenceActivity.this)){
                        String insertUpdatePreferenceURL = APIName.URL+"/candidate/insertUpdatePreferrence?candidate_Id="+candidateidstr;
                        System.out.println("INSERT UPDATE PREFERENCE URL IS---"+ insertUpdatePreferenceURL);
                        insertUpdatePreferenceAPI(insertUpdatePreferenceURL,pref_location_str,pref_company_str,non_pref_company_str,currentDateTimeString);

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please fill the fields!!!", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });



    }

    @Override
    public void onBackPressed() {


                Intent i = new Intent(EditJobPreferenceActivity.this,CandidateDetailsActivity.class);
                startActivity(i);

    }

    private void insertUpdatePreferenceAPI(String url, final String pref_location_str, final String pref_company_str, final String non_pref_company_str, final String currentDateTimeString){

        pdia = new ProgressDialog(EditJobPreferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE PREFERENCE API IS---" + response);

                        JSONObject insertupdatepreferencejson = null;
                        try {
                            insertupdatepreferencejson = new JSONObject(response);

                            System.out.println("INSERT UPDATE PREFERENCE JSON IS---" + insertupdatepreferencejson);

                            String statusstr = insertupdatepreferencejson.getString("status");
                            String msgstr = insertupdatepreferencejson.getString("message");

                            if(statusstr.equalsIgnoreCase("1"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditJobPreferenceActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("company",pref_company_str);
                params.put("not_preferred",non_pref_company_str);
                params.put("location",pref_location_str);
                params.put("candidate_Id",candidateidstr);
                params.put("modified",currentDateTimeString);

                System.out.println("PARAMS OF insertUpdatePreferenceAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditJobPreferenceActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetPreferenceDetailAPI(String url) {

        pdia = new ProgressDialog(EditJobPreferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        preflocationidlist.clear();
                        preferencelocationlist.clear();

                        prefcompanyidlist.clear();
                        preferencecompanylist.clear();

                        nonprefcompanyidlist.clear();
                        nonpreferencecompanylist.clear();

                        System.out.println("GET JOB PREFERENCE RESPONSE==="+ response);


                        JSONObject getjobpreferencejson = null;
                        try {
                            getjobpreferencejson = new JSONObject(response);

                            JSONObject CandidateAllPreferenceJSON = getjobpreferencejson.getJSONObject("CandidateAllPreference");

                            if(!CandidateAllPreferenceJSON.isNull("PrefLocation"))
                            {
                                JSONArray PrefLocationJSONArray = CandidateAllPreferenceJSON.getJSONArray("PrefLocation");

                                for(int i = 0; i < PrefLocationJSONArray.length(); i++)
                                {
                                    preflocationidlist.add(PrefLocationJSONArray.getJSONObject(i).getString("preference_location_id"));
                                    preferencelocationlist.add(PrefLocationJSONArray.getJSONObject(i).getString("preference_location"));

                                    if(preferencelocationlist.size()==1) {
                                        location_autocomplete1.setText(preferencelocationlist.get(0));
                                        location_autocomplete2.setText("");
                                        location_autocomplete3.setText("");
                                    }

                                    if(preferencelocationlist.size()==2) {
                                        location_autocomplete1.setText(preferencelocationlist.get(0));
                                        location_autocomplete2.setText(preferencelocationlist.get(1));
                                        location_autocomplete3.setText("");
                                    }

                                    if(preferencelocationlist.size()==3) {
                                        location_autocomplete1.setText(preferencelocationlist.get(0));
                                        location_autocomplete2.setText(preferencelocationlist.get(1));
                                        location_autocomplete3.setText(preferencelocationlist.get(2));
                                    }


                                }

                               // pref_location_str = android.text.TextUtils.join(",", preferencelocationlist);
                            }
                            else
                            {
                                location_autocomplete1.setText("");
                                location_autocomplete2.setText("");
                                location_autocomplete3.setText("");
                            }


                            if(!CandidateAllPreferenceJSON.isNull("PrefCompany"))
                            {
                                JSONArray PrefCompanyJSONArray = CandidateAllPreferenceJSON.getJSONArray("PrefCompany");

                                for(int i = 0; i < PrefCompanyJSONArray.length(); i++)
                                {

                                    prefcompanyidlist.add(PrefCompanyJSONArray.getJSONObject(i).getString("preference_company_id"));
                                    preferencecompanylist.add(PrefCompanyJSONArray.getJSONObject(i).getString("preference_company"));

                                    if(preferencecompanylist.size()==1) {
                                        prefcompany1edt.setText(preferencecompanylist.get(0));
                                        prefcompany2edt.setText("");
                                        prefcompany3edt.setText("");
                                    }

                                    if(preferencecompanylist.size()==2) {
                                        prefcompany1edt.setText(preferencecompanylist.get(0));
                                        prefcompany2edt.setText(preferencecompanylist.get(1));
                                        prefcompany3edt.setText("");
                                    }

                                    if(preferencecompanylist.size()==3) {
                                        prefcompany1edt.setText(preferencecompanylist.get(0));
                                        prefcompany2edt.setText(preferencecompanylist.get(1));
                                        prefcompany3edt.setText(preferencecompanylist.get(2));
                                    }


                                }

                             //   pref_company_str = android.text.TextUtils.join(",", preferencecompanylist);

                            }
                            else
                            {
                                prefcompany1edt.setText("");
                                prefcompany2edt.setText("");
                                prefcompany3edt.setText("");
                            }


                            if(!CandidateAllPreferenceJSON.isNull("NonPrefCompany"))
                            {
                                JSONArray NonPrefLocationJSONArray = CandidateAllPreferenceJSON.getJSONArray("NonPrefCompany");

                                for(int i = 0; i < NonPrefLocationJSONArray.length(); i++)
                                {

                                    nonprefcompanyidlist.add(NonPrefLocationJSONArray.getJSONObject(i).getString("non_preference_company_id"));
                                    nonpreferencecompanylist.add(NonPrefLocationJSONArray.getJSONObject(i).getString("non_preference_company"));

                                    if(nonpreferencecompanylist.size()==1) {
                                        notprefcompany1edt.setText(nonpreferencecompanylist.get(0));
                                        notprefcompany2edt.setText("");
                                        notprefcompany3edt.setText("");
                                    }

                                    if(nonpreferencecompanylist.size()==2) {
                                        notprefcompany1edt.setText(nonpreferencecompanylist.get(0));
                                        notprefcompany2edt.setText(nonpreferencecompanylist.get(1));
                                        notprefcompany3edt.setText("");
                                    }

                                    if(nonpreferencecompanylist.size()==3) {
                                        notprefcompany1edt.setText(nonpreferencecompanylist.get(0));
                                        notprefcompany2edt.setText(nonpreferencecompanylist.get(1));
                                        notprefcompany3edt.setText(nonpreferencecompanylist.get(2));
                                    }


                                }

                              //  non_pref_company_str = android.text.TextUtils.join(",", nonpreferencecompanylist);

                            }
                            else
                            {
                                notprefcompany1edt.setText("");
                                notprefcompany2edt.setText("");
                                notprefcompany3edt.setText("");
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {



                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditJobPreferenceActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        locationstr = (String) adapterView.getItemAtPosition(position);
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            // sb.append("&components=&types=(cities)");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
