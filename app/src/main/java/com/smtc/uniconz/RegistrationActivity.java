package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 9/19/2017.
 */

public class RegistrationActivity extends Activity {

    private CoordinatorLayout coordinatorLayout;
    ImageView backiv;
    String[] IndustryList = {"IT-SOFTWARE","IT-HARDWARE","NETWORKING","TELECOM","BFSI-BANKING & FINANCE","E-COMMERCE","RESEARCH & ANALYTICS","OIL & GAS","ENERGY","REAL ESTATE","RECRUITMENT","SEMICONDUCTOR & ELECTRONICS","OTHERS"};
    MaterialBetterSpinner industry_spinner;
    RelativeLayout submitrelative;
    EditText candidatenameedt,emailedt,mobileedt,skilledt,totalexpedt,designationedt;
    Typeface roboto_light_font,roboto_medium_font;
    String industry_spinner_str;
    AutoCompleteTextView location_autocomplete;
    ArrayList<String> citylist = new ArrayList<String>();
    ProgressDialog pdia;
    String industry_type_Id;
    Button choosefilebtn;
    TextView uploadresumetxt,nofiletxt;
    String type = "";
    String attachmentstr,upload_condition_str;
    TextInputLayout uploadresumetxtin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registration);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        roboto_medium_font = Typeface.createFromAsset(getAssets(),  "roboto/Roboto-Medium.ttf");

        backiv = (ImageView)findViewById(R.id.backiv);
        candidatenameedt = (EditText)findViewById(R.id.candidatenameedt);
        emailedt = (EditText)findViewById(R.id.emailedt);
        mobileedt = (EditText)findViewById(R.id.mobileedt);
        skilledt = (EditText)findViewById(R.id.skilledt);
        location_autocomplete = (AutoCompleteTextView)findViewById(R.id.location_autocomplete);

        totalexpedt = (EditText)findViewById(R.id.totalexpedt);
        uploadresumetxtin = (TextInputLayout)findViewById(R.id.uploadresumetxtin);

        //validation for decimal
        totalexpedt.setInputType(0x00002002);
        designationedt = (EditText)findViewById(R.id.designationedt);
        

        uploadresumetxt = (TextView)findViewById(R.id.uploadresumetxt);

      


        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        uploadresumetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
                uploadresumetxt.setText("");
            }
        });

       if(NetworkUtility.checkConnectivity(RegistrationActivity.this)){
            String GetLocationMasterURL = APIName.URL+"/master/getLocationMaster";
            System.out.println("GET LOCATION MASTER URL IS---"+ GetLocationMasterURL);
            GetLocationMasterAPI(GetLocationMasterURL);

        }
        else{
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }


        ArrayAdapter<String> industryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, IndustryList);
        industry_spinner = (MaterialBetterSpinner)
                findViewById(R.id.industry_spinner);
        industry_spinner.setAdapter(industryAdapter);


        industry_spinner.setTypeface(roboto_light_font);

        industry_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                industry_spinner_str = parent.getItemAtPosition(position).toString();

                if(position == 0) {
                    industry_type_Id = "0";
                }

                if(position == 1) {
                    industry_type_Id = "1";
                }

                if(position == 2) {
                    industry_type_Id = "2";
                }

                if(position == 3) {
                    industry_type_Id = "3";
                }

                if(position == 4) {
                    industry_type_Id = "4";
                }

                if(position == 5) {
                    industry_type_Id = "5";
                }

                if(position == 6) {
                    industry_type_Id = "6";
                }

                if(position == 7) {
                    industry_type_Id = "7";
                }

                if(position == 8) {
                    industry_type_Id = "8";
                }

                if(position == 9) {
                    industry_type_Id = "9";
                }

                if(position == 10) {
                    industry_type_Id = "10";
                }

                if(position == 11) {
                    industry_type_Id = "11";
                }

                if(position == 12) {
                    industry_type_Id = "12";
                }




            }
        });



        submitrelative = (RelativeLayout)findViewById(R.id.submitrelative);

        submitrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                String candidatenamestr = candidatenameedt.getText().toString();
                String emailstr = emailedt.getText().toString();
                String mobilestr = mobileedt.getText().toString();
                String skillstr = skilledt.getText().toString();
                String totalexpstr = totalexpedt.getText().toString();
                String designationstr = designationedt.getText().toString();


                if(attachmentstr==null)
                {
                    attachmentstr = "0";
                    SingletonActivity.attachmentstr = attachmentstr;

                }
                else
                {
                    SingletonActivity.attachmentstr = attachmentstr;

                }


                if(attachmentstr==null)
                {
                    upload_condition_str = "0";
                }
                else
                {
                    upload_condition_str = "1";
                }

                SingletonActivity.upload_condition_str = upload_condition_str;

                SingletonActivity.candidatenamestr = candidatenamestr;
                SingletonActivity.emailstr = emailstr;
                SingletonActivity.mobilestr = mobilestr;
                SingletonActivity.skillstr = skillstr;
                SingletonActivity.industry_type_Id = industry_type_Id;
                SingletonActivity.designationstr = designationstr;
                SingletonActivity.totalexpstr = totalexpstr;
                SingletonActivity.location_autocomplete= location_autocomplete.getText().toString();
                SingletonActivity.upload_condition_str = upload_condition_str;
                SingletonActivity.attachmentstr = attachmentstr;



                if (candidatenamestr.equals("")) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Candidate Name", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                else  if (!isValidEmail(emailstr)) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();


                }
                else  if (!isValidMobileNumber(mobilestr)) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile No.", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

               else if (skillstr.equals("")) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Skills", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else  if (location_autocomplete.getText().toString().equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Location", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

                else  if (totalexpstr.equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Years of Experience", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }


                else  if (designationstr.equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Designation", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

                else  if (industry_spinner.getText().toString().equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Industry", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

                else if(type.equals(""))
                {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Upload Resume", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }


                else if (invalid == false) {

               if(NetworkUtility.checkConnectivity(RegistrationActivity.this)){
                  String CheckEmailURL = APIName.URL+"/candidate/checkEmail";
                  System.out.println("CHECK EMAIL URL IS---"+ CheckEmailURL);
                  CheckEmailAPI(CheckEmailURL,mobilestr,emailstr);

                 }
                 else{
                  Snackbar snackbar = Snackbar
                          .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                  View view = snackbar.getView();
                  TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                  tv.setTextColor(Color.RED);
                  snackbar.show();
               }




                }
            }
        });

    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");



        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();
                    System.out.println("SELECTED FILE PATH IS--"+ filePath);

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;



                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);

                        if(type.equals("pdf")||type.equals("doc")||type.equals("docx"))
                        {
                            attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                            System.out.println("BASE 64 STRING------"+ attachmentstr);

                            if (uriString.startsWith("content://")) {
                                Cursor cursor = null;
                                try {
                                    cursor = RegistrationActivity.this.getContentResolver().query(uri, null, null, null, null);
                                    if (cursor != null && cursor.moveToFirst()) {
                                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        System.out.println("displayName 1 is------"+ displayName);
                                        uploadresumetxt.setText(displayName);
                                    }
                                } finally {
                                    cursor.close();
                                }
                            } else if (uriString.startsWith("file://")) {
                                displayName = myFile.getName();
                                System.out.println("displayName 2 is------"+ displayName);
                                uploadresumetxt.setText(displayName);
                            }
                        }
                        else {

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Please upload the resume in following formats .pdf,.doc,.docx only", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }





                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }



    private void CheckEmailAPI(String url,final String mobilenum,final String email) {

        pdia = new ProgressDialog(RegistrationActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF CHECK EMAIL API IS---" + response);

                        try {
                            JSONObject checkemailjson = new JSONObject(response);

                            String signupstr = checkemailjson.getString("signup");
                            String userstr = checkemailjson.getString("user");

                            if ((userstr.equalsIgnoreCase("true"))) {


                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "USER EMAIL EXISTS", Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();

                            }else if (signupstr.equalsIgnoreCase("true")) {
                               // [self registrationAlertMessage:@"USER ALREADY REGISTERED"  title:@"ERROR"];

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "USER ALREADY REGISTERED", Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();

                            }else if (!(userstr.equalsIgnoreCase("true"))&& (!signupstr.equalsIgnoreCase("true"))){
                                //OTP hit
                               // [self jsonOtpPostMethod:[NSString stringWithFormat:@"%@/message/generateOTP",[appD.defaults objectForKey:@"serviceURL"]] method:@"POST" paramStr:[NSString stringWithFormat:@"mob_number=%@",self.mobNoTxtFld.text]];
                                if(NetworkUtility.checkConnectivity(RegistrationActivity.this)){
                                String GenerateOTPURL = APIName.URL+"/message/generateOTP";
                                System.out.println("GENERATE OTP URL IS---"+ GenerateOTPURL);
                                 GenerateOTPAPI(GenerateOTPURL,mobilenum);
                                }
                               else{
                                    Snackbar snackbar = Snackbar
                                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                                    View view = snackbar.getView();
                                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                    tv.setTextColor(Color.RED);
                                    snackbar.show();
                                }

                            }
                        } catch (JSONException e) {
                            pdia.dismiss();
                            e.printStackTrace();
                        }






                }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                               // Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");


                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();

                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("email",email);


                System.out.println("CHECK EMAIL PARAMS ARE---"+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
        requestQueue.add(stringRequest);
    }


    private void GenerateOTPAPI(String url,final String mobileno) {

        pdia = new ProgressDialog(RegistrationActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF GENERATE OTP API IS---" + response);

                        try {
                            JSONObject generateotpjson = new JSONObject(response);
                            String statusstr = generateotpjson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")){

                                Intent i = new Intent(RegistrationActivity.this,OTPActivity.class);
                                SingletonActivity.otp = generateotpjson.getString("otp");
                                startActivity(i);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                              //  Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                          //  util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("mob_number",mobileno);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                params.put("mob_number",mobileno);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
        requestQueue.add(stringRequest);
    }


    private void GetLocationMasterAPI(String url) {

        /*pdia = new ProgressDialog(RegistrationActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

*/

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                      //  pdia.dismiss();

                        System.out.println("RESPONSE OF GET LOCATION MASTER API IS---" + response);
                        citylist.clear();

                       JSONObject CityListJSON;
                        try {
                            CityListJSON = new JSONObject(response);
                            JSONArray CityListJSONArray = CityListJSON.getJSONArray("CityList");



                            for(int i = 0 ; i < CityListJSONArray.length(); i++)
                            {
                                String cityname = CityListJSONArray.getJSONObject(i).getString("city_name");
                                System.out.println("CITY NAME IS---" + cityname);
                                citylist.add(cityname);
                            }

                            ArrayAdapter<String> cityadapter = new ArrayAdapter<String>(RegistrationActivity.this,android.R.layout.simple_list_item_1,citylist);
                            location_autocomplete.setAdapter(cityadapter);




                        } catch (JSONException e) {

                          //  pdia.dismiss();
                            e.printStackTrace();
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                         //   util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
        requestQueue.add(stringRequest);
    }




    public final  boolean isValidEmail(String email) {
        if (email == null) {

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }


}
