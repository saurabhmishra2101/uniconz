package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/4/2018.
 */

public class SecondLevelReferenceActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr,currentDateTimeString;
    ProgressDialog pdia;
    ArrayList<String> candidate_name_list = new ArrayList<String>();
    ArrayList<String> designation_list = new ArrayList<String>();
    ArrayList<String> status_list = new ArrayList<String>();
    ArrayList<String> mobile_num_list = new ArrayList<String>();
    ArrayList<String> email_id_list = new ArrayList<String>();
    ArrayList<String> total_exp_list = new ArrayList<String>();
    CustomAdap customadap;
    ListView firstlevellistvw;
    TextView referralrqst;
    Typeface bebas_font,sf_font;
    ImageView backiv;
    ArrayList<String> inprogresslist = new ArrayList<String>();
    ArrayList<String> placedlist = new ArrayList<String>();
    ArrayList<String> rejectedlist = new ArrayList<String>();
    ArrayList<String> secondlevelcountlist = new ArrayList<String>();
    Button rel1,rel2,rel3,rel4;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_level_reference_coordinator);



        backiv = (ImageView)findViewById(R.id.backiv);
        rel1 = (Button) findViewById(R.id.rel1);
        rel2 = (Button) findViewById(R.id.rel2);
        rel3 = (Button) findViewById(R.id.rel3);
        rel4 = (Button) findViewById(R.id.rel4);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SecondLevelReferenceActivity.this,FirstLevelReferenceActivity.class);
                startActivity(i);
            }
        });



        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");
        sf_font = Typeface.createFromAsset(getAssets(), "sfuidisplay/SF.ttf");

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        firstlevellistvw = (ListView)findViewById(R.id.firstlevellistvw);

        referralrqst = (TextView)findViewById(R.id.referralrqst);
        referralrqst.setTypeface(bebas_font);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());

        if (NetworkUtility.checkConnectivity(SecondLevelReferenceActivity.this)) {
            String getCandChildRefForMobileURL = APIName.URL + "/candidate/getCandChildRefForMobile?candidate_Id=" + SingletonActivity.secondlevelchildid;
            System.out.println("GET CAND CHILD REF FOR MOBILE URL 2 IS---" + getCandChildRefForMobileURL);
            getCandChildRefForMobileAPI(getCandChildRefForMobileURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SecondLevelReferenceActivity.this,FirstLevelReferenceActivity.class);
        startActivity(i);

    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        ArrayList<String> candidate_name_lists;
        ArrayList<String> designation_lists;
        ArrayList<String> status_lists;
        ArrayList<String> mobile_num_lists;
        ArrayList<String> email_id_lists;
        ArrayList<String> total_exp_lists;
        ArrayList<String> secondlevelcountlists;


        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,ArrayList<String> secondlevelcountlist,ArrayList<String> candidate_name_list,ArrayList<String> designation_list,ArrayList<String> status_list,ArrayList<String> mobile_num_list,ArrayList<String> email_id_list,ArrayList<String> total_exp_list)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.candidate_name_lists = candidate_name_list;
            this.designation_lists = designation_list;
            this.status_lists = status_list;
            this.mobile_num_lists = mobile_num_list;
            this.email_id_lists = email_id_list;
            this.total_exp_lists = total_exp_list;
            this.secondlevelcountlists = secondlevelcountlist;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            // return 7;
            return candidate_name_lists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            // final Holder holder = new Holder();
            Holder holder ;


            View rowView = null;




            if(rowView==null){
               /* LayoutInflater inflater =(LayoutInflater)
                        ((Activity)context).getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
                rowView = inflater.inflate(R.layout.second_level_reference_row, null);

                holder = new Holder();

                holder.nametxt = (TextView)rowView.findViewById(R.id.nametxt);
                holder.designationtxt = (TextView)rowView.findViewById(R.id.designationtxt);
                holder.statusdesctxt = (TextView)rowView.findViewById(R.id.statusdesctxt);
                holder.mobilenodesctxt = (TextView)rowView.findViewById(R.id.mobilenodesctxt);
                holder.emaildesctxt = (TextView)rowView.findViewById(R.id.emaildesctxt);
                holder.totalexpdesctxt = (TextView)rowView.findViewById(R.id.totalexpdesctxt);

                holder.statustxt = (TextView)rowView.findViewById(R.id.statustxt);
                holder.mobilenotxt = (TextView)rowView.findViewById(R.id.mobilenotxt);
                holder.emailtxt = (TextView)rowView.findViewById(R.id.emailtxt);
                holder.totalexptxt = (TextView)rowView.findViewById(R.id.totalexptxt);

                holder.secondleveldesctxt = (TextView)rowView.findViewById(R.id.secondleveldesctxt);


                holder.nametxt.setTypeface(sf_font);
                holder.designationtxt.setTypeface(sf_font);
                holder.statusdesctxt.setTypeface(sf_font);
                holder.mobilenodesctxt.setTypeface(sf_font);
                holder.emaildesctxt.setTypeface(sf_font);
                holder.totalexpdesctxt.setTypeface(sf_font);
                holder.statustxt.setTypeface(sf_font);
                holder.mobilenotxt.setTypeface(sf_font);
                holder.emailtxt.setTypeface(sf_font);
                holder.totalexptxt.setTypeface(sf_font);

                System.out.println("STATUS LIST 1st level==="+ status_lists);



                holder.nametxt.setText(candidate_name_lists.get(position));
                holder.designationtxt.setText(designation_lists.get(position));

                inprogresslist.clear();
                placedlist.clear();
                rejectedlist.clear();

                if(status_lists.get(position).equalsIgnoreCase("0")||status_lists.get(position).equalsIgnoreCase("1")) {
                    holder.statusdesctxt.setText("In Progress");
                    inprogresslist.add(status_lists.get(position));
                    System.out.println("IN LOOP ==1");
                    int inprogesscount = inprogresslist.size();
                    rel3.setText(Integer.toString(inprogesscount));

                }



                if(status_lists.get(position).equalsIgnoreCase("2"))
                {
                    holder.statusdesctxt.setText("Approved");
                    placedlist.add(status_lists.get(position));
                    System.out.println("IN LOOP ==2");
                    int placedcount = placedlist.size();
                    rel2.setText(Integer.toString(placedcount));
                }

                if(status_lists.get(position).equalsIgnoreCase("3"))
                {
                    holder.statusdesctxt.setText("Rejected");
                    rejectedlist.add(status_lists.get(position));
                    System.out.println("IN LOOP ==3"+ rejectedlist.size());
                    int rejectedcount = rejectedlist.size();
                    rel4.setText(Integer.toString(rejectedcount));
                }

                if(rel2.getText().toString().equalsIgnoreCase(""))
                {
                    rel2.setText("0");
                }

                if(rel3.getText().toString().equalsIgnoreCase(""))
                {
                    rel3.setText("0");
                }

                if(rel4.getText().toString().equalsIgnoreCase(""))
                {
                    rel4.setText("0");
                }

                 int rel2count = Integer.parseInt(rel2.getText().toString());
                int rel3count = Integer.parseInt(rel3.getText().toString());
                int rel4count = Integer.parseInt(rel4.getText().toString());

                int alltotal = rel2count + rel3count + rel4count;

                  rel1.setText(Integer.toString(alltotal));




                holder.mobilenodesctxt.setText(mobile_num_lists.get(position));
                holder.emaildesctxt.setText(email_id_lists.get(position));
                holder.totalexpdesctxt.setText(total_exp_lists.get(position) + " years");




                rowView.setTag(holder);
            }
            holder = (Holder) rowView.getTag();





            return rowView;

        }


    }




    private void getCandChildRefForMobileAPI(String url) {

        pdia = new ProgressDialog(SecondLevelReferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        candidate_name_list.clear();
                        designation_list.clear();
                        status_list.clear();
                        mobile_num_list.clear();
                        email_id_list.clear();
                        total_exp_list.clear();

                        inprogresslist.clear();
                        placedlist.clear();
                        rejectedlist.clear();

                        secondlevelcountlist.clear();

                        System.out.println("RESPONSE OF getCandChildRefForMobileAPI==="+ response);



                        JSONObject ref_candidate_json = null;
                        try {
                            ref_candidate_json = new JSONObject(response);

                            JSONArray CandidateChildReferenceJSONArray = ref_candidate_json.getJSONArray("CandidateChildReference");
                            JSONArray gChildJSONArray;

                            for(int i = 0 ; i < CandidateChildReferenceJSONArray.length(); i++)
                            {

                                candidate_name_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_candidate_name"));
                                designation_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_role"));
                                status_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_isVerified"));
                                mobile_num_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_mob_number"));
                                email_id_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_email"));
                                total_exp_list.add(CandidateChildReferenceJSONArray.getJSONObject(i).getString("child_total_work_experience"));

                                System.out.println("LENGTH=====" + CandidateChildReferenceJSONArray.getJSONObject(i).getString("gchild").length());

                                if(CandidateChildReferenceJSONArray.getJSONObject(i).getString("gchild").length()==4)
                                {
                                    secondlevelcountlist.add("0");
                                }
                                else
                                {

                                    JSONArray jsonArray1 = new JSONArray(CandidateChildReferenceJSONArray.getJSONObject(i).getString("gchild"));
                                    secondlevelcountlist.add(Integer.toString(jsonArray1.length()));
                                }
                            }


                            customadap = new CustomAdap(SecondLevelReferenceActivity.this,secondlevelcountlist,candidate_name_list,designation_list,status_list,mobile_num_list,email_id_list,total_exp_list);
                            firstlevellistvw.setAdapter(customadap);




                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(Set Snackbar snackbar = Snackbar


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        pdia.dismiss();

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();


                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SecondLevelReferenceActivity.this);
        requestQueue.add(stringRequest);
    }

    public class Holder {
        TextView nametxt,designationtxt,statusdesctxt,mobilenodesctxt,emaildesctxt,totalexpdesctxt;
        TextView statustxt,mobilenotxt,emailtxt,totalexptxt,secondleveldesctxt;

    }

}
