package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/6/2018.
 */

public class ReferJobActivity extends AppCompatActivity {

    EditText nametitleedt,emaildescedt,contactnodescedt,skillsdescedt,descriptiondescedt;
    RelativeLayout saverelative;
    Typeface roboto_light_font;
    TextView descriptiontxt,headertitle;
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    String candidateidstr,usernamestr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    ImageView backiv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refer_job_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        backiv = (ImageView)findViewById(R.id.backiv);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonActivity.fromreferjobs = true;

                if(SingletonActivity.fromalljobs == true)
                {
                    finish();
                }


                if(SingletonActivity.fromnewjobs == true)
                {
                    finish();
                }

                if(SingletonActivity.fromappliedjobs == true)
                {
                    finish();
                }



            }
        });





        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        nametitleedt = (EditText)findViewById(R.id.nametitleedt);
        emaildescedt = (EditText)findViewById(R.id.emaildescedt);
        contactnodescedt = (EditText)findViewById(R.id.contactnodescedt);
        skillsdescedt = (EditText)findViewById(R.id.skillsdescedt);
        descriptiondescedt = (EditText)findViewById(R.id.descriptiondescedt);
        descriptiontxt = (TextView)findViewById(R.id.descriptiontxt);

        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        headertitle = (TextView)findViewById(R.id.headertitle);
        int job_requirement_id = Integer.parseInt(SingletonActivity.jobrequirementid);
        int new_job_requirement_id = 100 + job_requirement_id;

        String new_job_requirement_str = "JOB"+Integer.toString(new_job_requirement_id);


        headertitle.setText("REFER("+new_job_requirement_str+")");

        nametitleedt.setTypeface(roboto_light_font);
        emaildescedt.setTypeface(roboto_light_font);
        contactnodescedt.setTypeface(roboto_light_font);
        skillsdescedt.setTypeface(roboto_light_font);
        descriptiondescedt.setTypeface(roboto_light_font);
        descriptiontxt.setTypeface(roboto_light_font);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);
        usernamestr = prefs.getString("name", null);



        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                if (nametitleedt.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Name", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }



                else if (!isValidEmail(emaildescedt.getText().toString())) {
                    invalid = true;
                   /* Toast.makeText(getApplicationContext(),
                            "Please Enter Valid Email", Toast.LENGTH_SHORT)
                            .show();
*/
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(!isValidMobileNumber(contactnodescedt.getText().toString()))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile Number", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }
                else if(skillsdescedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Skills", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }




                else if (invalid == false) {



                    if(NetworkUtility.checkConnectivity(ReferJobActivity.this)){
                        String sendRefferMailURL = APIName.URL+"candidate/sendRefferMail";
                        System.out.println("SEND REFFER MAIL URL IS---"+ sendRefferMailURL);
                        sendRefferMailAPI(sendRefferMailURL,nametitleedt.getText().toString(),emaildescedt.getText().toString(),contactnodescedt.getText().toString(),skillsdescedt.getText().toString(),descriptiondescedt.getText().toString());

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }


                }

            }
        });


    }

    @Override
    public void onBackPressed() {

        SingletonActivity.fromreferjobs = true;

        if(SingletonActivity.fromalljobs == true)
        {
            finish();
        }


        if(SingletonActivity.fromnewjobs == true)
        {
            finish();
        }

        if(SingletonActivity.fromappliedjobs == true)
        {
            finish();
        }

    }

    private void sendRefferMailAPI(String url, final String nametitlestr, final String emailstr, final String contactnostr, final String skills, final String descriptionstr){

        pdia = new ProgressDialog(ReferJobActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE CANDIDATE API IS---" + response);

                        JSONObject insertupdatecandidatejson = null;
                        try {
                            insertupdatecandidatejson = new JSONObject(response);

                            System.out.println("INSERT UPDATE CANDIDATE JSON IS---" + insertupdatecandidatejson);

                            String statusstr = insertupdatecandidatejson.getString("status");
                            String msgstr = insertupdatecandidatejson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        if(SingletonActivity.fromalljobs == true) {
                                            Intent i = new Intent(ReferJobActivity.this, AllJobsActivity.class);
                                            startActivity(i);
                                        }

                                        if(SingletonActivity.fromviewalljob == true) {
                                            Intent i = new Intent(ReferJobActivity.this, ViewAllJobDetailsActivity.class);
                                            startActivity(i);
                                        }

                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("refer_upload_file","");
                params.put("name",usernamestr);
                params.put("candidate_Id",candidateidstr);
                params.put("job_title",SingletonActivity.titlename);
                params.put("job_requirement_Id",SingletonActivity.jobrequirementid);
                params.put("refer_name",nametitlestr);
                params.put("refer_email",emailstr);
                params.put("refer_contact",contactnostr);
                params.put("message",descriptionstr);
                params.put("refer_skills",skills);

                System.out.println("PARAMS OF sendRefferMailAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ReferJobActivity.this);
        requestQueue.add(stringRequest);
    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }



    public final  boolean isValidEmail(String email) {
        if (email == null) {

           /* Toast.makeText(this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();
*/

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

}
