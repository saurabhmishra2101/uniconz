package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 9/22/2017.
 */

public class OTPActivity extends Activity {

    EditText otpedt1,otpedt2,otpedt3,otpedt4,otpedt5,otpedt6;
    RelativeLayout verifyrelative;
    private CoordinatorLayout coordinatorLayout;
    String otpstr;
    ImageView backiv;
    ProgressDialog pdia;
    TextView regenerateotptxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        backiv = (ImageView)findViewById(R.id.backiv);

        regenerateotptxt = (TextView)findViewById(R.id.regenerateotptxt); 
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);


        System.out.println("OTP IS--------"+SingletonActivity.otp);

        otpedt1 = (EditText)findViewById(R.id.otpedt1);
        otpedt2 = (EditText)findViewById(R.id.otpedt2);
        otpedt3 = (EditText)findViewById(R.id.otpedt3);
        otpedt4 = (EditText)findViewById(R.id.otpedt4);
        otpedt5 = (EditText)findViewById(R.id.otpedt5);
        otpedt6 = (EditText)findViewById(R.id.otpedt6);

        verifyrelative = (RelativeLayout)findViewById(R.id.verifyrelative);

        regenerateotptxt.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if(NetworkUtility.checkConnectivity(OTPActivity.this)){
                    String GenerateOTPURL = APIName.URL+"/message/generateOTP";
                    System.out.println("GENERATE OTP URL IS---"+ GenerateOTPURL);
                    GenerateOTPAPI(GenerateOTPURL,SingletonActivity.mobilestr);
                }
                else{
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });
                
        verifyrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otpstr1 = otpedt1.getText().toString();
                String otpstr2 = otpedt2.getText().toString();
                String otpstr3 = otpedt3.getText().toString();
                String otpstr4 = otpedt4.getText().toString();
                String otpstr5 = otpedt5.getText().toString();
                String otpstr6 = otpedt6.getText().toString();

                otpstr = otpstr1+otpstr2+otpstr3+otpstr4+otpstr5+otpstr6;
                System.out.println("OTP ON VERIFY CLICK IS--------"+otpstr);


        if(NetworkUtility.checkConnectivity(OTPActivity.this)){
            String SendMailURL = APIName.URL+"/message/sendMail";
            System.out.println("SEND MAIL URL IS---"+ SendMailURL);
            SendMailAPI(SendMailURL);

        }
        else{
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }




            }
        });

        otpedt1.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    otpedt2.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        otpedt2.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    otpedt3.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        otpedt3.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    otpedt4.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        otpedt4.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    otpedt5.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        otpedt5.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    otpedt6.requestFocus();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        otpedt6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt6.setText("");
                    otpedt6.clearFocus();
                    otpedt5.requestFocus();
                }
                return false;
            }
        });
        otpedt5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt5.setText("");
                    otpedt5.clearFocus();
                    otpedt4.requestFocus();
                }
                return false;
            }
        });
        otpedt4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt4.setText("");
                    otpedt4.clearFocus();
                    otpedt3.requestFocus();
                }
                return false;
            }
        });
        otpedt3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt3.setText("");
                    otpedt3.clearFocus();
                    otpedt2.requestFocus();
                }
                return false;
            }
        });

        otpedt2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt2.setText("");
                    otpedt2.clearFocus();
                    otpedt1.requestFocus();
                }
                return false;
            }
        });

        otpedt1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    otpedt1.setText("");
                    otpedt1.requestFocus();
                }
                return false;
            }
        });
    }

    private void GenerateOTPAPI(String url,final String mobileno) {

        pdia = new ProgressDialog(OTPActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF GENERATE OTP API IS---" + response);

                        try {
                            JSONObject generateotpjson = new JSONObject(response);
                            String statusstr = generateotpjson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")){

                                Intent i = new Intent(OTPActivity.this,OTPActivity.class);
                                SingletonActivity.otp = generateotpjson.getString("otp");
                                startActivity(i);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //  Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //  util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("mob_number",mobileno);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                params.put("mob_number",mobileno);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(OTPActivity.this);
        requestQueue.add(stringRequest);
    }


    private void CandidateRegisterAPI(String url,final String attachmentstr,final String upload_condition_str,final String candidatename,final String email,final String mobilenum,final String skill,final String industry_type_Id,final String role,final String totalexpstr,final String location) {

        pdia = new ProgressDialog(OTPActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF CANDIDATE REGISTER API IS---" + response);
                        JSONObject jsonObject;
                        String statusstr = null;
                        try {
                            jsonObject = new JSONObject(response);
                            statusstr = jsonObject.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {
                                  if(SingletonActivity.otp.equalsIgnoreCase(otpstr)){
                                      Intent i = new Intent(OTPActivity.this,OTPVerifiedActivity.class);
                                      startActivity(i);
                }
                else
                {

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout,"Wrong OTP", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("candidate_name",candidatename);
                params.put("email",email);
                params.put("mob_number",mobilenum);
                params.put("candidate_skill",skill);
                params.put("industry_type_Id",industry_type_Id);
                params.put("role",role);
                params.put("total_work_experience",totalexpstr);
                params.put("message","");
                params.put("current_location",location);
                params.put("attachment",attachmentstr);
                params.put("upload_Condition",upload_condition_str);

                System.out.println("CANDIDATE REGISTER PARAMS ARE---"+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(OTPActivity.this);
        requestQueue.add(stringRequest);
    }

    private void SendMailAPI(String url) {


        pdia = new ProgressDialog(OTPActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                          pdia.dismiss();


                        System.out.println("RESPONSE OF SEND MAIL API IS---" + response);

                        String CandidateRagisterURL = APIName.URL+"/candidate/candidateRegister";
                        System.out.println("CANDIDATE REGISTER URL IS---"+ CandidateRagisterURL);
                        CandidateRegisterAPI(CandidateRagisterURL,SingletonActivity.attachmentstr,SingletonActivity.upload_condition_str,SingletonActivity.candidatenamestr,SingletonActivity.emailstr,SingletonActivity.mobilestr,SingletonActivity.skillstr,SingletonActivity.industry_type_Id,SingletonActivity.designationstr,SingletonActivity.totalexpstr,SingletonActivity.location_autocomplete);



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout,"Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("email",SingletonActivity.emailstr);
                params.put("subject","Uniconz - Sign Up");

                params.put("message","Dear "+SingletonActivity.candidatenamestr+",<br/><br/> Thank you for registering with UNICONZ - Your Career Prospects.\n" +
                        "                Uniconz team will get back to you shortly.<br/><br/>\n" +
                        "                        Thanks and Regards, <br/>Uniconz Team<br/><br/><br/>This is system auto generated message, do not reply to it!");


                System.out.println("SEND MAIL PARAMS ARE---"+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(OTPActivity.this);
        requestQueue.add(stringRequest);
    }
}
