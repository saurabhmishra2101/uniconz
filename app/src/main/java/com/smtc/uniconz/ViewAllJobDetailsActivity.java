package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/5/2018.
 */

public class ViewAllJobDetailsActivity extends AppCompatActivity {

    ImageView backiv;
    String candidateidstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    JSONArray JobRequirementJSONArray;
    RelativeLayout applyrelative,referrelative;;
    String currentDateTimeString;
    Typeface sf_font;
    TextView apply2text,positiontxt,companytxt,jobiddesctxt,demandnodesctxt,primaryskilldesctxt,secondaryskilldesctxt,requiredexpdesctxt,joblocationdesctxt,noticeperioddesctxt,basicdetailsdesctxt;
    TextView jobidtitletxt,demandnotitletxt,primaryskilltitletxt,secondaryskilltitletxt,requiredexptitletxt,joblocationtitletxt,noticeperiodtitletxt,basicdetailstitletxt;
    RelativeLayout footer1,footer2,footer3,refer3relative,applyrelative2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_all_job_details_coordinator);



        sf_font = Typeface.createFromAsset(getAssets(), "sfuidisplay/SF.ttf");

        backiv = (ImageView)findViewById(R.id.backiv);
        referrelative = (RelativeLayout)findViewById(R.id.referrelative);

        footer3 = (RelativeLayout)findViewById(R.id.footer3);
        refer3relative= (RelativeLayout)findViewById(R.id.refer3relative);

        apply2text = (TextView)findViewById(R.id.apply2text);
        positiontxt = (TextView)findViewById(R.id.positiontxt);
        companytxt  = (TextView)findViewById(R.id.companytxt);
        jobiddesctxt = (TextView)findViewById(R.id.jobiddesctxt);
        demandnodesctxt = (TextView)findViewById(R.id.demandnodesctxt);
        primaryskilldesctxt = (TextView)findViewById(R.id.primaryskilldesctxt);
        secondaryskilldesctxt =  (TextView)findViewById(R.id.secondaryskilldesctxt);
        requiredexpdesctxt =  (TextView)findViewById(R.id.requiredexpdesctxt);
        joblocationdesctxt  =  (TextView)findViewById(R.id.joblocationdesctxt);
        noticeperioddesctxt   =  (TextView)findViewById(R.id.noticeperioddesctxt);
        basicdetailsdesctxt  =  (TextView)findViewById(R.id.basicdetailsdesctxt);

        jobidtitletxt = (TextView)findViewById(R.id.jobidtitletxt);
        demandnotitletxt = (TextView)findViewById(R.id.demandnotitletxt);
        primaryskilltitletxt =  (TextView)findViewById(R.id.primaryskilltitletxt);
        secondaryskilltitletxt=  (TextView)findViewById(R.id.secondaryskilltitletxt);
        requiredexptitletxt=  (TextView)findViewById(R.id.requiredexptitletxt);
        joblocationtitletxt =  (TextView)findViewById(R.id.joblocationtitletxt);
        noticeperiodtitletxt =  (TextView)findViewById(R.id.noticeperiodtitletxt);
        basicdetailstitletxt =  (TextView)findViewById(R.id.basicdetailstitletxt);

        positiontxt.setTypeface(sf_font);
        companytxt.setTypeface(sf_font);
        jobiddesctxt.setTypeface(sf_font);
        demandnodesctxt.setTypeface(sf_font);
        primaryskilldesctxt.setTypeface(sf_font);
        secondaryskilldesctxt.setTypeface(sf_font);
        requiredexpdesctxt.setTypeface(sf_font);
        joblocationdesctxt.setTypeface(sf_font);
        noticeperioddesctxt.setTypeface(sf_font);
        basicdetailsdesctxt.setTypeface(sf_font);

        jobidtitletxt.setTypeface(sf_font);
        demandnotitletxt.setTypeface(sf_font);
        primaryskilltitletxt.setTypeface(sf_font);
        secondaryskilltitletxt.setTypeface(sf_font);
        requiredexptitletxt.setTypeface(sf_font);
        joblocationtitletxt.setTypeface(sf_font);
        noticeperiodtitletxt.setTypeface(sf_font);
        basicdetailstitletxt.setTypeface(sf_font);


        applyrelative = (RelativeLayout)findViewById(R.id.applyrelative);
        applyrelative2 = (RelativeLayout)findViewById(R.id.apply2relative);
        footer2 =  (RelativeLayout)findViewById(R.id.footer2);
        footer1 = (RelativeLayout)findViewById(R.id.footer1);

        if(SingletonActivity.fromrecommendedjobs == true)
        {
            applyrelative.setVisibility(View.GONE);
            applyrelative2.setVisibility(View.VISIBLE);
            footer2.setVisibility(View.VISIBLE);
            footer1.setVisibility(View.GONE);
            footer3.setVisibility(View.GONE);
        }
        if(SingletonActivity.fromnewjobs == true)
        {
            applyrelative.setVisibility(View.VISIBLE);
            applyrelative2.setVisibility(View.GONE);
            footer2.setVisibility(View.GONE);
            footer1.setVisibility(View.VISIBLE);
            footer3.setVisibility(View.GONE);
        }
        if(SingletonActivity.fromappliedjobs == true)
        {
            applyrelative.setVisibility(View.GONE);
            applyrelative2.setVisibility(View.GONE);
            footer2.setVisibility(View.GONE);
            footer1.setVisibility(View.GONE);
            footer3.setVisibility(View.VISIBLE);
        }

        if(SingletonActivity.fromalljobs == true)
        {
            applyrelative.setVisibility(View.VISIBLE);
            applyrelative2.setVisibility(View.GONE);
            footer2.setVisibility(View.GONE);
            footer1.setVisibility(View.VISIBLE);
            footer3.setVisibility(View.GONE);
        }

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());


        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(SingletonActivity.fromalljobs == true) {

                    SingletonActivity.fromalljobs = false;

                    if (SingletonActivity.fromreferjobs == true) {
                        Intent i = new Intent(ViewAllJobDetailsActivity.this, AllJobsActivity.class);
                        SingletonActivity.fromreferjobs = false;
                        startActivity(i);
                    } else {
                        finish();
                    }
                }
                if(SingletonActivity.fromrecommendedjobs == true) {

                    SingletonActivity.fromrecommendedjobs = false;

                    if (SingletonActivity.fromreferjobs == true) {
                        Intent i = new Intent(ViewAllJobDetailsActivity.this, RecommendedJobsActivity.class);
                        SingletonActivity.fromreferjobs = false;
                        startActivity(i);
                    } else {
                        finish();
                    }

                }

                if(SingletonActivity.fromnewjobs == true) {

                    SingletonActivity.fromnewjobs = false;

                    if (SingletonActivity.fromreferjobs == true) {
                        Intent i = new Intent(ViewAllJobDetailsActivity.this, NewJobsFromRecruitersActivity.class);
                        SingletonActivity.fromreferjobs = false;
                        startActivity(i);
                    } else {
                        finish();
                    }

                }

                if(SingletonActivity.fromappliedjobs == true) {

                    SingletonActivity.fromappliedjobs = false;

                    if (SingletonActivity.fromreferjobs == true) {
                        Intent i = new Intent(ViewAllJobDetailsActivity.this, JobApplyUpdatesActivity.class);
                        SingletonActivity.fromreferjobs = false;
                        startActivity(i);
                    } else {
                        finish();
                    }

                }

            }
        });

        if (NetworkUtility.checkConnectivity(ViewAllJobDetailsActivity.this)) {
            String ViewJobRequirementURL = APIName.URL + "/candidate/getjobrequirement?candidate_Id=" + candidateidstr + "&job_requirement_Id="+ SingletonActivity.jobrequirementid;
            System.out.println("VIEW JOB REQUIREMENT URL IS---" + ViewJobRequirementURL);
            ViewJobRequirementAPI(ViewJobRequirementURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }




        applyrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ViewAllJobDetailsActivity.this,ApplyJobActivity.class);
             //   SingletonActivity.job_requirement_id = job_requirement_id;
                startActivity(i);



               /* if (NetworkUtility.checkConnectivity(ViewAllJobDetailsActivity.this)) {
                    String ApplyJobURL = APIName.URL + "candidate/applyjob?candidate_Id=" + candidateidstr;
                    System.out.println("APPLY JOB URL IS---" + ApplyJobURL);
                    ApplyJobAPI(ApplyJobURL);

                } else {


                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }*/

            }
        });

        applyrelative2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkUtility.checkConnectivity(ViewAllJobDetailsActivity.this)) {
                    String ApplyJobURL = APIName.URL + "/candidate/applyjob?candidate_Id=" + candidateidstr;
                    System.out.println("APPLY JOB URL IS---" + ApplyJobURL);
                    ApplyJobAPI(ApplyJobURL);

                } else {


                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

            }
        });



        referrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingletonActivity.fromviewalljob = true;
                Intent i = new Intent(ViewAllJobDetailsActivity.this,ReferJobActivity.class);

                try {
                    SingletonActivity.titlename = JobRequirementJSONArray.getJSONObject(0).getString("title");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if(SingletonActivity.fromalljobs == true) {

            SingletonActivity.fromalljobs = false;

            if (SingletonActivity.fromreferjobs == true) {
                Intent i = new Intent(ViewAllJobDetailsActivity.this, AllJobsActivity.class);
                SingletonActivity.fromreferjobs = false;
                startActivity(i);
            } else {
                finish();
            }
        }
        if(SingletonActivity.fromrecommendedjobs == true) {

            SingletonActivity.fromrecommendedjobs = false;

            if (SingletonActivity.fromreferjobs == true) {
                Intent i = new Intent(ViewAllJobDetailsActivity.this, RecommendedJobsActivity.class);
                SingletonActivity.fromreferjobs = false;
                startActivity(i);
            } else {
                finish();
            }

        }

        if(SingletonActivity.fromnewjobs == true) {

            SingletonActivity.fromnewjobs = false;

            if (SingletonActivity.fromreferjobs == true) {
                Intent i = new Intent(ViewAllJobDetailsActivity.this, NewJobsFromRecruitersActivity.class);
                SingletonActivity.fromreferjobs = false;
                startActivity(i);
            } else {
                finish();
            }

        }

        if(SingletonActivity.fromappliedjobs == true) {

            SingletonActivity.fromappliedjobs = false;

            if (SingletonActivity.fromreferjobs == true) {
                Intent i = new Intent(ViewAllJobDetailsActivity.this, JobApplyUpdatesActivity.class);
                SingletonActivity.fromreferjobs = false;
                startActivity(i);
            } else {
                finish();
            }

        }


    }

    private void ApplyJobAPI(String url){

        pdia = new ProgressDialog(ViewAllJobDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF APPLY JOB API IS---" + response);

                        JSONObject applyjobjson = null;
                        try {
                            applyjobjson = new JSONObject(response);

                            System.out.println("APPLY JOB JSON IS---" + applyjobjson);

                            String statusstr = applyjobjson.getString("status");
                            String msgstr = applyjobjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_Id",candidateidstr);
                params.put("job_requirement_Id",SingletonActivity.jobrequirementid);
                params.put("modified",currentDateTimeString);


                System.out.println("PARAMS OF ApplyJob API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ViewAllJobDetailsActivity.this);
        requestQueue.add(stringRequest);
    }

    private void ViewJobRequirementAPI(String url) {

        pdia = new ProgressDialog(ViewAllJobDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF VIEW ALL JOBS API IS---" + response);




                        JSONObject getjobrequirementjson = null;
                        try {
                            getjobrequirementjson = new JSONObject(response);

                             JobRequirementJSONArray = getjobrequirementjson.getJSONArray("Jobrequirement");



                                if(JobRequirementJSONArray.getJSONObject(0).getString("status").equalsIgnoreCase("0"))
                                {

                                 positiontxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("title"));
                                    companytxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("hirer_name"));


                                        int job_requirement_id = Integer.parseInt(JobRequirementJSONArray.getJSONObject(0).getString("job_requirement_Id"));
                                        int new_job_requirement_id = 100 + job_requirement_id;

                                        String new_job_requirement_str = "JOB"+Integer.toString(new_job_requirement_id);
                                       // holder.jobiddesctxt.setText(new_job_requirement_str);

                                        jobiddesctxt.setText(new_job_requirement_str);


                                    JSONArray JobSkillsJSONArray = JobRequirementJSONArray.getJSONObject(0).getJSONArray("Jobskills");
                                    if(JobSkillsJSONArray.getJSONObject(0).getString("proficiency_level").equalsIgnoreCase("Advanced")||JobSkillsJSONArray.getJSONObject(0).getString("proficiency_level").equalsIgnoreCase("Intermediate"))
                                    {
                                        primaryskilldesctxt.setText(JobSkillsJSONArray.getJSONObject(0).getString("skill_Name"));
                                        secondaryskilltitletxt.setVisibility(View.GONE);
                                        secondaryskilldesctxt.setVisibility(View.GONE);

                                    }
                                    else
                                    {

                                        primaryskilltitletxt.setVisibility(View.GONE);
                                        primaryskilldesctxt.setVisibility(View.GONE);
                                        secondaryskilldesctxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("skill_Name"));

                                    }

                                    demandnodesctxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("demandNos"));
                                //    primaryskilldesctxt.setText();
                                  //  secondaryskilldesctxt.setText();

                                    if(!JobRequirementJSONArray.getJSONObject(0).getString("total_experience").equalsIgnoreCase("")) {
                                        requiredexpdesctxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("total_experience")+" years");
                                    }
                                    joblocationdesctxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("location"));
                                    if(!JobRequirementJSONArray.getJSONObject(0).getString("notice_period").equalsIgnoreCase("")) {
                                        noticeperioddesctxt.setText(JobRequirementJSONArray.getJSONObject(0).getString("notice_period")+" days");
                                    }

                                    String job_req_str = JobRequirementJSONArray.getJSONObject(0).getString("job_description");
                                    String formated_job_req_str = Html.fromHtml(job_req_str).toString();
                                    basicdetailsdesctxt.setText(formated_job_req_str);
                                    if(JobRequirementJSONArray.getJSONObject(0).getString("JobApplyStatus").equalsIgnoreCase("Applied"))
                                    {
                                        applyrelative.setEnabled(false);
                                        applyrelative.setBackgroundColor(Color.parseColor("#e4e4e4"));

                                        applyrelative2.setEnabled(false);
                                        applyrelative2.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        apply2text.setText("A P P L I E D");
                                    }

                                }





                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ViewAllJobDetailsActivity.this);
        requestQueue.add(stringRequest);
    }
}
