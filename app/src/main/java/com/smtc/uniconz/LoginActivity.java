package com.smtc.uniconz;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by 10161 on 9/13/2017.
 */

public class LoginActivity extends Activity {

    UtilsDialog util = new UtilsDialog();
    Typeface roboto_light_font,roboto_medium_font;
    EditText emailedt,pwdedt;
    TextView emailtitletxtvw,passwordtitletxtvw,buildtxtvw,forgetpwdtxt;
    TextView signuptxtvw;
    RelativeLayout loginrelative;
    private CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    AlertDialog b;
    TextView wehavesenttxtvw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        buildtxtvw = (TextView)findViewById(R.id.buildtxtvw);
        forgetpwdtxt = (TextView)findViewById(R.id.forgetpwdtxt);


        /* buildtxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  Intent i = new Intent(LoginActivity.this,DetailsPageActivity.class);
                 startActivity(i);
            }
        });*/

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);


        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        roboto_medium_font = Typeface.createFromAsset(getAssets(),  "roboto/Roboto-Medium.ttf");

        emailedt = (EditText)findViewById(R.id.emailedt);
        pwdedt = (EditText)findViewById(R.id.pwdedt);
        emailtitletxtvw = (TextView)findViewById(R.id.emailtitletxtvw);
        passwordtitletxtvw = (TextView)findViewById(R.id.passwordtitletxtvw);
        signuptxtvw = (TextView)findViewById(R.id.signuptxtvw);

        //Saurabh Credential
    /*  emailedt.setText("saurabhm.android@gmail.com");
        pwdedt.setText("admin");*/

        //Snehal Account--
       /* emailedt.setText("kadamsnehal1115@gmail.com");
        pwdedt.setText("123456789sk");*/


        //Chandra Bogesh Credential:
      /*  emailedt.setText("saurabh.andro453@gmail.com");
        pwdedt.setText("admin");
*/

        //Sireesha Credential
      /*  emailedt.setText("sireesha.siddineni@skill-mine.com");
        pwdedt.setText("admin@123");*/


        //Randeep Credential
      /*emailedt.setText("shashank_chinks@yahoo.com");
        pwdedt.setText("admin@123");*/


        emailedt.setTypeface(roboto_light_font);
        pwdedt.setTypeface(roboto_light_font);
        signuptxtvw.setTypeface(roboto_medium_font);
        buildtxtvw.setTypeface(roboto_light_font);
        forgetpwdtxt.setTypeface(roboto_medium_font);

        forgetpwdtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgetPwdDialog();
            }
        });

        signuptxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(i);
            }
        });

        loginrelative = (RelativeLayout)findViewById(R.id.loginrelative);

        loginrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean invalid = false;

                String email = emailedt.getText().toString();
                String password = pwdedt.getText().toString();

                System.out.println("EMAIL IS-----" + email);

                if (!isValidEmail(email)) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                } else if (!isValidPassword(password)) {
                    invalid = true;
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid 5 or More Character Password", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                } else if (invalid == false) {

                    if (NetworkUtility.checkConnectivity(LoginActivity.this)) {
                        String UserLoginURL = APIName.URL + "/user/userLogin";
                        System.out.println("USER LOGIN URL IS---" + UserLoginURL);
                        UserLoginAPI(UserLoginURL,email,password);

                    } else {
                        // util.dialog(LoginActivity.this, "Please check your internet connection.");

                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }
                }
            }});

            }

    public void showForgetPwdDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.forget_pwd_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);



        final EditText forgetpwdemailedt = (EditText) dialogView.findViewById(R.id.emailedt);

        wehavesenttxtvw = (TextView) dialogView.findViewById(R.id.wehavesenttxtvw);


        dialogBuilder.setTitle("Enter your email");
       // dialogBuilder.setMessage("Please enter your mobile number");

        dialogBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                //Do stuff, possibly set wantToCloseDialog to true then...

            }
        });

        b = dialogBuilder.create();
        b.show();


        Button pbutton = b.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#FF1BA46B"));

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    b.dismiss();

                boolean invalid = false;

                if ((!isValidEmail(forgetpwdemailedt.getText().toString())) ) {
                    invalid = true;

                    wehavesenttxtvw.setText("Please Enter Email");
                    wehavesenttxtvw.setTextColor(Color.parseColor("#ff0000"));

                }else if (invalid == false) {



                    if(NetworkUtility.checkConnectivity(LoginActivity.this)){
                        String ResetPasswordURL = APIName.URL+"/user/resetPassword";
                        System.out.println("RESET PASSWORD URL IS---"+ ResetPasswordURL);
                        ResetPasswordAPI(ResetPasswordURL,forgetpwdemailedt.getText().toString());
                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }

                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });


    }

    private void ResetPasswordAPI(String url,final String email) {

        pdia = new ProgressDialog(LoginActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        wehavesenttxtvw.setText("We have sent you a new password");
                        wehavesenttxtvw.setTextColor(Color.parseColor("#FF1BA46B"));

                        System.out.println("RESPONSE OF RESET PASSWORD API IS---" + response);

                      try {
                            JSONObject resetpasswordjson = new JSONObject(response);
                            String statusstr = resetpasswordjson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")){
                               // String subjectstr = resetpasswordjson.getString("subject");

                                b.dismiss();

                                String messagestr = resetpasswordjson.getString("message");

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);
                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();



                               /* if(NetworkUtility.checkConnectivity(LoginActivity.this)){
                                    String SendMailURL = APIName.URL+"/message/sendMail";
                                    System.out.println("SEND MAIL URL IS---"+ SendMailURL);
                                    SendMailAPI(SendMailURL,email);
                                }
                                else{
                                    Snackbar snackbar = Snackbar
                                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                                    View view = snackbar.getView();
                                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                    tv.setTextColor(Color.RED);
                                    snackbar.show();
                                }
*/

                            }
                            else
                            {
                                String messagestr = resetpasswordjson.getString("message");
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);
                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //  Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //  util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                params.put("email",email);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }

    private void SendMailAPI(String url,final String email) {

        pdia = new ProgressDialog(LoginActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF SEND MAIL API IS---" + response);

                        try {
                            JSONObject resetpasswordjson = new JSONObject(response);
                            String statusstr = resetpasswordjson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")){
                               b.dismiss();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //  Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //  util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email",email);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }


    public final  boolean isValidEmail(String email) {
        if (email == null) {

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public  final boolean isValidPassword(String password) {
        return password.length() >= 5;
    }




    private void UserLoginAPI(String url, final String email,final String password) {

        pdia = new ProgressDialog(LoginActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();



        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();
                        System.out.println("RESPONSE OF USER LOGIN API IS---" + response);



                        JSONObject userloginjson = null;
                        try {
                            userloginjson = new JSONObject(response);
                            String message = userloginjson.getString("message");

                            JSONObject Userjson = userloginjson.getJSONObject("User");

                            String candidateid = Userjson.getString("candidate_Id");
                            String username = Userjson.getString("name");

                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("candidate_id", candidateid);
                            editor.putString("pwd", pwdedt.getText().toString());
                            editor.putString("name",username);


                            editor.commit();

                           /* Intent i = new Intent(LoginActivity.this,CandidateDetailsActivity.class);
                            startActivity(i);*/

                            Intent i = new Intent(LoginActivity.this,DashboardActivity.class);
                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                        System.out.println("USER LOGIN JSON IS---" + userloginjson);




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                           // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("email",email);
                params.put("password",password);




                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent i = new Intent(LoginActivity.this,LoginActivity.class);
        startActivity(i);

    }

}
