package com.smtc.uniconz;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/9/2018.
 */

public class DashboardActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    CoordinatorLayout coordinatorLayout;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    SessionManager sessionManager;
    Typeface roboto_light_font,roboto_medium_font,bebas_font,sf_font;
    ImageView profileiv,cancel_dialog;
    private static final int CAPTURE_IMAGE_CAPTURE_CODE = 1888;
    private static final int SELECT_PICTURE = 1;
    TextView upload_from_memory,click_image_from_camera,candidatenametxt,designationtxt,percenttxt,lastupdatedtxt,profilecompletenesstxt;
    ProgressBar progress;
    String candidateidstr;
    ProgressDialog pdia;
    JSONArray CandProfSummary;
    TextView countrecommendeddesctxt,recommendedjobtxt,countnewjobsdesctxt,newjobtxt,countjobapplyupdatesdesctxt,jobapplyupdatestxt,jobapplypostdesctxt,jobapplycmpnydesctxt,jobapplystatusdesctxt,seealltxt;
    RelativeLayout personaldetrel,newjobrelative,recommendedjobrelative,jobapplyupdatesrelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_details);

        candidatenametxt = (TextView)findViewById(R.id.candidatenametxt);
        designationtxt= (TextView)findViewById(R.id.designationtxt);
        percenttxt= (TextView)findViewById(R.id.percenttxt);
        profilecompletenesstxt = (TextView)findViewById(R.id.lastupdatedtxt);
       lastupdatedtxt= (TextView)findViewById(R.id.lastupdatedtxt);

       countrecommendeddesctxt = (TextView)findViewById(R.id.countrecommendeddesctxt);
       recommendedjobtxt  = (TextView)findViewById(R.id.recommendedjobtxt);
       countnewjobsdesctxt = (TextView)findViewById(R.id.countnewjobsdesctxt);
       newjobtxt = (TextView)findViewById(R.id.newjobtxt);
       countjobapplyupdatesdesctxt = (TextView)findViewById(R.id.countjobapplyupdatesdesctxt);
       jobapplyupdatestxt = (TextView)findViewById(R.id.jobapplyupdatestxt);
       jobapplypostdesctxt = (TextView)findViewById(R.id.jobapplypostdesctxt);
       jobapplycmpnydesctxt = (TextView)findViewById(R.id.jobapplycmpnydesctxt);
       jobapplystatusdesctxt = (TextView)findViewById(R.id.jobapplystatusdesctxt);
       seealltxt = (TextView)findViewById(R.id.seealltxt);
       personaldetrel = (RelativeLayout)findViewById(R.id.personaldetrel);
       newjobrelative = (RelativeLayout)findViewById(R.id.newjobrelative);
       recommendedjobrelative = (RelativeLayout)findViewById(R.id.recommendedjobrelative);
        profileiv = (ImageView) findViewById(R.id.profileiv);

        jobapplyupdatesrelative = (RelativeLayout)findViewById(R.id.jobapplyupdatesrelative);

        personaldetrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(DashboardActivity.this,CandidateDetailsActivity.class);
                startActivity(i);
            }
        });



        progress = (ProgressBar)findViewById(R.id.progress);

        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        roboto_medium_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Medium.ttf");
        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");
        sf_font = Typeface.createFromAsset(getAssets(), "sfuidisplay/SF.ttf");

        candidatenametxt.setTypeface(sf_font);
        designationtxt.setTypeface(sf_font);
        percenttxt.setTypeface(sf_font);
        lastupdatedtxt.setTypeface(sf_font);
        profilecompletenesstxt.setTypeface(sf_font);

        countrecommendeddesctxt.setTypeface(sf_font);
        recommendedjobtxt.setTypeface(sf_font);
        countnewjobsdesctxt.setTypeface(sf_font);
        newjobtxt.setTypeface(sf_font);
        countjobapplyupdatesdesctxt.setTypeface(sf_font);
        jobapplyupdatestxt.setTypeface(sf_font);
        jobapplypostdesctxt.setTypeface(sf_font);
        jobapplycmpnydesctxt.setTypeface(sf_font);
        jobapplystatusdesctxt.setTypeface(sf_font);
        seealltxt.setTypeface(sf_font);




        profileiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog mDialog = new Dialog(DashboardActivity.this, R.style.DialogSlideAnim);
                mDialog.setCancelable(false);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mDialog.setContentView(R.layout.dialog_upload_image);
                cancel_dialog = (ImageView) mDialog
                        .findViewById(R.id.cancel_dialog);
                upload_from_memory = (TextView) mDialog
                        .findViewById(R.id.upload_from_memory);
                click_image_from_camera = (TextView) mDialog
                        .findViewById(R.id.click_image_from_camera);
                upload_from_memory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                        mDialog.dismiss();

                    }
                });
                click_image_from_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                        mDialog.dismiss();

                    }
                });
                cancel_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();

            }
        });




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        if (NetworkUtility.checkConnectivity(DashboardActivity.this)) {
            String GetCandidateAllDetailsURL = APIName.URL + "/candidate/getCandidateAllDetails?candidate_Id=" + candidateidstr;
            System.out.println("GET CANDIDATE ALL DETAILS URL IS---" + GetCandidateAllDetailsURL);
            GetCandidateAllDetailsAPI(GetCandidateAllDetailsURL);

        } else {
            // util.dialog(LoginActivity.this, "Please check your internet connection.");

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }


    }

    @Override
    public void onBackPressed() {

    }

    private void GetCandidateAllDetailsAPI(String url) {

        pdia = new ProgressDialog(DashboardActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF GET CANDIDATE ALL DETAILS API IS---" + response);

                        JSONObject candidatedetailsjson = null;
                        try {
                            candidatedetailsjson = new JSONObject(response);

                            final JSONObject CandidateDetails = candidatedetailsjson.getJSONObject("CandidateAllDetails");

                            String candidatenamestr = CandidateDetails.getString("candidate_name");
                            candidatenametxt.setText(candidatenamestr);


                            String lastupdatedstr = CandidateDetails.getString("last_modified");

                            //  String[] dateTimeArray = lastupdatedstr.split(" ");

                            lastupdatedtxt.setText("Last Updated: " + lastupdatedstr);
                            //lastupdatedtxt.setText("Last Updated: "+dateTimeArray[0]);


                            if (CandidateDetails.getString("CandProfSummary").length() == 4)
                            //your rest of codes
                            {
                                designationtxt.setText("Designation Not Mentioned");


                            } else {


                                CandProfSummary = CandidateDetails.getJSONArray("CandProfSummary");
                                System.out.println("JSONARRAY CANDPROFSUMMARY=====" + CandProfSummary);

                                String designationstr = CandProfSummary.getJSONObject(0).getString("latest_desgination");
                                designationtxt.setText(designationstr);

                            }


                            if (NetworkUtility.checkConnectivity(DashboardActivity.this)) {
                                String GetJobCountsURL = APIName.URL + "/candidate/getJobCounts?candidate_Id=" + candidateidstr;
                                System.out.println("GET JOB COUNTS URL IS---" + GetJobCountsURL);
                                GetJobCountAPI(GetJobCountsURL);

                            } else {


                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetJobCountAPI(String url) {

        pdia = new ProgressDialog(DashboardActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET JOB COUNTS API IS---" + response);

                        JSONObject candkeyskillsjson = null;
                        try {
                            candkeyskillsjson = new JSONObject(response);

                            if(candkeyskillsjson.getString("status").equalsIgnoreCase("true")) {


                                countrecommendeddesctxt.setText(candkeyskillsjson.getString("CandidateRecommandedJob"));
                                countnewjobsdesctxt.setText(candkeyskillsjson.getString("NewJobCounts"));

                                JSONObject CandidateJSON = candkeyskillsjson.getJSONObject("Candidate");
                                System.out.println("CandidateJobApplyJSONArrayintval length is---" + CandidateJSON.getString("CandidateJobApply").length());

                                if(CandidateJSON.getString("CandidateJobApply").length() == 4)
                                {
                                    jobapplyupdatesrelative.setVisibility(View.GONE);
                                }
                                else
                                {
                                    jobapplyupdatesrelative.setVisibility(View.VISIBLE);

                                    JSONArray CandidateJobApplyJSONArray = CandidateJSON.getJSONArray("CandidateJobApply");


                                    int CandidateJobApplyJSONArrayintval = CandidateJobApplyJSONArray.length();


                                    int len = CandidateJobApplyJSONArray.length();

                                /*jobapplypostdesctxt.setText(CandidateJobApplyJSONArray.getJSONObject(0).getString("title"));
                                jobapplycmpnydesctxt.setText(CandidateJobApplyJSONArray.getJSONObject(0).getString("hirer_name"));*/
                                    countjobapplyupdatesdesctxt.setText(Integer.toString(len));
                                }



                                   /* System.out.println("candidate jsonarray length is---" + len);

                                    if (len == 4) {
                                        jobapplyupdatesrelative.setVisibility(View.GONE);
                                    } else {
                                        jobapplyupdatesrelative.setVisibility(View.VISIBLE);
                                    }
*/

                                if (countrecommendeddesctxt.getText().toString().equalsIgnoreCase("0")) {
                                    recommendedjobrelative.setEnabled(false);
                                } else {

                                    recommendedjobrelative.setEnabled(true);
                                    recommendedjobrelative.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent i = new Intent(DashboardActivity.this, RecommendedJobsActivity.class);
                                            startActivity(i);
                                        }
                                    });
                                }

                                if (countnewjobsdesctxt.getText().toString().equalsIgnoreCase("0")) {
                                    newjobrelative.setEnabled(false);
                                } else {
                                    newjobrelative.setEnabled(true);

                                    newjobrelative.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent i = new Intent(DashboardActivity.this, NewJobsFromRecruitersActivity.class);
                                            startActivity(i);
                                        }
                                    });
                                }


                                seealltxt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Intent i = new Intent(DashboardActivity.this, JobApplyUpdatesActivity.class);
                                        startActivity(i);
                                    }
                                });
                            }
                            else
                            {
                                countrecommendeddesctxt.setText("0");
                                countnewjobsdesctxt.setText("0");
                                jobapplyupdatesrelative.setVisibility(View.GONE);

                                if(countrecommendeddesctxt.getText().toString().equalsIgnoreCase("0"))
                                {
                                    recommendedjobrelative.setEnabled(false);
                                }


                                if(countnewjobsdesctxt.getText().toString().equalsIgnoreCase("0"))
                                {
                                    newjobrelative.setEnabled(false);
                                }


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {


                                        countrecommendeddesctxt.setText("0");
                                        countnewjobsdesctxt.setText("0");
                                        jobapplyupdatesrelative.setVisibility(View.GONE);

                                        if(countrecommendeddesctxt.getText().toString().equalsIgnoreCase("0"))
                                        {
                                            recommendedjobrelative.setEnabled(false);
                                        }


                                        if(countnewjobsdesctxt.getText().toString().equalsIgnoreCase("0"))
                                        {
                                            newjobrelative.setEnabled(false);
                                        }




                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.candidate_profile) {

            Intent i = new Intent(DashboardActivity.this,CandidateDetailsActivity.class);
            startActivity(i);

            // Handle the camera action
        } else if (id == R.id.all_jobs) {

            Intent i = new Intent(DashboardActivity.this,AllJobsActivity.class);
            startActivity(i);

        } else if (id == R.id.referral) {

            Intent i = new Intent(DashboardActivity.this,ReferralActivity.class);
            startActivity(i);

        } else if (id == R.id.my_account) {

            Intent i = new Intent(DashboardActivity.this,MyAccountActivity.class);
            startActivity(i);

        } else if (id == R.id.change_password) {

            Intent i = new Intent(DashboardActivity.this,ChangePasswordActivity.class);
            startActivity(i);

        } else if (id == R.id.logout) {

            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            editor.putBoolean("login",false);


            editor.clear();
            editor.commit();

            sessionManager.logoutUser();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
