package com.smtc.uniconz;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/23/2018.
 */

public class MyAccountActivity extends AppCompatActivity {

    ImageView backiv;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr,emailVerifiedstr,emailstr,mobilestr,namestr,mobVerifiedstr;
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    TextView  emaildesctxt,mobiledescdesctxt,updateemailmobiletxt;
    RelativeLayout emailrelative,mobilerelative;
    AlertDialog b;
    Typeface bebas_font;
    EditText otpedt,mobileedt;
    Boolean isMobVerify = false;
    String otpstr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");

        emailrelative = (RelativeLayout)findViewById(R.id.emailrelative);
        mobilerelative = (RelativeLayout)findViewById(R.id.mobilerelative);



        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        emaildesctxt = (TextView) findViewById(R.id.emaildesctxt);
        mobiledescdesctxt = (TextView)findViewById(R.id.mobiledescdesctxt);
        updateemailmobiletxt = (TextView)findViewById(R.id.updateemailmobiletxt);
        updateemailmobiletxt.setText("UPDATE  EMAIL  &  MOBILE  NUMBER");
        updateemailmobiletxt.setTypeface(bebas_font);

        backiv = (ImageView)findViewById(R.id.backiv);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MyAccountActivity.this,DashboardActivity.class);
                startActivity(i);
            }
        });

        if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
            String getUserCredentialURL = APIName.URL + "/candidate/getUserCredential?candidate_Id="+candidateidstr;
            System.out.println("GET USER CREDENTIAL URL IS---" + getUserCredentialURL);
            getUserCredentialAPI(getUserCredentialURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        emailrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateVerifyEmailDialog();

            }
        });


        mobilerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateVerifyMobileNoDialog();

            }
        });

    }

    @Override
    public void onBackPressed() {


        Intent i = new Intent(MyAccountActivity.this,DashboardActivity.class);
        startActivity(i);
    }

    public void updateVerifyMobileNoDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_verify_mobile, null);
        dialogBuilder.setView(dialogView);

        mobileedt = (EditText) dialogView.findViewById(R.id.mobileedt);
        otpedt = (EditText) dialogView.findViewById(R.id.otpedt);

        otpedt.setVisibility(View.GONE);

       // System.out.println("EMAIL VERIFIED STRING IS---"+ emailVerifiedstr);

        //emailedt.setText(emailstr);


        dialogBuilder.setMessage("Update & Verify Mobile");



        dialogBuilder.setPositiveButton("Verify", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                boolean invalid = false;

                if (!isValidMobileNumber(mobileedt.getText().toString())) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile No.", Snackbar.LENGTH_LONG);
                    View views = snackbar.getView();
                    TextView tv = (TextView)views.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }


                else if (invalid == false) {

                    if(b.getButton(AlertDialog.BUTTON_POSITIVE).getText().toString().equalsIgnoreCase("Verify")) {


                        if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
                            String GenerateOTPURL = APIName.URL + "/message/generateOTP";
                            System.out.println("GENERATE OTP URL IS---" + GenerateOTPURL);
                            GenerateOTPAPI(GenerateOTPURL, mobileedt.getText().toString());
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                        b.getButton(AlertDialog.BUTTON_POSITIVE).setText("Update");
                    }
                    else {

                        if (otpedt.getText().toString().equalsIgnoreCase(otpstr)) {
                            if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
                                String updateEmailMobileURL = APIName.URL + "/candidate/updateEmailMobile?candidate_Id=" + candidateidstr;
                                System.out.println("UPDATE EMAIL MOBILE URL IS---" + updateEmailMobileURL);
                                updateEmailMobile1API(updateEmailMobileURL, mobileedt.getText().toString());
                            } else {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                                View views = snackbar.getView();
                                TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                            b.dismiss();
                        }
                        else
                        {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "OTP Mismatch!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    b.dismiss();

              /*  boolean invalid = false;

                if ((!isValidEmail(emailedt.getText().toString())) ) {
                    invalid = true;
                    wantToCloseDialog = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Email !!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }else if (invalid == false) {
                    wantToCloseDialog = false;

                    if(NetworkUtility.checkConnectivity(MyAccountActivity.this)){
                        String verifyEmailURL = APIName.URL+"/candidate/verifyEmail";
                        System.out.println("VERIFY EMAIL URL IS---"+ verifyEmailURL);
                        verifyEmailAPI(verifyEmailURL,emailedt.getText().toString());
                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }
*/
            }
        });
        final  AlertDialog b = dialogBuilder.create();
        b.show();

        if(mobVerifiedstr.equalsIgnoreCase("1"))
        {
           b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
        }
        else
        {
            b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
        }


        mobileedt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);

                if(mobiledescdesctxt.getText().toString().equals(mobileedt.getText().toString()))
                {
                    b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                    mobileedt.setTextColor(Color.parseColor("#E4E4E4"));
                }

            }


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(s)) {
                    // call method
                    b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    if(mobiledescdesctxt.getText().toString().equals(mobileedt.getText().toString()))
                    {
                        b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                       // mobileedt.setTextColor(Color.parseColor("#E4E4E4"));
                    }

                }
            }
        });

        Button postivebtn = b.getButton(AlertDialog.BUTTON_POSITIVE);
        postivebtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                boolean invalid = false;

                if (!isValidMobileNumber(mobileedt.getText().toString())) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile No.", Snackbar.LENGTH_LONG);
                    View views = snackbar.getView();
                    TextView tv = (TextView)views.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }


                else if (invalid == false) {

                    if(b.getButton(AlertDialog.BUTTON_POSITIVE).getText().toString().equalsIgnoreCase("Verify")) {


                        if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
                            String GenerateOTPURL = APIName.URL + "/message/generateOTP";
                            System.out.println("GENERATE OTP URL IS---" + GenerateOTPURL);
                            GenerateOTPAPI(GenerateOTPURL, mobileedt.getText().toString());
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                        b.getButton(AlertDialog.BUTTON_POSITIVE).setText("Update");
                    }
                    else {

                        if (otpedt.getText().toString().equalsIgnoreCase(otpstr)) {
                            if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
                                String updateEmailMobileURL = APIName.URL + "/candidate/updateEmailMobile?candidate_Id=" + candidateidstr;
                                System.out.println("UPDATE EMAIL MOBILE URL IS---" + updateEmailMobileURL);
                                updateEmailMobile1API(updateEmailMobileURL, mobileedt.getText().toString());
                            } else {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                                View views = snackbar.getView();
                                TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                            b.dismiss();
                        }
                        else
                        {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "OTP Mismatch!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                }

            }







        });


    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }

    private void GenerateOTPAPI(String url,final String mobileno) {

        pdia = new ProgressDialog(MyAccountActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GENERATE OTP API IS---" + response);

                        try {
                            JSONObject generateotpjson = new JSONObject(response);
                            String statusstr = generateotpjson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")){


                                isMobVerify = true;
                                otpstr = generateotpjson.getString("otp");
                                otpedt.setVisibility(View.VISIBLE);
                                otpedt.setText(otpstr);
                                mobileedt.setEnabled(false);
                                mobileedt.setText(mobileno);
                                mobileedt.setTextColor(Color.parseColor("#E4E4E4"));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                //  Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        }
                        else
                        {
                            //  util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                params.put("mob_number",mobileno);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }
    
    public void updateVerifyEmailDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_verify_email, null);
        dialogBuilder.setView(dialogView);

        final EditText emailedt = (EditText) dialogView.findViewById(R.id.emailedt);

         System.out.println("EMAIL VERIFIED STRING IS---"+ emailVerifiedstr);

        emailedt.setText(emailstr);


        dialogBuilder.setMessage("Update & Verify Email");



        dialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {



            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    b.dismiss();

              /*  boolean invalid = false;

                if ((!isValidEmail(emailedt.getText().toString())) ) {
                    invalid = true;
                    wantToCloseDialog = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Email !!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }else if (invalid == false) {
                    wantToCloseDialog = false;

                    if(NetworkUtility.checkConnectivity(MyAccountActivity.this)){
                        String verifyEmailURL = APIName.URL+"/candidate/verifyEmail";
                        System.out.println("VERIFY EMAIL URL IS---"+ verifyEmailURL);
                        verifyEmailAPI(verifyEmailURL,emailedt.getText().toString());
                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }
*/
            }
        });
        final  AlertDialog b = dialogBuilder.create();
        b.show();

        Button postivebtn = b.getButton(AlertDialog.BUTTON_POSITIVE);
        postivebtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                boolean invalid = false;

                if ((!isValidEmail(emailedt.getText().toString())) ) {
                    invalid = true;


                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Email !!!", Snackbar.LENGTH_LONG);
                    View views = snackbar.getView();
                    TextView tv = (TextView)views.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }else if (invalid == false) {


                    if(b.getButton(AlertDialog.BUTTON_POSITIVE).getText().toString().equalsIgnoreCase("Update")) {
                        if (NetworkUtility.checkConnectivity(MyAccountActivity.this)) {
                            String updateEmailMobileURL = APIName.URL + "/candidate/updateEmailMobile?candidate_Id=" + candidateidstr;
                            System.out.println("UPDATE EMAIL MOBILE URL IS---" + updateEmailMobileURL);
                            updateEmailMobileAPI(updateEmailMobileURL, emailedt.getText().toString());
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView) views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }
                    }
                    else
                    {
                        if(NetworkUtility.checkConnectivity(MyAccountActivity.this)){
                            String verifyEmailURL = APIName.URL+"/candidate/verifyEmail";
                            System.out.println("VERIFY EMAIL URL IS---"+ verifyEmailURL);
                            verifyEmailAPI(verifyEmailURL,emailedt.getText().toString());
                        }
                        else{
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                            View views = snackbar.getView();
                            TextView tv = (TextView)views.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }

                }


            }







        });

        if(emailVerifiedstr.equalsIgnoreCase("0"))
        {
          //  b.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
          //  b.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);

            b.getButton(AlertDialog.BUTTON_POSITIVE).setText("Verify");

            emailedt.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // TODO Auto-generated method stub

                }


                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(TextUtils.isEmpty(s)) {
                        // call method
                        emailedt.setTextColor(Color.BLACK);
                      //  b.getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(View.GONE);
                      //  b.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.VISIBLE);

                        b.getButton(AlertDialog.BUTTON_POSITIVE).setText("Update");

                    }
                }
            });

        }
        else
        {
            b.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);


        }

    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }


    private void verifyEmailAPI(String url,final String userEmailStr) {

        pdia = new ProgressDialog(MyAccountActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        System.out.println("RESPONSE OF VERIFY EMAIL API IS---" + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("candidate_Id",candidateidstr);
                params.put("name",namestr);
                params.put("email",userEmailStr);
                params.put("emailVerified","0");

                System.out.println("PARAMETERS OF verifyEmailAPI--------" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }

    private void updateEmailMobile1API(String url,final String mobilestr) {

        pdia = new ProgressDialog(MyAccountActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        System.out.println("RESPONSE OF UPDATE EMAIL MOBILE 1 API IS---" + response);

                        Intent i = new Intent(MyAccountActivity.this,MyAccountActivity.class);
                        startActivity(i);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

               // params.put("userEmail",userEmailStr);
                 params.put("userMobile",mobilestr);

                System.out.println("PARAMETERS OF updateEmailMobileAPI--------" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }

    private void updateEmailMobileAPI(String url,final String userEmailStr) {

        pdia = new ProgressDialog(MyAccountActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        System.out.println("RESPONSE OF UPDATE EMAIL MOBILE API IS---" + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userEmail",userEmailStr);
               // params.put("userMobile",mobilestr);

                System.out.println("PARAMETERS OF updateEmailMobileAPI--------" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getUserCredentialAPI(String url) {

        pdia = new ProgressDialog(MyAccountActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET USER CREDENTIAL API IS---" + response);

                        JSONObject candidateacaddetailsjson = null;
                        try {
                            candidateacaddetailsjson = new JSONObject(response);

                            JSONArray UserCredentialJSONArray = candidateacaddetailsjson.getJSONArray("UserCredential");

                            emailstr = UserCredentialJSONArray.getJSONObject(0).getString("email");
                            mobilestr =  UserCredentialJSONArray.getJSONObject(0).getString("mob_number");
                            namestr = UserCredentialJSONArray.getJSONObject(0).getString("name");

                            emaildesctxt.setText(UserCredentialJSONArray.getJSONObject(0).getString("email"));
                            mobiledescdesctxt.setText(UserCredentialJSONArray.getJSONObject(0).getString("mob_number"));

                            emailVerifiedstr = UserCredentialJSONArray.getJSONObject(0).getString("emailVerified");
                            mobVerifiedstr = UserCredentialJSONArray.getJSONObject(0).getString("mobVerified");


                            if(emailVerifiedstr.equalsIgnoreCase("0"))
                            {
                               // emaildesctxt.setTextColor(Color.RED);
                                emaildesctxt.setTextColor(Color.parseColor("#FFA5A5A5"));
                            }
                            else
                            {
                                emaildesctxt.setTextColor(Color.BLACK);
                            }


                            if(mobVerifiedstr.equalsIgnoreCase("0"))
                            {
                                mobiledescdesctxt.setTextColor(Color.parseColor("#FFA5A5A5"));
                            }
                            else
                            {
                                mobiledescdesctxt.setTextColor(Color.BLACK);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(stringRequest);
    }



}
