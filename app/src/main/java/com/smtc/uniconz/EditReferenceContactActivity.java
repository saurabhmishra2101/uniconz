package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/3/2018.
 */

public class EditReferenceContactActivity extends AppCompatActivity implements  AdapterView.OnItemClickListener{

    ImageView backiv;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr,locationstr;
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    EditText nametitleedt,emailedt,designationedt,companyedt,mobilenoedt;
    EditText nametitle1edt,email1edt,designation1edt,company1edt,mobileno1edt;
    AutoCompleteTextView location_autocomplete,location_autocomplete1;
    Typeface roboto_light_font,bebas_font;
    ArrayList<String> address_refrence_Id_list = new ArrayList<String>();
    TextView reference1txt,reference2txt;
    ArrayList<String> name_list = new ArrayList<String>();
    ArrayList<String> mob_number_list = new ArrayList<String>();
    ArrayList<String> email_list = new ArrayList<String>();
    ArrayList<String> designation_list = new ArrayList<String>();
    ArrayList<String> company_list = new ArrayList<String>();
    ArrayList<String> location_list = new ArrayList<String>();
    private static final String LOG_TAG = "Uniconz";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";
    RelativeLayout saverelative;
    JSONArray CandidateOtherReferenceJSONArray;
    String address_ref_id_str,name_str,email_str,designation_str,company_str,mob_num_str,location_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_reference_coordinator);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        reference1txt = (TextView)findViewById(R.id.reference1txt);
        reference2txt = (TextView)findViewById(R.id.reference2txt);

        nametitleedt = (EditText)findViewById(R.id.nametitleedt);
        emailedt = (EditText)findViewById(R.id.emailedt);
        designationedt = (EditText)findViewById(R.id.designationedt);
        companyedt = (EditText)findViewById(R.id.companyedt);
        mobilenoedt = (EditText)findViewById(R.id.mobilenoedt);
        location_autocomplete = (AutoCompleteTextView) findViewById(R.id.location_autocomplete);

        nametitle1edt = (EditText)findViewById(R.id.nametitle1edt);
        email1edt = (EditText)findViewById(R.id.email1edt);
        designation1edt = (EditText)findViewById(R.id.designation1edt);
        company1edt = (EditText)findViewById(R.id.company1edt);
        mobileno1edt = (EditText)findViewById(R.id.mobileno1edt);
        location_autocomplete1 = (AutoCompleteTextView) findViewById(R.id.location_autocomplete1);

        nametitleedt.setTypeface(roboto_light_font);
        emailedt.setTypeface(roboto_light_font);
        designationedt.setTypeface(roboto_light_font);
        companyedt.setTypeface(roboto_light_font);
        mobilenoedt.setTypeface(roboto_light_font);
        location_autocomplete.setTypeface(roboto_light_font);

        nametitle1edt.setTypeface(roboto_light_font);
        email1edt.setTypeface(roboto_light_font);
        designation1edt.setTypeface(roboto_light_font);
        company1edt.setTypeface(roboto_light_font);
        mobileno1edt.setTypeface(roboto_light_font);
        location_autocomplete1.setTypeface(roboto_light_font);

        reference1txt.setTypeface(bebas_font);
        reference2txt.setTypeface(bebas_font);

        backiv = (ImageView)findViewById(R.id.backiv);

        location_autocomplete.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete.setOnItemClickListener(EditReferenceContactActivity.this);


        location_autocomplete1.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        location_autocomplete1.setOnItemClickListener(EditReferenceContactActivity.this);


        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent i = new Intent(EditReferenceContactActivity.this,CandidateDetailsActivity.class);
                startActivity(i);
            }
        });

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        if (NetworkUtility.checkConnectivity(EditReferenceContactActivity.this)) {
            String GetCandOtherRefURL = APIName.URL + "/candidate/getCandOtherRef?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND OTHER REF URL IS---" + GetCandOtherRefURL);
            GetCandOtherRefAPI(GetCandOtherRefURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        nametitleedt = (EditText)findViewById(R.id.nametitleedt);
        emailedt = (EditText)findViewById(R.id.emailedt);
        designationedt = (EditText)findViewById(R.id.designationedt);
        companyedt = (EditText)findViewById(R.id.companyedt);
        mobilenoedt = (EditText)findViewById(R.id.mobilenoedt);
        location_autocomplete = (AutoCompleteTextView) findViewById(R.id.location_autocomplete);


        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean invalid = false;

                if (nametitleedt.getText().toString().equals("")) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Name", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (!isValidEmail(emailedt.getText().toString())) {
                    invalid = true;

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Email", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(!isValidMobileNumber(mobilenoedt.getText().toString()))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Valid Mobile Number", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }

                else if(location_autocomplete.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                            Toast.LENGTH_SHORT).show();
                 */
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Location", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();

                }


                else if (invalid == false) {

                    address_ref_id_str = "";


                    if(CandidateOtherReferenceJSONArray.length() == 0)
                    {
                        address_ref_id_str = ",";
                    }

                    if(CandidateOtherReferenceJSONArray.length() == 1)
                    {
                        try {
                            address_ref_id_str = CandidateOtherReferenceJSONArray.getJSONObject(0).getString("address_refrence_Id") + ",";
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                    if(CandidateOtherReferenceJSONArray.length() == 2)
                    {
                        try {
                            address_ref_id_str = CandidateOtherReferenceJSONArray.getJSONObject(0).getString("address_refrence_Id") + "," + CandidateOtherReferenceJSONArray.getJSONObject(1).getString("address_refrence_Id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println("COMMA SEPARATED address_ref_id_str===="+ address_ref_id_str);

                    //======================name str============================
                    name_str = "";




                    if (nametitleedt.getText().toString().length()>0) {
                        name_str = nametitleedt.getText().toString()+",";

                    }
                    if (nametitle1edt.getText().toString().length()>0){
                        name_str = name_str+nametitle1edt.getText().toString();
                    }

                    System.out.println("COMMA SEPARATED name_str===="+ name_str);


                    //======================email str============================

                    email_str = "";

                    if (emailedt.getText().toString().length()>0) {
                        email_str = emailedt.getText().toString()+",";

                    }
                    if (email1edt.getText().toString().length()>0){
                        email_str = email_str+email1edt.getText().toString();
                    }

                    System.out.println("COMMA SEPARATED email_str===="+ email_str);

                    //======================designation str============================

                    designation_str = "";

                    if (designationedt.getText().toString().length()>0) {
                        designation_str = designationedt.getText().toString()+",";

                    }
                    else
                    {
                        designation_str = ",";
                    }
                    if (designation1edt.getText().toString().length()>0){
                        designation_str = designation_str+designation1edt.getText().toString();
                    }
                    else
                    {

                    }

                    System.out.println("COMMA SEPARATED designation_str===="+ designation_str);

                    //======================company str============================

                    company_str = "";

                    if (companyedt.getText().toString().length()>0) {
                        company_str = companyedt.getText().toString()+",";

                    }
                    else
                    {
                        company_str = ",";
                    }
                    if (company1edt.getText().toString().length()>0){
                        company_str = company_str+company1edt.getText().toString();
                    }
                    else
                    {

                    }

                    System.out.println("COMMA SEPARATED company_str===="+ company_str);


                    //======================mob_num_str============================

                    mob_num_str = "";


                    if (mobilenoedt.getText().toString().length()>0) {
                        mob_num_str = mobilenoedt.getText().toString()+",";

                    }
                    if (mobileno1edt.getText().toString().length()>0){
                        mob_num_str = mob_num_str+mobileno1edt.getText().toString();
                    }

                    System.out.println("COMMA SEPARATED mob_num_str===="+ mob_num_str);

                    //======================location_str============================

                    location_str = "";


                    if (location_autocomplete.getText().toString().length()>0) {
                        location_str = location_autocomplete.getText().toString()+",";

                    }
                    if (location_autocomplete1.getText().toString().length()>0){
                        location_str = location_str+location_autocomplete1.getText().toString();
                    }

                    System.out.println("COMMA SEPARATED location_str===="+ location_str);

                    //===========================================================



                    if(NetworkUtility.checkConnectivity(EditReferenceContactActivity.this)){
                        String insertUpdateOtherRefURL = APIName.URL+"/candidate/insertUpdateOtherRef?candidate_Id="+candidateidstr;
                        System.out.println("INSERT UPDATE OTHER REF URL IS---"+ insertUpdateOtherRefURL);
                        insertUpdateOtherRefAPI(insertUpdateOtherRefURL,address_ref_id_str,name_str,email_str,designation_str,company_str,mob_num_str,location_str);

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }

                }


            }
        });


    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(EditReferenceContactActivity.this,CandidateDetailsActivity.class);
        startActivity(i);
    }

    private void insertUpdateOtherRefAPI(String url, final String address_ref_id_str, final String name_str, final String email_str, final String designation_str, final String company_str, final String mob_num_str, final String location_str){

        pdia = new ProgressDialog(EditReferenceContactActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE OTHER REF API IS---" + response);

                        JSONObject insertupdateotherrefjson = null;
                        try {
                            insertupdateotherrefjson = new JSONObject(response);

                            System.out.println("INSERT UPDATE OTHER REF JSON IS---" + insertupdateotherrefjson);

                            String statusstr = insertupdateotherrefjson.getString("status");
                            String msgstr = insertupdateotherrefjson.getString("message");

                            if(statusstr.equalsIgnoreCase("1"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditReferenceContactActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("address_refrence_Id",address_ref_id_str);
                params.put("name",name_str);
                params.put("email",email_str);
                params.put("designation",designation_str);
                params.put("company",company_str);
                params.put("mob_number",mob_num_str);
                params.put("location",location_str);
                params.put("candidate_Id",candidateidstr);

                System.out.println("PARAMS OF insertUpdateOtherRefAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditReferenceContactActivity.this);
        requestQueue.add(stringRequest);
    }

    public  final boolean isValidMobileNumber(String mobile) {
        return mobile.length() == 10;
    }


    public final  boolean isValidEmail(String email) {
        if (email == null) {

           /* Toast.makeText(this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();
*/

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }


    private void GetCandOtherRefAPI(String url) {

        pdia = new ProgressDialog(EditReferenceContactActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        name_list.clear();
                        mob_number_list.clear();
                        email_list.clear();
                        designation_list.clear();
                        company_list.clear();
                        location_list.clear();

                        System.out.println("RESPONSE OF GET CAND OTHER REF API IS---" + response);


                        JSONObject candotherrefjson = null;
                        try {
                            candotherrefjson = new JSONObject(response);

                             CandidateOtherReferenceJSONArray = candotherrefjson.getJSONArray("CandidateOtherReference");

                            for(int i = 0 ; i < CandidateOtherReferenceJSONArray.length(); i++) {

                                name_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("name"));
                                mob_number_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("mob_number"));
                                email_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("email"));
                                designation_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("designation"));
                                company_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("company"));
                                location_list.add(CandidateOtherReferenceJSONArray.getJSONObject(i).getString("location"));


                            }
                            if(CandidateOtherReferenceJSONArray.length()==1)
                            {
                                nametitleedt.setText(name_list.get(0));
                                emailedt.setText(email_list.get(0));
                                designationedt.setText(designation_list.get(0));
                                companyedt.setText(company_list.get(0));
                                mobilenoedt.setText(mob_number_list.get(0));
                                location_autocomplete.setText(location_list.get(0));

                                nametitle1edt.setText("");
                                email1edt.setText("");
                                designation1edt.setText("");
                                company1edt.setText("");
                                mobileno1edt.setText("");
                                location_autocomplete1.setText("");
                            }

                            if(CandidateOtherReferenceJSONArray.length()==2)
                            {
                                nametitleedt.setText(name_list.get(0));
                                emailedt.setText(email_list.get(0));
                                designationedt.setText(designation_list.get(0));
                                companyedt.setText(company_list.get(0));
                                mobilenoedt.setText(mob_number_list.get(0));
                                location_autocomplete.setText(location_list.get(0));


                                nametitle1edt.setText(name_list.get(1));
                                email1edt.setText(email_list.get(1));
                                designation1edt.setText(designation_list.get(1));
                                company1edt.setText(company_list.get(1));
                                mobileno1edt.setText(mob_number_list.get(1));
                                location_autocomplete1.setText(location_list.get(1));
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditReferenceContactActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        locationstr = (String) adapterView.getItemAtPosition(position);
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            // sb.append("&components=&types=(cities)");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
}
