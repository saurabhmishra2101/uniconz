package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/21/2018.
 */

public class EditKeySkillActivity extends AppCompatActivity {

    AutoCompleteTextView skill_autocompletetxtvw;
    EditText descriptionedt;
    MaterialBetterSpinner proficiency_level_spinner;
    ImageView backiv;
    TextView deletetxt;
    ArrayList<String> proficiency_list = new ArrayList<String>();
    ArrayList<String> skillnamelist = new ArrayList<String>();
    Typeface roboto_light_font;
    String proficiency_level_str,currentDateTimeString;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr;
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    RelativeLayout saverelative;
    String attachmentstr,type,upload_condition_str;
    Button choosefilebtn;
    TextView uploadresumetxt,nofiletxt,uploadeddocumenttxt;
    ImageView docimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_key_skill_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        uploadresumetxt = (TextView)findViewById(R.id.uploadresumetxt);

        uploadeddocumenttxt = (TextView)findViewById(R.id.uploadeddocumenttxt);
        docimg = (ImageView)findViewById(R.id.docimg);

        skill_autocompletetxtvw = (AutoCompleteTextView) findViewById(R.id.skill_autocompletetxtvw);
        descriptionedt = (EditText)findViewById(R.id.descriptionedt);
        proficiency_level_spinner = (MaterialBetterSpinner)findViewById(R.id.proficiency_level_spinner);
        backiv = (ImageView)findViewById(R.id.backiv);

        uploadresumetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
                uploadresumetxt.setText("");
            }
        });
        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);


        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        uploadresumetxt.setTypeface(roboto_light_font);
        uploadeddocumenttxt.setTypeface(roboto_light_font);


        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditKeySkillActivity.this,CandidateDetailsActivity.class);
                startActivity(i);

            }
        });

        deletetxt = (TextView)findViewById(R.id.deletetxt);

        if(!SingletonActivity.candidateskillid.equalsIgnoreCase("")) {
            deletetxt.setVisibility(View.VISIBLE);
        }
        else
        {
            deletetxt.setVisibility(View.INVISIBLE);
        }

        deletetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtility.checkConnectivity(EditKeySkillActivity.this)){
                    String deleteCandidateSkillURL = APIName.URL+"/candidate/deleteCandidateSkill?candidate_skill_Id="+SingletonActivity.candidateskillid;
                    System.out.println("DELETE CANDIDATE SKILL URL IS---"+ deleteCandidateSkillURL);
                    deleteCandidateSkillAPI(deleteCandidateSkillURL);

                }
                else{
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());


        System.out.println("CURRENT DATE TIME STRING---"+ currentDateTimeString);


        if (NetworkUtility.checkConnectivity(EditKeySkillActivity.this)) {
            String GetSkillMasterURL = APIName.URL + "/master/getSkillMaster";
            System.out.println("GET SKILL MASTER URL IS---" + GetSkillMasterURL);
            GetSkillMasterAPI(GetSkillMasterURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }


        proficiency_list.add("Beginner");
        proficiency_list.add("Intermediate");
        proficiency_list.add("Advanced");

//=====For Proficiency Level==================
        ArrayAdapter<String> proficiencyAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, proficiency_list);

        proficiency_level_spinner.setAdapter(proficiencyAdapter);

        skill_autocompletetxtvw.setTypeface(roboto_light_font);
        descriptionedt.setTypeface(roboto_light_font);
        proficiency_level_spinner.setTypeface(roboto_light_font);

        proficiency_level_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                proficiency_level_str = parent.getItemAtPosition(position).toString();


            }
        });

        if (NetworkUtility.checkConnectivity(EditKeySkillActivity.this)) {
            String GetCandKeySkillsURL = APIName.URL + "/candidate/getCandKeySkills?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND KEY SKILLS URL IS---" + GetCandKeySkillsURL);
            GetCandKeySkillsAPI(GetCandKeySkillsURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                if (skill_autocompletetxtvw.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Skills", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(descriptionedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Description", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                else if(proficiency_level_spinner.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Select Proficiency Level", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {


                    if(NetworkUtility.checkConnectivity(EditKeySkillActivity.this)){
                        String insertUpdateCandSkillsURL = APIName.URL+"/candidate/insertUpdateCandSkills?candidate_Id="+candidateidstr;
                        System.out.println("INSERT UPDATE CAND SKILLS URL IS---"+ insertUpdateCandSkillsURL);
                        insertUpdateCandSkillsAPI(insertUpdateCandSkillsURL,SingletonActivity.candidateskillid,candidateidstr,descriptionedt.getText().toString(),SingletonActivity.skillid,proficiency_level_spinner.getText().toString(),skill_autocompletetxtvw.getText().toString(),currentDateTimeString);

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }



                }


            }
        });

    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");



        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();
                    System.out.println("SELECTED FILE PATH IS--"+ filePath);

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;


                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);
                        if(type.equals("pdf")||type.equals("doc")||type.equals("docx"))
                        {
                            attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                            System.out.println("BASE 64 STRING------"+ attachmentstr);

                            if (uriString.startsWith("content://")) {
                                Cursor cursor = null;
                                try {
                                    cursor = EditKeySkillActivity.this.getContentResolver().query(uri, null, null, null, null);
                                    if (cursor != null && cursor.moveToFirst()) {
                                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        System.out.println("displayName 1 is------"+ displayName);
                                        uploadresumetxt.setText(displayName);
                                    }
                                } finally {
                                    cursor.close();
                                }
                            } else if (uriString.startsWith("file://")) {
                                displayName = myFile.getName();
                                System.out.println("displayName 2 is------"+ displayName);
                                uploadresumetxt.setText(displayName);
                            }
                        }
                        else {

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Please upload the resume in following formats .pdf,.doc,.docx only", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }




                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    @Override
    public void onBackPressed() {



                Intent i = new Intent(EditKeySkillActivity.this,CandidateDetailsActivity.class);
                startActivity(i);


    }

    private void deleteCandidateSkillAPI(String url){

        pdia = new ProgressDialog(EditKeySkillActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF DELETE CANDIDATE SKILL API IS---" + response);

                        JSONObject deletecandskilljson = null;
                        try {
                            deletecandskilljson = new JSONObject(response);

                            System.out.println("DELETE CANDIDATE SKILL JSON IS---" + deletecandskilljson);

                            String statusstr = deletecandskilljson.getString("status");
                            String msgstr = deletecandskilljson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditKeySkillActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditKeySkillActivity.this);
        requestQueue.add(stringRequest);
    }



    private void insertUpdateCandSkillsAPI(String url,final String candidateskillidstr,final String candidateidstr,final String descriptionstr,final String skillid,final String proficiency_level_str,final String skillstr,final String currentDateTimeString){

        pdia = new ProgressDialog(EditKeySkillActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE CAND SKILLS API IS---" + response);

                        JSONObject insertupdatecandskillsjson = null;
                        try {
                            insertupdatecandskillsjson = new JSONObject(response);

                            System.out.println("INSERT UPDATE CAND SKILLS JSON IS---" + insertupdatecandskillsjson);

                            String statusstr = insertupdatecandskillsjson.getString("status");
                            String msgstr = insertupdatecandskillsjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditKeySkillActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_skill_Id",candidateskillidstr);
                params.put("candidate_Id",candidateidstr);
                params.put("skill_detail",descriptionstr);
                params.put("skill_Id",skillid);
                params.put("proficiency_level",proficiency_level_str);
                params.put("skill_name",skillstr);
                params.put("modified",currentDateTimeString);

                if(attachmentstr==null)
                {
                    attachmentstr = "0";
                    params.put("attachment",attachmentstr);
                    //  params.put("extension","");
                }
                else
                {
                    params.put("attachment",attachmentstr);
                    //   params.put("extension",type);
                }


                if(attachmentstr==null)
                {
                    upload_condition_str = "0";
                }
                else
                {
                    upload_condition_str = "1";
                }

                params.put("upload_Condition",upload_condition_str);




                System.out.println("PARAMS OF insertUpdateCandSkillsAPI==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditKeySkillActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetCandKeySkillsAPI(String url) {

        pdia = new ProgressDialog(EditKeySkillActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET CAND KEY SKILLS API IS---" + response);

                        JSONObject candkeyskillsjson = null;
                        try {
                            candkeyskillsjson = new JSONObject(response);

                            final JSONArray CandidateKeySkillsJSONArray = candkeyskillsjson.getJSONArray("CandidateKeySkills");


                            System.out.println("candidateskillid is---" + SingletonActivity.candidateskillid);

                            if(!SingletonActivity.candidateskillid.equalsIgnoreCase("")) {
                                final int pos = SingletonActivity.keyskillpos;
                                System.out.println("CandidateKeySkillsJSONArray position IS---" + pos);
                                System.out.println("CandidateKeySkillsJSONArray IS---" + CandidateKeySkillsJSONArray.get(pos));

                                skill_autocompletetxtvw.setText(CandidateKeySkillsJSONArray.getJSONObject(pos).getString("skill_name"));
                                descriptionedt.setText(CandidateKeySkillsJSONArray.getJSONObject(pos).getString("skill_detail"));
                                proficiency_level_spinner.setText(CandidateKeySkillsJSONArray.getJSONObject(pos).getString("proficiency_level"));



                                System.out.println("certification_upload IS---" + CandidateKeySkillsJSONArray.getJSONObject(pos).getString("upload"));

                                if(!CandidateKeySkillsJSONArray.getJSONObject(pos).getString("upload").equalsIgnoreCase(""))
                                {
                                    docimg.setVisibility(View.VISIBLE);
                                    uploadeddocumenttxt.setVisibility(View.VISIBLE);

                                    String image_url = CandidateKeySkillsJSONArray.getJSONObject(pos).getString("upload");
                                    String final_image_url = APIName.IMAGE_URL + image_url;

                                    Uri uri = Uri.parse(final_image_url);
                                    String token = uri.getLastPathSegment();

                                    System.out.println("token IS---" + token);

                                   uploadresumetxt.setText(token);


                                    docimg.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Intent i = new Intent(EditKeySkillActivity.this,WebViewActivity.class);
                                            try {
                                                SingletonActivity.certificateuploadurl = CandidateKeySkillsJSONArray.getJSONObject(pos).getString("upload");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            startActivity(i);

                                        }
                                    });

                                }
                                else
                                {
                                    uploadeddocumenttxt.setVisibility(View.GONE);
                                    docimg.setVisibility(View.GONE);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditKeySkillActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetSkillMasterAPI(String url) {

    /*    pdia = new ProgressDialog(EditKeySkillActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        skillnamelist.clear();



                        System.out.println("RESPONSE OF GET SKILL MASTER API IS---" + response);

                        JSONObject getskilldetailsjson = null;
                        try {
                            getskilldetailsjson = new JSONObject(response);

                            JSONArray SkillDetailsJSONArray = getskilldetailsjson.getJSONArray("SkillDetails");




                            for(int i = 0 ; i < SkillDetailsJSONArray.length(); i++)
                            {
                                String skill_name_str = SkillDetailsJSONArray.getJSONObject(i).getString("skill_name");
                                System.out.println("SKILL NAME IS---" + skill_name_str);
                                skillnamelist.add(skill_name_str);
                            }

                            ArrayAdapter<String> skilladapter = new ArrayAdapter<String>(EditKeySkillActivity.this,android.R.layout.simple_list_item_1,skillnamelist);
                            skill_autocompletetxtvw.setAdapter(skilladapter);





                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //  pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditKeySkillActivity.this);
        requestQueue.add(stringRequest);
    }
}
