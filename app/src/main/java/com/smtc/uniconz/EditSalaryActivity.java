package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/28/2018.
 */

public class EditSalaryActivity extends AppCompatActivity {

    String candidateidstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    CoordinatorLayout coordinatorLayout;
    ProgressDialog pdia;
    EditText currentctcedt,currentfixedsaledt,currentvariablesalaryedt,allowanceedt,commentedt,expectedctcedt;
    RelativeLayout saverelative;
    Typeface roboto_light_font;
    String currentDateTimeString;
    ImageView backiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_salary_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        backiv = (ImageView)findViewById(R.id.backiv);
        currentctcedt = (EditText)findViewById(R.id.currentctcedt);
        currentfixedsaledt = (EditText)findViewById(R.id.currentfixedsaledt);
        currentvariablesalaryedt = (EditText)findViewById(R.id.currentvariablesalaryedt);
        allowanceedt= (EditText)findViewById(R.id.allowanceedt);
        commentedt = (EditText)findViewById(R.id.commentedt);
        expectedctcedt = (EditText)findViewById(R.id.expectedctcedt);
        saverelative = (RelativeLayout)findViewById(R.id.saverelative);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditSalaryActivity.this,CandidateDetailsActivity.class);
              startActivity(i);
            }
        });

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");

        currentctcedt.setTypeface(roboto_light_font);
        currentfixedsaledt.setTypeface(roboto_light_font);
        currentvariablesalaryedt.setTypeface(roboto_light_font);
        allowanceedt.setTypeface(roboto_light_font);
        commentedt.setTypeface(roboto_light_font);
        expectedctcedt.setTypeface(roboto_light_font);


        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

         Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());


        if (NetworkUtility.checkConnectivity(EditSalaryActivity.this)) {
            String GetCandSalaryDetailsURL = APIName.URL + "/candidate/getCandSalaryDetails?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND SALARY DETAILS URL IS---" + GetCandSalaryDetailsURL);
            GetCandSalaryDetailsAPI(GetCandSalaryDetailsURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                if (currentctcedt.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Current CTC", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(expectedctcedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Expected CTC", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {

                    if(NetworkUtility.checkConnectivity(EditSalaryActivity.this)){
                        String insertUpdateSalaryDetailsURL = APIName.URL+"/candidate/insertUpdateSalaryDetails?salary_Id="+SingletonActivity.salaryid;
                        System.out.println("INSERT UPDATE SALARY DETAILS URL IS---"+ insertUpdateSalaryDetailsURL);
                        insertUpdateSalaryDetailsAPI(insertUpdateSalaryDetailsURL,currentctcedt.getText().toString(),currentfixedsaledt.getText().toString(),currentvariablesalaryedt.getText().toString(),allowanceedt.getText().toString(),commentedt.getText().toString(),expectedctcedt.getText().toString(),currentDateTimeString);

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }
                }


            }
        });


    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(EditSalaryActivity.this,CandidateDetailsActivity.class);
        startActivity(i);
    }

    private void insertUpdateSalaryDetailsAPI(String url, final String currentctcstr, final String currentfixedsalstr, final String currentvariablesalarystr, final String allowancestr, final String commentstr, final String expectedctcstr, final String currentDateTimeString){

        pdia = new ProgressDialog(EditSalaryActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE SALARY DETAILS API IS---" + response);

                        JSONObject insertupdatesalarydetailsjson = null;
                        try {
                            insertupdatesalarydetailsjson = new JSONObject(response);

                            System.out.println("INSERT UPDATE SALARY DETAILS JSON IS---" + insertupdatesalarydetailsjson);

                            String statusstr = insertupdatesalarydetailsjson.getString("status");
                            String msgstr = insertupdatesalarydetailsjson.getString("message");

                            if(statusstr.equalsIgnoreCase("1"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditSalaryActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("current_ctc",currentctcstr);
                params.put("current_fixed_salary",currentfixedsalstr);
                params.put("current_variable_salary",currentvariablesalarystr);
                params.put("current_allowance",allowancestr);
                params.put("comment",commentstr);
                params.put("expected_ctc",expectedctcstr);
                params.put("candidate_Id",candidateidstr);
                params.put("modified",currentDateTimeString);



                System.out.println("PARAMS OF insertUpdateCredential API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditSalaryActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetCandSalaryDetailsAPI(String url) {

        pdia = new ProgressDialog(EditSalaryActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET CAND SALARY DETAILS API IS---" + response);

                        JSONObject candidatesaldetailsjson = null;
                        try {
                            candidatesaldetailsjson = new JSONObject(response);

                            JSONObject CandidateSalaryDetailsJSON = candidatesaldetailsjson.getJSONObject("CandidateSalaryDetails");

                            currentctcedt.setText(CandidateSalaryDetailsJSON.getString("current_ctc"));
                            currentfixedsaledt.setText(CandidateSalaryDetailsJSON.getString("current_fixed_salary"));
                            currentvariablesalaryedt.setText(CandidateSalaryDetailsJSON.getString("current_variable_salary"));
                            allowanceedt.setText(CandidateSalaryDetailsJSON.getString("current_allowance"));
                            commentedt.setText(CandidateSalaryDetailsJSON.getString("comment"));
                            expectedctcedt.setText(CandidateSalaryDetailsJSON.getString("expected_ctc"));


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditSalaryActivity.this);
        requestQueue.add(stringRequest);
    }
}
