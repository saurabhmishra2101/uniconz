package com.smtc.uniconz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 3/22/2018.
 */

public class EditCredentialActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;
    TextView deletetxt;
    EditText crededt,descriptionedt;
    RelativeLayout saverelative;
    Typeface roboto_light_font;
    ImageView backiv,docimg;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr,currentDateTimeString;
    ProgressDialog pdia;
    TextView uploadresumetxt;
    String attachmentstr,type,upload_condition_str;
    TextView uploadfiletxt,nofiletxt,uploadeddocumenttxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_credential_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        uploadresumetxt = (TextView)findViewById(R.id.uploadresumetxt);
        docimg = (ImageView)findViewById(R.id.docimg);


        uploadeddocumenttxt = (TextView)findViewById(R.id.uploadeddocumenttxt);

        uploadresumetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
                uploadresumetxt.setText("");
            }
        });

        deletetxt = (TextView)findViewById(R.id.deletetxt);
        crededt = (EditText) findViewById(R.id.crededt);
        descriptionedt = (EditText)findViewById(R.id.descriptionedt);
        saverelative = (RelativeLayout) findViewById(R.id.saverelative);
        backiv = (ImageView)findViewById(R.id.backiv);

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");


        crededt.setTypeface(roboto_light_font);
        descriptionedt.setTypeface(roboto_light_font);



        uploadeddocumenttxt.setTypeface(roboto_light_font);

        if(!SingletonActivity.credentialid.equalsIgnoreCase("")) {
            deletetxt.setVisibility(View.VISIBLE);
        }
        else
        {
            deletetxt.setVisibility(View.INVISIBLE);
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentDateTimeString = sdf.format(c.getTime());

        deletetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtility.checkConnectivity(EditCredentialActivity.this)){
                    String deleteCandCredentialURL = APIName.URL+"/candidate/deleteCandCredential?credential_Id="+SingletonActivity.credentialid;
                    System.out.println("DELETE CAND CREDENTIAL URL IS---"+ deleteCandCredentialURL);
                    deleteCandCredentialAPI(deleteCandCredentialURL);

                }
                else{
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                    View view = snackbar.getView();
                    TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        });

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditCredentialActivity.this,CandidateDetailsActivity.class);
                startActivity(i);

            }
        });

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        if (NetworkUtility.checkConnectivity(EditCredentialActivity.this)) {
            String GetCandCredentialsURL = APIName.URL + "/candidate/getCandCredentials?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND CREDENTIALS URL IS---" + GetCandCredentialsURL);
            GetCandCredentialsAPI(GetCandCredentialsURL);

        } else {


            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalid = false;

                if (crededt.getText().toString().equals("")) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(), "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Title", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if(descriptionedt.getText().toString().equals(""))
                {
                    invalid = true;
                 /*   Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();*/

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Please Enter Description", Snackbar.LENGTH_LONG);


                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

                else if (invalid == false) {

                    if(NetworkUtility.checkConnectivity(EditCredentialActivity.this)){
                        String insertUpdateCredentialURL = APIName.URL+"/candidate/insertUpdateCredential?credential_Id="+SingletonActivity.credentialid+"&candidate_Id="+candidateidstr;
                        System.out.println("INSERT UPDATE CREDENTIAL URL IS---"+ insertUpdateCredentialURL);
                        insertUpdateCredentialAPI(insertUpdateCredentialURL,crededt.getText().toString(),descriptionedt.getText().toString());

                    }
                    else{
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);
                        View view = snackbar.getView();
                        TextView tv = (TextView)view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.RED);
                        snackbar.show();
                    }
                }


            }
        });


    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");



        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();
                    System.out.println("SELECTED FILE PATH IS--"+ filePath);

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;



                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);

                        if(type.equals("pdf")||type.equals("doc")||type.equals("docx"))
                        {
                            attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                            System.out.println("BASE 64 STRING------"+ attachmentstr);

                            if (uriString.startsWith("content://")) {
                                Cursor cursor = null;
                                try {
                                    cursor = EditCredentialActivity.this.getContentResolver().query(uri, null, null, null, null);
                                    if (cursor != null && cursor.moveToFirst()) {
                                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        System.out.println("displayName 1 is------"+ displayName);
                                        uploadresumetxt.setText(displayName);
                                    }
                                } finally {
                                    cursor.close();
                                }
                            } else if (uriString.startsWith("file://")) {
                                displayName = myFile.getName();
                                System.out.println("displayName 2 is------"+ displayName);
                                uploadresumetxt.setText(displayName);
                            }
                        }
                        else {

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Please upload the resume in following formats .pdf,.doc,.docx only", Snackbar.LENGTH_LONG);

                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }


    @Override
    public void onBackPressed() {

                Intent i = new Intent(EditCredentialActivity.this,CandidateDetailsActivity.class);
                startActivity(i);


    }

    private void deleteCandCredentialAPI(String url){

        pdia = new ProgressDialog(EditCredentialActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF DELETE CAND CREDENTIAL API IS---" + response);

                        JSONObject deletecandcredentialjson = null;
                        try {
                            deletecandcredentialjson = new JSONObject(response);

                            System.out.println("DELETE CAND CREDENTIAL JSON IS---" + deletecandcredentialjson);

                            String statusstr = deletecandcredentialjson.getString("status");
                            String msgstr = deletecandcredentialjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditCredentialActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditCredentialActivity.this);
        requestQueue.add(stringRequest);
    }
    
    private void insertUpdateCredentialAPI(String url,final String title,final String description){

        pdia = new ProgressDialog(EditCredentialActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF INSERT UPDATE CREDENTIAL API IS---" + response);

                        JSONObject insertupdatecredentialjson = null;
                        try {
                            insertupdatecredentialjson = new JSONObject(response);

                            System.out.println("INSERT UPDATE CREDENTIAL JSON IS---" + insertupdatecredentialjson);

                            String statusstr = insertupdatecredentialjson.getString("status");
                            String msgstr = insertupdatecredentialjson.getString("message");

                            if(statusstr.equalsIgnoreCase("1"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditCredentialActivity.this, CandidateDetailsActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("candidate_Id",candidateidstr);
                params.put("credential_name",title);
                params.put("credential_description",description);
                params.put("isVerified","0");
                params.put("modified",currentDateTimeString);

                if(attachmentstr==null)
                {
                    attachmentstr = "0";
                    params.put("attachment",attachmentstr);
                    //  params.put("extension","");
                }
                else
                {
                    params.put("attachment",attachmentstr);
                    //   params.put("extension",type);
                }


                if(attachmentstr==null)
                {
                    upload_condition_str = "0";
                }
                else
                {
                    upload_condition_str = "1";
                }

                params.put("upload_Condition",upload_condition_str);



                System.out.println("PARAMS OF insertUpdateCredential API==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditCredentialActivity.this);
        requestQueue.add(stringRequest);
    }

    private void GetCandCredentialsAPI(String url) {

        pdia = new ProgressDialog(EditCredentialActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF GET CAND CREDENTIAL API IS---" + response);


                        JSONObject candidateacaddetailsjson = null;
                        try {
                            candidateacaddetailsjson = new JSONObject(response);

                            final JSONArray CandidateCredentialsJSONArray = candidateacaddetailsjson.getJSONArray("CandidateCredentials");

                            System.out.println("credentialid is---" + SingletonActivity.credentialid);

                            if(!SingletonActivity.credentialid.equalsIgnoreCase("")) {
                                final int pos = SingletonActivity.credentialpos;
                                System.out.println("CandidateCredentialsJSONArray position IS---" + pos);
                                System.out.println("CandidateCredentialsJSONArray IS---" + CandidateCredentialsJSONArray.get(pos));

                                crededt.setText(CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_name"));
                                descriptionedt.setText(CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_description"));

                                System.out.println("credential_upload in EditCredential is---" + CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_upload").length());

                                if(CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_upload").length()>4)
                                {
                                    docimg.setVisibility(View.VISIBLE);
                                    uploadeddocumenttxt.setVisibility(View.VISIBLE);

                                    String image_url = CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_upload");
                                    String final_image_url = APIName.IMAGE_URL + image_url;

                                    Uri uri = Uri.parse(final_image_url);
                                    String token = uri.getLastPathSegment();

                                    System.out.println("token IS---" + token);

                                    uploadresumetxt.setText(token);


                                    docimg.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Intent i = new Intent(EditCredentialActivity.this,WebViewActivity.class);
                                            try {
                                                SingletonActivity.certificateuploadurl = CandidateCredentialsJSONArray.getJSONObject(pos).getString("credential_upload");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            startActivity(i);

                                        }
                                    });

                                }
                                else
                                {
                                    uploadeddocumenttxt.setVisibility(View.GONE);
                                    docimg.setVisibility(View.GONE);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditCredentialActivity.this);
        requestQueue.add(stringRequest);
    }

}
