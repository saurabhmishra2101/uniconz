package com.smtc.uniconz;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CandidateDetailsActivity extends AppCompatActivity
         {

    Typeface roboto_light_font, roboto_medium_font, bebas_font;
    TextView candidatenametxt, designationtxt, upload_from_memory, click_image_from_camera, lastupdatedtxt, personaldetailstxt, profilecompletenesstxt, percenttxt, editpersonaldetailstxt;
    ImageView profileiv, cancel_dialog;
    private static final int CAPTURE_IMAGE_CAPTURE_CODE = 1888;
    private static final int SELECT_PICTURE = 1;
    CoordinatorLayout coordinatorLayout;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String candidateidstr;
    TextView candidatenametitletxt, candidatenamedettxt, dobtitletxt, dobdetailtxt, emailtitletxt, emaildetailtxt, mobilenotitletxt, mobilenumdetailtxt, pancardtitletxt, pancarddetailtxt, passporttitletxt, passportdetailtxt, aadharnotitletxt, aadharnodetailtxt, currentaddresstitletxt, currentaddressdetailtxt, permanentaddresstitletxt, permanentaddressdetailtxt;
    TextView noacadqualitxt, acadqualiftxt, addacadqualiftxt, keyskilltxt,addkeyskilltxt,nokeyskilltxt,professionalcoursetxt,addprofcoursetxt,noprofcoursetxt;
    TextView professionalsummarytxt,addprofsummarytxt,noprofsummarytxt;
    TextView addprefdetailstxt,preferencetxt,nopreferencetxt,preferredlocationtitletxt,preferredlocationdettxt,preferredcompanytitletxt,preferredcompanydettxt,notpreferredcompanytitletxt,notpreferredcompanydettxt;
    ProgressDialog pdia;
    CustomAdap customadap;
    CustomKeySkillAdap customkeyskilladap;
    CustomProfCourseAdap customprofcourseadap;
    CustomProfSummaryAdap customprofsummaryadap;
    CustomCredentialsAdap customcredentialsadap;
    RelativeLayout salarydetailsrel,preferencerel;

    ListView acadqualilistvw,keyskilllistvw,profcourselistvw,profsummarylistvw;
    ArrayList<String> academicidlist = new ArrayList<String>();
    ArrayList<String> academicnamelist = new ArrayList<String>();
    ArrayList<String> yearlist = new ArrayList<String>();
    ArrayList<String> aggregatelist = new ArrayList<String>();
    ArrayList<String> institutelist = new ArrayList<String>();
    ArrayList<String> universitylist = new ArrayList<String>();

    ArrayList<String> candidateskillidlist = new ArrayList<String>();
    ArrayList<String> skillidlist = new ArrayList<String>();
    ArrayList<String> skillnamelist = new ArrayList<String>();
    ArrayList<String> skilldetaillist = new ArrayList<String>();
    ArrayList<String> proficiencylevellist = new ArrayList<String>();

    ArrayList<String> certificationnamelist = new ArrayList<String>();
    ArrayList<String> certificationyearlist = new ArrayList<String>();
    ArrayList<String> certificationinstitutelist = new ArrayList<String>();
    ArrayList<String> certificationlocationlist = new ArrayList<String>();

    ArrayList<String> companynamelist = new ArrayList<String>();
    ArrayList<String> locationlist = new ArrayList<String>();
    ArrayList<String> designationlist = new ArrayList<String>();
    ArrayList<String> startdatelist = new ArrayList<String>();
    ArrayList<String> enddatelist = new ArrayList<String>();
    ArrayList<String> roledescriptionlist = new ArrayList<String>();

    ArrayList<String> credentialnamelist = new ArrayList<String>();
    ArrayList<String> credentialdesclist = new ArrayList<String>();
    ArrayList<String> credentialuploadlist = new ArrayList<String>();
    ArrayList<String> certificationidlist = new ArrayList<String>();
    ArrayList<String> credentialidlist = new ArrayList<String>();
    ArrayList<String> experienceidlist = new ArrayList<String>();


    ArrayList<String> additional_detail_Id_list = new ArrayList<String>();
    ArrayList<String> isAgreed_list = new ArrayList<String>();

    JSONArray CandAdditional,OtherReference,NonPrefCompany,PrefCompany,PrefLocation,CandBankDetail,CandAcademicSummary,CandKeySkillSummary,CandCertificationSummary,CandProfSummary,CandCredentials,CandSalary;
    RelativeLayout personaldetailsrel;

    TextView credentialstxt,addcredentialstxt,nocredentialstxt;
    TextView salarydetailstxt,addsalarydetailstxt,nosalarydetailstxt,currentctcdettxt,expectedctcdetailtxt,currentctctitletxt,expectedctctitletxt;
    ListView credentialslistvw;

    TextView bankdetailstxt,addbankdetailstxt,nobankdetailstxt,nametitletxt,namedettxt,accountnumtitletxt,accountnumdetailtxt,ifsccodetitletxt,ifsccodedetailtxt;
    RelativeLayout bankdetailsrel;

    TextView locationref1titletxt,locationref1desctxt,referencetxt,addrefdetailstxt,noreferencetxt,namemobile1dettxt,emailref1titletxt,emailref1desctxt,designationref1titletxt,designationref1desctxt,companyref1titletxt,companyref1desctxt;
    RelativeLayout reference1rel,reference2rel;

    TextView locationref2titletxt,locationref2desctxt,namemobile2dettxt,emailref2titletxt,emailref2desctxt,designationref2titletxt,designationref2desctxt,companyref2titletxt,companyref2desctxt;

    TextView otherpreferencetxt,addotherprefdetailstxt,nootherpreferencetxt,otherpreference1titletxt,otherpreference2titletxt,otherpreference3titletxt,otherpreference4titletxt,otherpreference5titletxt,otherpreference6titletxt,otherpreference7titletxt,otherpreference8titletxt;
    RelativeLayout otherpreferencerel;

    RelativeLayout saverelative;
    ImageView backiv;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_scroll_details_coordinator);


        backiv = (ImageView)findViewById(R.id.backiv);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,DashboardActivity.class);
                startActivity(i);

              //  finish();
            }
        });

        otherpreferencetxt = (TextView) findViewById(R.id.otherpreferencetxt);
        addotherprefdetailstxt = (TextView) findViewById(R.id.addotherprefdetailstxt);
        nootherpreferencetxt = (TextView) findViewById(R.id.nootherpreferencetxt);
        otherpreference1titletxt = (TextView) findViewById(R.id.otherpreference1titletxt);
        otherpreference2titletxt = (TextView) findViewById(R.id.otherpreference2titletxt);
        otherpreference3titletxt = (TextView) findViewById(R.id.otherpreference3titletxt);
        otherpreference4titletxt = (TextView) findViewById(R.id.otherpreference4titletxt);
        otherpreference5titletxt = (TextView) findViewById(R.id.otherpreference5titletxt);
        otherpreference6titletxt = (TextView) findViewById(R.id.otherpreference6titletxt);
        otherpreference7titletxt = (TextView) findViewById(R.id.otherpreference7titletxt);


        otherpreferencerel = (RelativeLayout)findViewById(R.id.otherpreferencerel);



        namemobile2dettxt = (TextView) findViewById(R.id.namemobile2dettxt);
        emailref2titletxt = (TextView) findViewById(R.id.emailref2titletxt);
        emailref2desctxt = (TextView) findViewById(R.id.emailref2desctxt);
        designationref2titletxt = (TextView) findViewById(R.id.designationref2titletxt);
        designationref2desctxt = (TextView) findViewById(R.id.designationref2desctxt);
        companyref2titletxt = (TextView) findViewById(R.id.companyref2titletxt);
        companyref2desctxt = (TextView) findViewById(R.id.companyref2desctxt);
        locationref2titletxt = (TextView) findViewById(R.id.locationref2titletxt);
        locationref2desctxt = (TextView) findViewById(R.id.locationref2desctxt);

        locationref1titletxt = (TextView) findViewById(R.id.locationref1titletxt);
        locationref1desctxt = (TextView) findViewById(R.id.locationref1desctxt);
        referencetxt = (TextView) findViewById(R.id.referencetxt);
        addrefdetailstxt = (TextView) findViewById(R.id.addrefdetailstxt);
        noreferencetxt = (TextView) findViewById(R.id.noreferencetxt);
        namemobile1dettxt = (TextView) findViewById(R.id.namemobile1dettxt);
        emailref1titletxt = (TextView) findViewById(R.id.emailref1titletxt);
        emailref1desctxt = (TextView) findViewById(R.id.emailref1desctxt);
        designationref1titletxt = (TextView) findViewById(R.id.designationref1titletxt);
        designationref1desctxt = (TextView) findViewById(R.id.designationref1desctxt);
        companyref1titletxt = (TextView) findViewById(R.id.companyref1titletxt);
        companyref1desctxt = (TextView) findViewById(R.id.companyref1desctxt);

        reference1rel = (RelativeLayout)findViewById(R.id.reference1rel);
        reference2rel = (RelativeLayout)findViewById(R.id.reference2rel);
        preferencerel = (RelativeLayout)findViewById(R.id.preferencerel);

        addprefdetailstxt = (TextView) findViewById(R.id.addprefdetailstxt);
        preferencetxt = (TextView) findViewById(R.id.preferencetxt);
        nopreferencetxt = (TextView) findViewById(R.id.nopreferencetxt);
        preferredlocationtitletxt = (TextView) findViewById(R.id.preferredlocationtitletxt);
        preferredlocationdettxt = (TextView) findViewById(R.id.preferredlocationdettxt);
        preferredcompanytitletxt = (TextView) findViewById(R.id.preferredcompanytitletxt);
        preferredcompanydettxt = (TextView) findViewById(R.id.preferredcompanydettxt);
        notpreferredcompanytitletxt = (TextView) findViewById(R.id.notpreferredcompanytitletxt);
        notpreferredcompanydettxt = (TextView) findViewById(R.id.notpreferredcompanydettxt);

        bankdetailstxt = (TextView) findViewById(R.id.bankdetailstxt);
        addbankdetailstxt = (TextView) findViewById(R.id.addbankdetailstxt);
        nobankdetailstxt = (TextView) findViewById(R.id.nobankdetailstxt);
        nametitletxt = (TextView) findViewById(R.id.nametitletxt);
        namedettxt = (TextView) findViewById(R.id.namedettxt);
        accountnumtitletxt = (TextView) findViewById(R.id.accountnumtitletxt);
        accountnumdetailtxt = (TextView) findViewById(R.id.accountnumdetailtxt);
        ifsccodetitletxt = (TextView) findViewById(R.id.ifsccodetitletxt);
        ifsccodedetailtxt = (TextView) findViewById(R.id.ifsccodedetailtxt);
        bankdetailsrel =  (RelativeLayout)findViewById(R.id.bankdetailsrel);

        salarydetailsrel = (RelativeLayout)findViewById(R.id.salarydetailsrel);
        personaldetailsrel = (RelativeLayout)findViewById(R.id.personaldetailsrel);
        candidatenametxt = (TextView) findViewById(R.id.candidatenametxt);
        designationtxt = (TextView) findViewById(R.id.designationtxt);
        lastupdatedtxt = (TextView) findViewById(R.id.lastupdatedtxt);
        personaldetailstxt = (TextView) findViewById(R.id.personaldetailstxt);
        profilecompletenesstxt = (TextView) findViewById(R.id.profilecompletenesstxt);
        percenttxt = (TextView) findViewById(R.id.percenttxt);
        editpersonaldetailstxt = (TextView) findViewById(R.id.editpersonaldetailstxt);
        candidatenametitletxt = (TextView) findViewById(R.id.candidatenametitletxt);
        candidatenamedettxt = (TextView) findViewById(R.id.candidatenamedettxt);
        dobtitletxt = (TextView) findViewById(R.id.dobtitletxt);
        dobdetailtxt = (TextView) findViewById(R.id.dobdetailtxt);
        emailtitletxt = (TextView) findViewById(R.id.emailtitletxt);
        emaildetailtxt = (TextView) findViewById(R.id.emaildetailtxt);
        mobilenotitletxt = (TextView) findViewById(R.id.mobilenotitletxt);
        mobilenumdetailtxt = (TextView) findViewById(R.id.mobilenumdetailtxt);
        pancardtitletxt = (TextView) findViewById(R.id.pancardtitletxt);
        pancarddetailtxt = (TextView) findViewById(R.id.pancarddetailtxt);
        passporttitletxt = (TextView) findViewById(R.id.passporttitletxt);
        passportdetailtxt = (TextView) findViewById(R.id.passportdetailtxt);
        aadharnotitletxt = (TextView) findViewById(R.id.aadharnotitletxt);
        aadharnodetailtxt = (TextView) findViewById(R.id.aadharnodetailtxt);
        currentaddresstitletxt = (TextView) findViewById(R.id.currentaddresstitletxt);
        currentaddressdetailtxt = (TextView) findViewById(R.id.currentaddressdetailtxt);
        permanentaddresstitletxt = (TextView) findViewById(R.id.permanentaddresstitletxt);
        permanentaddressdetailtxt = (TextView) findViewById(R.id.permanentaddressdetailtxt);
        noacadqualitxt = (TextView) findViewById(R.id.noacadqualitxt);
        acadqualiftxt = (TextView) findViewById(R.id.acadqualiftxt);
        addacadqualiftxt = (TextView) findViewById(R.id.addacadqualiftxt);
        keyskilltxt= (TextView) findViewById(R.id.keyskilltxt);
        addkeyskilltxt =  (TextView) findViewById(R.id.addkeyskilltxt);

        saverelative = (RelativeLayout) findViewById(R.id.saverelative);

        addbankdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditBankDetailsActivity.class);
                SingletonActivity.bankid = "";
                startActivity(i);
            }
        });

        addkeyskilltxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CandidateDetailsActivity.this,EditKeySkillActivity.class);
                SingletonActivity.candidateskillid = "";
                SingletonActivity.skillid = "";
                startActivity(i);
            }
        });

        nokeyskilltxt = (TextView) findViewById(R.id.nokeyskilltxt);
        professionalcoursetxt = (TextView) findViewById(R.id.professionalcoursetxt);
        addprofcoursetxt = (TextView) findViewById(R.id.addprofcoursetxt);
        noprofcoursetxt = (TextView) findViewById(R.id.noprofcoursetxt);
        professionalsummarytxt = (TextView)findViewById(R.id.professionalsummarytxt);
        addprofsummarytxt = (TextView)findViewById(R.id.addprofsummarytxt);
        noprofsummarytxt = (TextView)findViewById(R.id.noprofsummarytxt);

        credentialstxt = (TextView)findViewById(R.id.credentialstxt);
        addcredentialstxt = (TextView)findViewById(R.id.addcredentialstxt);
        nocredentialstxt = (TextView)findViewById(R.id.nocredentialstxt);

        credentialslistvw = (ListView) findViewById(R.id.credentialslistvw);

        salarydetailstxt = (TextView)findViewById(R.id.salarydetailstxt);
        addsalarydetailstxt = (TextView)findViewById(R.id.addsalarydetailstxt);
        nosalarydetailstxt = (TextView)findViewById(R.id.nosalarydetailstxt);

        currentctctitletxt = (TextView)findViewById(R.id.currentctctitletxt);
        expectedctctitletxt = (TextView)findViewById(R.id.expectedctctitletxt);

        currentctcdettxt = (TextView)findViewById(R.id.currentctcdettxt);
        expectedctcdetailtxt = (TextView)findViewById(R.id.expectedctcdetailtxt);

        acadqualilistvw = (ListView) findViewById(R.id.acadqualilistvw);
        acadqualilistvw.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);

                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        keyskilllistvw = (ListView)findViewById(R.id.keyskilllistvw);
        keyskilllistvw.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);

                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        profcourselistvw = (ListView)findViewById(R.id.profcourselistvw);
        profcourselistvw.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);

                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });




        profsummarylistvw = (ListView)findViewById(R.id.profsummarylistvw);
        profsummarylistvw.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);


       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);


      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

      /*  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
*/
        profileiv = (ImageView) findViewById(R.id.profileiv);

        profileiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog mDialog = new Dialog(CandidateDetailsActivity.this, R.style.DialogSlideAnim);
                mDialog.setCancelable(false);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mDialog.setContentView(R.layout.dialog_upload_image);
                cancel_dialog = (ImageView) mDialog
                        .findViewById(R.id.cancel_dialog);
                upload_from_memory = (TextView) mDialog
                        .findViewById(R.id.upload_from_memory);
                click_image_from_camera = (TextView) mDialog
                        .findViewById(R.id.click_image_from_camera);
                upload_from_memory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                        mDialog.dismiss();

                    }
                });
                click_image_from_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                        mDialog.dismiss();

                    }
                });
                cancel_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();

            }
        });

        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        roboto_medium_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Medium.ttf");
        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");


        candidatenametxt.setTypeface(roboto_light_font);
        designationtxt.setTypeface(roboto_light_font);
        profilecompletenesstxt.setTypeface(roboto_light_font);
        lastupdatedtxt.setTypeface(roboto_light_font);
        percenttxt.setTypeface(roboto_light_font);
        editpersonaldetailstxt.setTypeface(roboto_light_font);

        personaldetailstxt.setTypeface(bebas_font);
        acadqualiftxt.setTypeface(bebas_font);
        keyskilltxt.setTypeface(bebas_font);
        professionalcoursetxt.setTypeface(bebas_font);
        professionalsummarytxt.setTypeface(bebas_font);
        credentialstxt.setTypeface(bebas_font);
        salarydetailstxt.setTypeface(bebas_font);
        bankdetailstxt.setTypeface(bebas_font);
        preferencetxt.setTypeface(bebas_font);
        referencetxt.setTypeface(bebas_font);

        candidatenametitletxt.setTypeface(roboto_light_font);
        candidatenamedettxt.setTypeface(roboto_light_font);
        dobtitletxt.setTypeface(roboto_light_font);
        dobdetailtxt.setTypeface(roboto_light_font);
        emailtitletxt.setTypeface(roboto_light_font);
        emaildetailtxt.setTypeface(roboto_light_font);
        mobilenotitletxt.setTypeface(roboto_light_font);
        mobilenumdetailtxt.setTypeface(roboto_light_font);
        pancardtitletxt.setTypeface(roboto_light_font);
        pancarddetailtxt.setTypeface(roboto_light_font);
        passporttitletxt.setTypeface(roboto_light_font);
        passportdetailtxt.setTypeface(roboto_light_font);
        aadharnotitletxt.setTypeface(roboto_light_font);
        aadharnodetailtxt.setTypeface(roboto_light_font);
        currentaddresstitletxt.setTypeface(roboto_light_font);
        currentaddressdetailtxt.setTypeface(roboto_light_font);
        permanentaddresstitletxt.setTypeface(roboto_light_font);
        permanentaddressdetailtxt.setTypeface(roboto_light_font);

        noacadqualitxt.setTypeface(roboto_light_font);
        addacadqualiftxt.setTypeface(roboto_light_font);

        addkeyskilltxt.setTypeface(roboto_light_font);
        nokeyskilltxt.setTypeface(roboto_light_font);

        addprofcoursetxt.setTypeface(roboto_light_font);
        noprofcoursetxt.setTypeface(roboto_light_font);

        addprofsummarytxt.setTypeface(roboto_light_font);
        noprofsummarytxt.setTypeface(roboto_light_font);

        addcredentialstxt.setTypeface(roboto_light_font);
        nocredentialstxt.setTypeface(roboto_light_font);

        addsalarydetailstxt.setTypeface(roboto_light_font);
        nosalarydetailstxt.setTypeface(roboto_light_font);

        currentctctitletxt.setTypeface(roboto_light_font);
        expectedctctitletxt.setTypeface(roboto_light_font);

        currentctcdettxt.setTypeface(roboto_light_font);
        expectedctcdetailtxt.setTypeface(roboto_light_font);


        addbankdetailstxt.setTypeface(roboto_light_font);
        nobankdetailstxt.setTypeface(roboto_light_font);
        nametitletxt.setTypeface(roboto_light_font);
        namedettxt.setTypeface(roboto_light_font);
        accountnumtitletxt.setTypeface(roboto_light_font);
        accountnumdetailtxt.setTypeface(roboto_light_font);
        ifsccodetitletxt.setTypeface(roboto_light_font);
        ifsccodedetailtxt.setTypeface(roboto_light_font);


        addprefdetailstxt.setTypeface(roboto_light_font);
        nopreferencetxt.setTypeface(roboto_light_font);
        preferredlocationtitletxt.setTypeface(roboto_light_font);
        preferredlocationdettxt.setTypeface(roboto_light_font);
        preferredcompanytitletxt.setTypeface(roboto_light_font);
        preferredcompanydettxt.setTypeface(roboto_light_font);
        notpreferredcompanytitletxt.setTypeface(roboto_light_font);
        notpreferredcompanydettxt.setTypeface(roboto_light_font);

        addrefdetailstxt.setTypeface(roboto_light_font);
        noreferencetxt.setTypeface(roboto_light_font);
        namemobile1dettxt.setTypeface(roboto_light_font);
        emailref1titletxt.setTypeface(roboto_light_font);
        emailref1desctxt.setTypeface(roboto_light_font);
        designationref1titletxt.setTypeface(roboto_light_font);
        designationref1desctxt.setTypeface(roboto_light_font);
        companyref1titletxt.setTypeface(roboto_light_font);
        companyref1desctxt.setTypeface(roboto_light_font);
        locationref1titletxt.setTypeface(roboto_light_font);
        locationref1desctxt.setTypeface(roboto_light_font);

        namemobile2dettxt.setTypeface(roboto_light_font);
        emailref2titletxt.setTypeface(roboto_light_font);
        emailref2desctxt.setTypeface(roboto_light_font);
        designationref2titletxt.setTypeface(roboto_light_font);
        designationref2desctxt.setTypeface(roboto_light_font);
        companyref2titletxt.setTypeface(roboto_light_font);
        companyref2desctxt.setTypeface(roboto_light_font);
        locationref2titletxt.setTypeface(roboto_light_font);
        locationref2desctxt.setTypeface(roboto_light_font);

        otherpreferencetxt.setTypeface(bebas_font);
        addotherprefdetailstxt.setTypeface(roboto_light_font);
        nootherpreferencetxt.setTypeface(roboto_light_font);
        otherpreference1titletxt.setTypeface(roboto_light_font);
        otherpreference2titletxt.setTypeface(roboto_light_font);
        otherpreference3titletxt.setTypeface(roboto_light_font);
        otherpreference4titletxt.setTypeface(roboto_light_font);
        otherpreference5titletxt.setTypeface(roboto_light_font);
        otherpreference6titletxt.setTypeface(roboto_light_font);
        otherpreference7titletxt.setTypeface(roboto_light_font);


        otherpreferencerel = (RelativeLayout)findViewById(R.id.otherpreferencerel);

        addrefdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CandidateDetailsActivity.this,EditReferenceContactActivity.class);

                startActivity(i);

            }
        });

        addprefdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditJobPreferenceActivity.class);
                //   SingletonActivity. = "";
                startActivity(i);

            }
        });

        addotherprefdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditOtherPreferenceActivity.class);
             //   SingletonActivity. = "";
                startActivity(i);
            }
        });

        addsalarydetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditSalaryActivity.class);
                SingletonActivity.salaryid = "";
                startActivity(i);
            }
        });

        addprofsummarytxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditProfessionalSummaryActivity.class);
                SingletonActivity.experienceid = "";

                startActivity(i);
            }
        });

        addcredentialstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditCredentialActivity.class);
                SingletonActivity.credentialid = "";

                startActivity(i);
            }
        });

        addprofcoursetxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CandidateDetailsActivity.this,EditProfessionalCourseActivity.class);
                SingletonActivity.certificationid = "";

                startActivity(i);
            }
        });

        addacadqualiftxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CandidateDetailsActivity.this,EditAcademicQualificationsActivity.class);
                SingletonActivity.academicid = "";
                startActivity(i);
            }
        });

        editpersonaldetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CandidateDetailsActivity.this,EditPersonalDetailActivity.class);
                startActivity(i);
            }
        });


        if (NetworkUtility.checkConnectivity(CandidateDetailsActivity.this)) {
            String GetCandidateAllDetailsURL = APIName.URL + "/candidate/getCandidateAllDetails?candidate_Id=" + candidateidstr;
            System.out.println("GET CANDIDATE ALL DETAILS URL IS---" + GetCandidateAllDetailsURL);
            GetCandidateAllDetailsAPI(GetCandidateAllDetailsURL);

        } else {
            // util.dialog(LoginActivity.this, "Please check your internet connection.");

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }


        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtility.checkConnectivity(CandidateDetailsActivity.this)) {
                    String SubmitProfileURL = APIName.URL + "/candidate/submitProfile?candidate_Id=" + candidateidstr;
                    System.out.println("SUBMIT PROFILE URL IS---" + SubmitProfileURL);
                    SubmitProfileAPI(SubmitProfileURL);

                } else {
                    // util.dialog(LoginActivity.this, "Please check your internet connection.");

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }

            }
        });

    }

    private void SubmitProfileAPI(String url){

        pdia = new ProgressDialog(CandidateDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF SUBMIT PROFILE API IS---" + response);

                        JSONObject submitprofilejson = null;
                        try {
                            submitprofilejson = new JSONObject(response);

                            System.out.println("SUBMIT PROFILE JSON IS---" + submitprofilejson);

                            String statusstr = submitprofilejson.getString("status");
                            String msgstr = submitprofilejson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, msgstr, Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.GREEN);
                                snackbar.show();



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("PARAMS OF SubmitProfile==="+ params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(CandidateDetailsActivity.this);
        requestQueue.add(stringRequest);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                Uri selectedImageURI = data.getData();

                Picasso.with(CandidateDetailsActivity.this).load(selectedImageURI).noPlaceholder().centerCrop().fit()
                        .into((profileiv));
            }

        }

        if (requestCode == CAPTURE_IMAGE_CAPTURE_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            profileiv.setImageBitmap(photo);
        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(CandidateDetailsActivity.this,DashboardActivity.class);
        startActivity(i);
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.candidate_details, menu);
        return true;
    }*/

    private void GetCandidateAllDetailsAPI(String url) {

        pdia = new ProgressDialog(CandidateDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();
                    academicnamelist.clear();
                     yearlist.clear();
                      aggregatelist.clear();
                       institutelist.clear();
                     universitylist.clear();
                     skillnamelist.clear();
                        skillidlist.clear();
                     candidateskillidlist.clear();
                     skilldetaillist.clear();
                     proficiencylevellist.clear();
                     certificationnamelist.clear();
                     certificationyearlist.clear();
                     certificationinstitutelist.clear();
                     certificationlocationlist.clear();

                     companynamelist.clear();
                     locationlist.clear();
                     designationlist.clear();
                     startdatelist.clear();
                     enddatelist.clear();
                     roledescriptionlist.clear();

                        credentialnamelist.clear();
                        credentialdesclist.clear();
                        credentialuploadlist.clear();

                        academicidlist.clear();
                        certificationidlist.clear();
                        credentialidlist.clear();
                        experienceidlist.clear();

                        additional_detail_Id_list.clear();
                        isAgreed_list.clear();

                        System.out.println("RESPONSE OF GET CANDIDATE ALL DETAILS API IS---" + response);

                        JSONObject candidatedetailsjson = null;
                        try {
                            candidatedetailsjson = new JSONObject(response);

                            final JSONObject CandidateDetails = candidatedetailsjson.getJSONObject("CandidateAllDetails");

                            String candidatenamestr = CandidateDetails.getString("candidate_name");
                            candidatenametxt.setText(candidatenamestr);
                            candidatenamedettxt.setText(candidatenamestr);

                            String dobstr = CandidateDetails.getString("dob");
                            System.out.println("LENGTH OF DOB---" + dobstr.length());
                            if (dobstr.length() == 0 || dobstr.length() == 4) {
                                dobdetailtxt.setText("Not Mentioned");
                            } else {
                                dobdetailtxt.setText(dobstr);
                            }

                            String emailstr = CandidateDetails.getString("email");
                            emaildetailtxt.setText(emailstr);

                            String mobnumstr = CandidateDetails.getString("mob_number");
                            mobilenumdetailtxt.setText(mobnumstr);

                            String pancardnostr = CandidateDetails.getString("pan_card_no");

                            if (pancardnostr.length() == 4) {
                                pancarddetailtxt.setText("Not Mentioned");
                            } else {
                                pancarddetailtxt.setText(pancardnostr);
                            }

                            String passportnostr = CandidateDetails.getString("passport_no");
                            if (passportnostr.length() == 0 || passportnostr.length() == 4) {
                                passportdetailtxt.setText("Not Mentioned");
                            } else {
                                passportdetailtxt.setText(passportnostr);
                            }

                            String currentaddressstr = CandidateDetails.getString("current_address");
                            if (currentaddressstr.length() == 0 || currentaddressstr.length() == 4) {
                                currentaddressdetailtxt.setText("Not Mentioned");
                            } else {
                                currentaddressdetailtxt.setText(currentaddressstr);
                            }

                            String permanentaddressstr = CandidateDetails.getString("permanent_address");
                            if (permanentaddressstr.length() == 0 || permanentaddressstr.length() == 4) {
                                permanentaddressdetailtxt.setText("Not Mentioned");
                            } else {
                                permanentaddressdetailtxt.setText(permanentaddressstr);
                            }


                            String aadharnostr = CandidateDetails.getString("aadhar_no");
                            if (aadharnostr.length() == 0 || aadharnostr.length() == 4) {
                                aadharnodetailtxt.setText("Not Mentioned");
                            } else {
                                aadharnodetailtxt.setText(aadharnostr);
                            }


                            String lastupdatedstr = CandidateDetails.getString("last_modified");

                            //  String[] dateTimeArray = lastupdatedstr.split(" ");

                            lastupdatedtxt.setText("Last Updated: " + lastupdatedstr);
                            //lastupdatedtxt.setText("Last Updated: "+dateTimeArray[0]);

                            personaldetailsrel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                  //  Toast.makeText(CandidateDetailsActivity.this,"PERSONAL DETAILS CLICKED",Toast.LENGTH_SHORT).show();
                                }
                            });

                            //============Reference=========================

                           /* TextView locationref1titletxt,locationref1desctxt,referencetxt,addrefdetailstxt,noreferencetxt,namemobile1dettxt,emailref1titletxt,emailref1desctxt,designationref1titletxt,designationref1desctxt,companyref1titletxt,companyref1desctxt;
                            RelativeLayout reference1rel,reference2rel;

                            TextView locationref2titletxt,locationref2desctxt,namemobile2dettxt,emailref2titletxt,emailref2desctxt,designationref2titletxt,designationref2desctxt,companyref2titletxt,companyref2desctxt;
*/

                            System.out.println("LENGTH=====" + CandidateDetails.getString("OtherReference").length());
                            if (CandidateDetails.getString("OtherReference").length() == 4)
                            //your rest of codes
                            {

                                noreferencetxt.setVisibility(View.VISIBLE);
                                reference1rel.setVisibility(View.GONE);
                                reference2rel.setVisibility(View.GONE);

                            } else {

                                noreferencetxt.setVisibility(View.GONE);
                                reference1rel.setVisibility(View.VISIBLE);
                                reference2rel.setVisibility(View.VISIBLE);

                                OtherReference = CandidateDetails.getJSONArray("OtherReference");
                                System.out.println("JSONARRAY OtherReference length=====" + OtherReference.length());

                             /*   if(OtherReference.length()==0)
                                {
                                    preferredlocationdettxt.setText("No Data");

                                }*/

                                if(OtherReference.length()==1)
                                {
                                    namemobile1dettxt.setText(OtherReference.getJSONObject(0).getString("name")+"("+OtherReference.getJSONObject(0).getString("mob_number")+")");
                                    emailref1desctxt.setText(OtherReference.getJSONObject(0).getString("email"));
                                    designationref1desctxt.setText(OtherReference.getJSONObject(0).getString("designation"));
                                    companyref1desctxt.setText(OtherReference.getJSONObject(0).getString("company"));
                                    locationref1desctxt.setText(OtherReference.getJSONObject(0).getString("location"));

                                    reference2rel.setVisibility(View.GONE);

                                }
                                if(OtherReference.length()==2)
                                {
                                    namemobile1dettxt.setText(OtherReference.getJSONObject(0).getString("name")+"("+OtherReference.getJSONObject(0).getString("mob_number")+")");
                                    emailref1desctxt.setText(OtherReference.getJSONObject(0).getString("email"));
                                    designationref1desctxt.setText(OtherReference.getJSONObject(0).getString("designation"));
                                    companyref1desctxt.setText(OtherReference.getJSONObject(0).getString("company"));
                                    locationref1desctxt.setText(OtherReference.getJSONObject(0).getString("location"));

                                    namemobile2dettxt.setText(OtherReference.getJSONObject(1).getString("name")+"("+OtherReference.getJSONObject(1).getString("mob_number")+")");
                                    emailref2desctxt.setText(OtherReference.getJSONObject(1).getString("email"));
                                    designationref2desctxt.setText(OtherReference.getJSONObject(1).getString("designation"));
                                    companyref2desctxt.setText(OtherReference.getJSONObject(1).getString("company"));
                                    locationref2desctxt.setText(OtherReference.getJSONObject(1).getString("location"));


                                }


                               /* preferencerel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {
                                        Toast.makeText(CandidateDetailsActivity.this,"PREFERENCE ROW",Toast.LENGTH_SHORT).show();

                                    }
                                });
                              */



                            }

                            //============Preferred Location=========================


                            System.out.println("LENGTH=====" + CandidateDetails.getString("PrefLocation").length());
                            if (CandidateDetails.getString("PrefLocation").length() == 4)
                            //your rest of codes
                            {

                                nopreferencetxt.setVisibility(View.VISIBLE);
                                preferencerel.setVisibility(View.GONE);

                                preferredlocationdettxt.setText("No Data");




                            } else {

                                nopreferencetxt.setVisibility(View.GONE);
                                preferencerel.setVisibility(View.VISIBLE);

                               PrefLocation = CandidateDetails.getJSONArray("PrefLocation");
                                System.out.println("JSONARRAY PrefLocation length=====" + PrefLocation.length());

                                if(PrefLocation.length()==0)
                                {
                                    preferredlocationdettxt.setText("No Data");

                                }

                                if(PrefLocation.length()==1)
                                {
                                    preferredlocationdettxt.setText(PrefLocation.getJSONObject(0).getString("preference_location"));

                                }
                                if(PrefLocation.length()==2)
                                {
                                    preferredlocationdettxt.setText(PrefLocation.getJSONObject(0).getString("preference_location")+","+PrefLocation.getJSONObject(1).getString("preference_location"));

                                }

                                if(PrefLocation.length()==3)
                                {
                                    preferredlocationdettxt.setText(PrefLocation.getJSONObject(0).getString("preference_location")+","+PrefLocation.getJSONObject(1).getString("preference_location")+","+PrefLocation.getJSONObject(2).getString("preference_location"));

                                }

                                preferencerel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {

                                       // Intent i = new Intent(CandidateDetailsActivity.this,EditJobPreferenceActivity.class);
                                       // startActivity(i);

                                    }
                                });




                            }
                            //============Preferred Company=========================


                            System.out.println("LENGTH=====" + CandidateDetails.getString("PrefCompany").length());
                            if (CandidateDetails.getString("PrefCompany").length() == 4)
                            //your rest of codes
                            {

                                nopreferencetxt.setVisibility(View.GONE);
                                preferencerel.setVisibility(View.VISIBLE);

                                preferredcompanydettxt.setText("No Data");




                            } else {

                                nopreferencetxt.setVisibility(View.GONE);
                                preferencerel.setVisibility(View.VISIBLE);

                                PrefCompany = CandidateDetails.getJSONArray("PrefCompany");
                                System.out.println("JSONARRAY PrefCompany length=====" + PrefCompany.length());

                               /* if(PrefCompany.length()==0)
                                {
                                    preferredcompanydettxt.setText("No Data");

                                }*/

                                if(PrefCompany.length()==1)
                                {
                                    preferredcompanydettxt.setText(PrefCompany.getJSONObject(0).getString("preference_company"));

                                }
                                if(PrefCompany.length()==2)
                                {
                                    preferredcompanydettxt.setText(PrefCompany.getJSONObject(0).getString("preference_company")+","+PrefCompany.getJSONObject(1).getString("preference_company"));

                                }

                                if(PrefCompany.length()==3)
                                {
                                    preferredcompanydettxt.setText(PrefCompany.getJSONObject(0).getString("preference_company")+","+PrefCompany.getJSONObject(1).getString("preference_company")+","+PrefCompany.getJSONObject(2).getString("preference_company"));

                                }

                                preferencerel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {

                                       // Intent i = new Intent(CandidateDetailsActivity.this,EditJobPreferenceActivity.class);
                                      //  startActivity(i);

                                    }
                                });

                            }


                            //============Non Preferred Company=========================


                            System.out.println("LENGTH=====" + CandidateDetails.getString("NonPrefCompany").length());
                            if (CandidateDetails.getString("NonPrefCompany").length() == 4)
                            //your rest of codes
                            {

                                nopreferencetxt.setVisibility(View.GONE);
                                preferencerel.setVisibility(View.VISIBLE);

                                notpreferredcompanydettxt.setText("No Data");




                            } else {

                                nopreferencetxt.setVisibility(View.GONE);
                                preferencerel.setVisibility(View.VISIBLE);



                                NonPrefCompany = CandidateDetails.getJSONArray("NonPrefCompany");
                                System.out.println("JSONARRAY NonPrefCompany length=====" + NonPrefCompany.length());

                               /* if(PrefCompany.length()==0)
                                {
                                    preferredcompanydettxt.setText("No Data");

                                }*/

                                if(NonPrefCompany.length()==1)
                                {
                                    notpreferredcompanydettxt.setText(NonPrefCompany.getJSONObject(0).getString("non_preference_company"));

                                }
                                if(NonPrefCompany.length()==2)
                                {
                                    notpreferredcompanydettxt.setText(NonPrefCompany.getJSONObject(0).getString("non_preference_company")+","+NonPrefCompany.getJSONObject(1).getString("non_preference_company"));

                                }

                                if(NonPrefCompany.length()==3)
                                {
                                    notpreferredcompanydettxt.setText(NonPrefCompany.getJSONObject(0).getString("non_preference_company")+","+NonPrefCompany.getJSONObject(1).getString("non_preference_company")+","+NonPrefCompany.getJSONObject(2).getString("non_preference_company"));

                                }


                                preferencerel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {

                                       // Intent i = new Intent(CandidateDetailsActivity.this,EditJobPreferenceActivity.class);
                                      //  startActivity(i);

                                    }
                                });

                            }

                            //==============================Other Preference==============================

                            System.out.println("LENGTH=====" + CandidateDetails.getString("CandAdditional").length());
                            if (CandidateDetails.getString("CandAdditional").length() == 4)
                            //your rest of codes
                            {


                                nootherpreferencetxt.setVisibility(View.VISIBLE);
                                otherpreferencerel.setVisibility(View.GONE);

                            } else {

                                nootherpreferencetxt.setVisibility(View.GONE);
                                otherpreferencerel.setVisibility(View.VISIBLE);

                                CandAdditional = CandidateDetails.getJSONArray("CandAdditional");
                                System.out.println("JSONARRAY CandAdditional=====" + CandAdditional);

                                for(int i = 0; i < CandAdditional.length(); i++)
                                {


                                    additional_detail_Id_list.add(CandAdditional.getJSONObject(i).getString("additional_detail_Id"));
                                    isAgreed_list.add(CandAdditional.getJSONObject(i).getString("isAgreed"));




                                    if(isAgreed_list.contains("1"))
                                    {
                                        otherpreference1titletxt.setVisibility(View.VISIBLE);


                                    }

                                    if(isAgreed_list.contains("2"))
                                    {

                                        otherpreference2titletxt.setVisibility(View.VISIBLE);


                                    }

                                    if(isAgreed_list.contains("3"))
                                    {

                                        otherpreference3titletxt.setVisibility(View.VISIBLE);


                                    }

                                    if(isAgreed_list.contains("4"))
                                    {

                                        otherpreference4titletxt.setVisibility(View.VISIBLE);


                                    }

                                    if(isAgreed_list.contains("5"))
                                    {

                                        otherpreference5titletxt.setVisibility(View.VISIBLE);


                                    }

                                    if(isAgreed_list.contains("6"))
                                    {

                                        otherpreference6titletxt.setVisibility(View.VISIBLE);

                                    }

                                    if(isAgreed_list.contains("7"))
                                    {

                                        otherpreference7titletxt.setVisibility(View.VISIBLE);


                                    }



                                }






                                otherpreferencerel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {

                                        Intent i = new Intent(CandidateDetailsActivity.this,EditOtherPreferenceActivity.class);
                                        startActivity(i);


                                    }
                                });


                            }

                            //==============Bank Details====================================

                            System.out.println("LENGTH=====" + CandidateDetails.getString("CandBankDetail").length());
                            if (CandidateDetails.getString("CandBankDetail").length() == 4)
                            //your rest of codes
                            {

                                nobankdetailstxt.setVisibility(View.VISIBLE);
                                bankdetailsrel.setVisibility(View.GONE);

                            } else {

                                nobankdetailstxt.setVisibility(View.GONE);
                                bankdetailsrel.setVisibility(View.VISIBLE);

                                CandBankDetail = CandidateDetails.getJSONArray("CandBankDetail");
                                System.out.println("JSONARRAY CandBankDetail=====" + CandBankDetail);


                                final String bankidstr = CandBankDetail.getJSONObject(0).getString("bank_Id");
                                String name_str = CandBankDetail.getJSONObject(0).getString("name");
                                String ifsc_code_str = CandBankDetail.getJSONObject(0).getString("ifsc_code");
                                String account_number_str = CandBankDetail.getJSONObject(0).getString("account_number");


                                namedettxt.setText(name_str);
                                accountnumdetailtxt.setText(ifsc_code_str);
                                ifsccodedetailtxt.setText(account_number_str);

                                bankdetailsrel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {

                                        Intent i = new Intent(CandidateDetailsActivity.this,EditBankDetailsActivity.class);
                                        SingletonActivity.bankid = bankidstr;
                                        startActivity(i);
                                    }
                                });


                            }


                            //==============Salary Details====================================




                            System.out.println("LENGTH of CandSalary=====" + CandidateDetails.getString("CandSalary").length());
                            if (CandidateDetails.getString("CandSalary").length() == 4)
                            //your rest of codes
                            {

                                nosalarydetailstxt.setVisibility(View.VISIBLE);
                                salarydetailsrel.setVisibility(View.GONE);

                            } else {

                                nosalarydetailstxt.setVisibility(View.GONE);
                                salarydetailsrel.setVisibility(View.VISIBLE);

                                CandSalary = CandidateDetails.getJSONArray("CandSalary");
                                System.out.println("JSONARRAY CandSalary=====" + CandSalary);


                                    final String salary_id_str  =  CandSalary.getJSONObject(0).getString("salary_Id");
                                    String current_ctc_str = CandSalary.getJSONObject(0).getString("current_ctc");
                                    String expected_ctc_str = CandSalary.getJSONObject(0).getString("expected_ctc");

                                currentctcdettxt.setText(current_ctc_str);
                                expectedctcdetailtxt.setText(expected_ctc_str);

                                salarydetailsrel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View v) {
                                       Intent i = new Intent(CandidateDetailsActivity.this,EditSalaryActivity.class);
                                       SingletonActivity.salaryid = salary_id_str;
                                       startActivity(i);
                                    }
                                });



                            }


                            //Credentials ==========
                            System.out.println("LENGTH=====" + CandidateDetails.getString("CandCredential").length());
                            if (CandidateDetails.getString("CandCredential").length() == 4)
                            //your rest of codes
                            {

                                nocredentialstxt.setVisibility(View.VISIBLE);
                                credentialslistvw.setVisibility(View.GONE);

                            } else {

                                nocredentialstxt.setVisibility(View.GONE);
                                credentialslistvw.setVisibility(View.VISIBLE);

                                CandCredentials = CandidateDetails.getJSONArray("CandCredential");
                                System.out.println("JSONARRAY CandCredentials=====" + CandCredentials);


                                for(int i = 0; i < CandCredentials.length();i++)
                                {
                                    String credential_id_str = CandCredentials.getJSONObject(i).getString("credential_Id");
                                    credentialidlist.add(credential_id_str);

                                    String credential_name_str = CandCredentials.getJSONObject(i).getString("credential_name");
                                    credentialnamelist.add(credential_name_str);

                                    String credential_description_str = CandCredentials.getJSONObject(i).getString("credential_description");
                                    credentialdesclist.add(credential_description_str);

                                    /*String credential_upload_str = CandCredentials.getJSONObject(i).getString("credential_upload");
                                    credentialuploadlist.add(credential_upload_str);*/


                                }



                                customcredentialsadap = new CustomCredentialsAdap(CandidateDetailsActivity.this,credentialidlist,credentialnamelist,credentialdesclist,credentialuploadlist);
                                credentialslistvw.setAdapter(customcredentialsadap);

                                customcredentialsadap.notifyDataSetChanged();

                                setListViewHeightBasedOnChildren(credentialslistvw);

                            }



                            System.out.println("LENGTH=====" + CandidateDetails.getString("CandProfSummary").length());
                            if (CandidateDetails.getString("CandProfSummary").length() == 4)
                            //your rest of codes
                            {
                                designationtxt.setText("Designation Not Mentioned");
                                noprofsummarytxt.setVisibility(View.VISIBLE);
                                profsummarylistvw.setVisibility(View.GONE);

                            } else {

                                noprofsummarytxt.setVisibility(View.GONE);
                                profsummarylistvw.setVisibility(View.VISIBLE);

                                CandProfSummary = CandidateDetails.getJSONArray("CandProfSummary");
                                System.out.println("JSONARRAY CANDPROFSUMMARY=====" + CandProfSummary);

                                String designationstr = CandProfSummary.getJSONObject(0).getString("latest_desgination");
                                designationtxt.setText(designationstr);

                                for(int i = 0; i < CandProfSummary.length();i++)
                                {


                                    String experienceidstr = CandProfSummary.getJSONObject(i).getString("experience_Id");
                                    experienceidlist.add(experienceidstr);

                                    String companynamestr = CandProfSummary.getJSONObject(i).getString("company_name");
                                    companynamelist.add(companynamestr);

                                    String locationstr = CandProfSummary.getJSONObject(i).getString("location");
                                    locationlist.add(locationstr);

                                    String designationStr = CandProfSummary.getJSONObject(i).getString("latest_desgination");
                                    designationlist.add(designationStr);

                                    String startdatestr = CandProfSummary.getJSONObject(i).getString("start_date");
                                    startdatelist.add(startdatestr);

                                    String enddatestr = CandProfSummary.getJSONObject(i).getString("end_date");
                                    enddatelist.add(enddatestr);

                                    String roledescriptionstr = CandProfSummary.getJSONObject(i).getString("role_description");
                                    roledescriptionlist.add(roledescriptionstr);
                                }



                                customprofsummaryadap = new CustomProfSummaryAdap(CandidateDetailsActivity.this,experienceidlist,companynamelist,locationlist,designationlist,startdatelist,enddatelist,roledescriptionlist);
                                profsummarylistvw.setAdapter(customprofsummaryadap);

                                customprofsummaryadap.notifyDataSetChanged();

                                setListViewHeightBasedOnChildren(profsummarylistvw);

                            }

                            if (CandidateDetails.getString("CandAcademic").length() == 4)
                            //your rest of codes
                            {
                                noacadqualitxt.setVisibility(View.VISIBLE);
                                acadqualilistvw.setVisibility(View.GONE);

                            } else {


                                noacadqualitxt.setVisibility(View.GONE);
                                acadqualilistvw.setVisibility(View.VISIBLE);


                                CandAcademicSummary = CandidateDetails.getJSONArray("CandAcademic");
                                System.out.println("JSONARRAY CANDACADEMIC-------"+CandAcademicSummary);

                               for(int i = 0; i < CandAcademicSummary.length();i++)
                               {

                                   String academic_id = CandAcademicSummary.getJSONObject(i).getString("academic_Id");
                                   academicidlist.add(academic_id);


                                   String academicnamestr = CandAcademicSummary.getJSONObject(i).getString("academic_name");
                                   academicnamelist.add(academicnamestr);

                                   String yearstr = CandAcademicSummary.getJSONObject(i).getString("year_of_passing");
                                   yearlist.add(yearstr);

                                   String aggregatestr = CandAcademicSummary.getJSONObject(i).getString("academic_aggregate");
                                   aggregatelist.add(aggregatestr);

                                   String institutestr = CandAcademicSummary.getJSONObject(i).getString("academic_institute");
                                   institutelist.add(institutestr);

                                   String universitystr = CandAcademicSummary.getJSONObject(i).getString("academic_university");
                                   universitylist.add(universitystr);
                               }
                                System.out.println("CANDACADEMIC academicnamelist list-------"+yearlist);

                                customadap = new CustomAdap(CandidateDetailsActivity.this,academicidlist,academicnamelist,yearlist,aggregatelist,institutelist,universitylist);
                                acadqualilistvw.setAdapter(customadap);
                                setListViewHeightBasedOnChildren(acadqualilistvw);



                            }


                            if (CandidateDetails.getString("CandKeySkill").length() == 4)
                            //your rest of codes
                            {
                                nokeyskilltxt.setVisibility(View.VISIBLE);
                                keyskilllistvw.setVisibility(View.GONE);

                            } else {


                                nokeyskilltxt.setVisibility(View.GONE);
                                keyskilllistvw.setVisibility(View.VISIBLE);


                                CandKeySkillSummary = CandidateDetails.getJSONArray("CandKeySkill");
                                System.out.println("JSONARRAY CANDKEYSKILL-------"+CandKeySkillSummary);

                                for(int i = 0; i < CandKeySkillSummary.length();i++)
                                {

                                    String candidate_skill_id = CandKeySkillSummary.getJSONObject(i).getString("candidate_skill_Id");
                                    candidateskillidlist.add(candidate_skill_id);

                                    String skill_id = CandKeySkillSummary.getJSONObject(i).getString("skill_Id");
                                     skillidlist.add(skill_id);



                                    String skillnamestr = CandKeySkillSummary.getJSONObject(i).getString("skill_name");
                                    skillnamelist.add(skillnamestr);

                                    String skilldetailstr = CandKeySkillSummary.getJSONObject(i).getString("skill_detail");
                                    skilldetaillist.add(skilldetailstr);

                                    String proficienylevelstr = CandKeySkillSummary.getJSONObject(i).getString("proficiency_level");
                                    proficiencylevellist.add(proficienylevelstr);


                                }


                                customkeyskilladap = new CustomKeySkillAdap(CandidateDetailsActivity.this,skillidlist,candidateskillidlist,skillnamelist,skilldetaillist,proficiencylevellist);
                                keyskilllistvw.setAdapter(customkeyskilladap);
                                setListViewHeightBasedOnChildren(keyskilllistvw);



                            }

                            if (CandidateDetails.getString("CandCertification").length() == 4)
                            //your rest of codes
                            {
                                noprofcoursetxt.setVisibility(View.VISIBLE);
                                profcourselistvw.setVisibility(View.GONE);

                            } else {


                                noprofcoursetxt.setVisibility(View.GONE);
                                profcourselistvw.setVisibility(View.VISIBLE);


                                CandCertificationSummary = CandidateDetails.getJSONArray("CandCertification");
                                System.out.println("JSONARRAY CANDIDATE PROFESSIONAL COURSE(CERTIFICATION SUMMARY)-------"+CandCertificationSummary);

                                for(int i = 0; i < CandCertificationSummary.length();i++)
                                {
                                    String certificationidstr = CandCertificationSummary.getJSONObject(i).getString("certification_Id");
                                    certificationidlist.add(certificationidstr);


                                    String certificationnamestr = CandCertificationSummary.getJSONObject(i).getString("certification_name");
                                    certificationnamelist.add(certificationnamestr);

                                    String certificationyearstr = CandCertificationSummary.getJSONObject(i).getString("year");
                                    certificationyearlist.add(certificationyearstr);

                                    String certificationinstitutestr = CandCertificationSummary.getJSONObject(i).getString("certification_institute");
                                    certificationinstitutelist.add(certificationinstitutestr);

                                    String certificationlocationstr = CandCertificationSummary.getJSONObject(i).getString("location");
                                    certificationlocationlist.add(certificationlocationstr);


                                }


                                customprofcourseadap = new CustomProfCourseAdap(CandidateDetailsActivity.this,certificationidlist,certificationnamelist,certificationyearlist,certificationinstitutelist,certificationlocationlist);
                                profcourselistvw.setAdapter(customprofcourseadap);
                                setListViewHeightBasedOnChildren(profcourselistvw);



                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(CandidateDetailsActivity.this);
        requestQueue.add(stringRequest);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, CoordinatorLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void setListViewHeightBasedOnChildren1(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, CoordinatorLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

/*

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent i = new Intent(CandidateDetailsActivity.this,LoginActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/

   /* @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.candidate_profile) {

            Intent i = new Intent(CandidateDetailsActivity.this,DashboardActivity.class);
            startActivity(i);

            // Handle the camera action
        } else if (id == R.id.all_jobs) {

            Intent i = new Intent(CandidateDetailsActivity.this,AllJobsActivity.class);
            startActivity(i);

        } else if (id == R.id.referral) {

        } else if (id == R.id.my_account) {

            Intent i = new Intent(CandidateDetailsActivity.this,MyAccountActivity.class);
            startActivity(i);

        } else if (id == R.id.change_password) {

            Intent i = new Intent(CandidateDetailsActivity.this,ChangePasswordActivity.class);
            startActivity(i);

        } else if (id == R.id.logout) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        ArrayList<String> academicnamelists;
        ArrayList<String> yearlists;
        ArrayList<String> aggregatelists;
        ArrayList<String> institutelists;
        ArrayList<String> universitylists;
        ArrayList<String> academicidlists;

        // public CustomAdap(Context mainActivity)

        private CustomAdap(Context mainActivity,ArrayList<String> academicidlist,ArrayList<String> academicnamels,ArrayList<String> yearls,ArrayList<String> aggregatels,ArrayList<String> institutels,ArrayList<String> universityls) {
            // TODO Auto-generated constructor stub

            this.c = mainActivity;
            this.academicnamelists = academicnamels;
            this.yearlists = yearls;
            this.aggregatelists = aggregatels;
            this.institutelists = institutels;
            this.universitylists = universityls;
            this.academicidlists = academicidlist;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return academicnamelists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub


            Holder holder = new Holder();


            View rowView;


            rowView = inflater.inflate(R.layout.academic_qualification_row, null);


            holder.academicdet1txt = (TextView) rowView.findViewById(R.id.academicdet1txt);
            holder.yeardet1txt = (TextView) rowView.findViewById(R.id.yeardet1txt);
            holder.scoredet1txt = (TextView) rowView.findViewById(R.id.scoredet1txt);
            holder.institutedet1txt = (TextView) rowView.findViewById(R.id.institutedet1txt);
            holder.universitydet1txt = (TextView) rowView.findViewById(R.id.universitydet1txt);

            holder.academicdet1txt.setTypeface(roboto_light_font);
            holder.yeardet1txt.setTypeface(roboto_light_font);
            holder.scoredet1txt.setTypeface(roboto_light_font);
            holder.institutedet1txt.setTypeface(roboto_light_font);
            holder.universitydet1txt.setTypeface(roboto_light_font);

            holder.academicdet1txt.setText(academicnamelists.get(position));
            holder.yeardet1txt.setText(yearlists.get(position));
            holder.scoredet1txt.setText(aggregatelists.get(position));
            holder.institutedet1txt.setText(institutelists.get(position));
            holder.universitydet1txt.setText(universitylists.get(position));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    //Toast.makeText(CandidateDetailsActivity.this,"INDEX==="+ position,Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(CandidateDetailsActivity.this,EditAcademicQualificationsActivity.class);
                    SingletonActivity.acadqualifpos = position;
                    SingletonActivity.academicid = academicidlists.get(position);

                    startActivity(i);

                }
            });


            return rowView;

        }
    }

    public class Holder {
        TextView academicdet1txt,yeardet1txt,scoredet1txt,institutedet1txt,universitydet1txt;

    }

    private class CustomKeySkillAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        ArrayList<String> skillnamelists;
        ArrayList<String> skilldetaillists;
        ArrayList<String> proficiencylevellists;
        ArrayList<String> candidateskillidlists;
        ArrayList<String> skillidlists;



        private CustomKeySkillAdap(Context mainActivity,ArrayList<String> skillidlist,ArrayList<String> candidateskillidlist,ArrayList<String> skillnamels,ArrayList<String> skilldetaills,ArrayList<String> proficiencylevells) {
            // TODO Auto-generated constructor stub

            this.c = mainActivity;
            this.skillnamelists = skillnamels;
            this.skilldetaillists = skilldetaills;
            this.proficiencylevellists = proficiencylevells;
            this.candidateskillidlists = candidateskillidlist;
            this.skillidlists = skillidlist;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return skillnamelists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub


            KeySkillHolder keyskillholder = new KeySkillHolder();


            View rowView;


            rowView = inflater.inflate(R.layout.key_skill_row, null);


            keyskillholder.skillnametxt = (TextView) rowView.findViewById(R.id.skillnametxt);
            keyskillholder.skilldetailtxt = (TextView) rowView.findViewById(R.id.skilldetailtxt);
            keyskillholder.proficiencyleveltxt = (TextView) rowView.findViewById(R.id.proficiencyleveltxt);

            keyskillholder.skillnametxt.setTypeface(roboto_light_font);
            keyskillholder.skilldetailtxt.setTypeface(roboto_light_font);
            keyskillholder.proficiencyleveltxt.setTypeface(roboto_light_font);

            System.out.println("proficiencylevellists ARE---"+ proficiencylevellists.get(position));

           // Intermediate,Advanced,Beginner

           // keyskillholder.skillnametxt.setText(skillnamelists.get(position));


            if(proficiencylevellists.get(position).equals("Intermediate"))
            {
                keyskillholder.skillnametxt.setText(skillnamelists.get(position)+" - (Inter.)");
            }
            if(proficiencylevellists.get(position).equals("Advanced"))
            {
                keyskillholder.skillnametxt.setText(skillnamelists.get(position)+" - (Advnc.)");
            }
            if(proficiencylevellists.get(position).equals("Beginner"))
            {
                keyskillholder.skillnametxt.setText(skillnamelists.get(position)+" - (Begnr.)");
            }


           // keyskillholder.skilldetailtxt.setText(skilldetaillists.get(position));
           // keyskillholder.proficiencyleveltxt.setText(proficiencylevellists.get(position));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    //Toast.makeText(CandidateDetailsActivity.this,"INDEX==="+ position,Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(CandidateDetailsActivity.this,EditKeySkillActivity.class);
                    SingletonActivity.keyskillpos = position;
                    SingletonActivity.candidateskillid = candidateskillidlists.get(position);
                    SingletonActivity.skillid = skillidlists.get(position);


                    startActivity(i);

                }
            });


            return rowView;

        }
    }

    public class KeySkillHolder {
        TextView skillnametxt,skilldetailtxt,proficiencyleveltxt;

    }

    private class CustomProfCourseAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        ArrayList<String> certificationnamelists;
        ArrayList<String> certificationyearlists;
        ArrayList<String> certificationinstitutelists;
        ArrayList<String> certificationlocationlists;
        ArrayList<String> certificationidlists;


        private CustomProfCourseAdap(Context mainActivity,ArrayList<String> certificationidlist,ArrayList<String> certificationnamels,ArrayList<String> certificationyearls,ArrayList<String> certificationinstitutels,ArrayList<String> certificationlocationls) {
            // TODO Auto-generated constructor stub

            this.c = mainActivity;
            this.certificationidlists = certificationidlist;
            this.certificationnamelists = certificationnamels;
            this.certificationyearlists = certificationyearls;
            this.certificationinstitutelists = certificationinstitutels;
            this.certificationlocationlists = certificationlocationls;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return certificationnamelists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub


            ProfCourseHolder profcourseholder = new ProfCourseHolder();


            View rowView;


            rowView = inflater.inflate(R.layout.prof_course_row, null);


            profcourseholder.coursetxt = (TextView) rowView.findViewById(R.id.coursetxt);
            profcourseholder.yeartxt = (TextView) rowView.findViewById(R.id.yeartxt);
            profcourseholder.institutetxt = (TextView) rowView.findViewById(R.id.institutetxt);
            profcourseholder.locationtxt = (TextView) rowView.findViewById(R.id.locationtxt);

            profcourseholder.coursetxt.setTypeface(roboto_light_font);
            profcourseholder.yeartxt.setTypeface(roboto_light_font);
            profcourseholder.institutetxt.setTypeface(roboto_light_font);
            profcourseholder.locationtxt.setTypeface(roboto_light_font);


            profcourseholder.coursetxt.setText(certificationnamelists.get(position));
            profcourseholder.yeartxt.setText(certificationyearlists.get(position));
            profcourseholder.institutetxt.setText(certificationinstitutelists.get(position));
            profcourseholder.locationtxt.setText(certificationlocationlists.get(position));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    Intent i = new Intent(CandidateDetailsActivity.this,EditProfessionalCourseActivity.class);
                    SingletonActivity.profcoursepos = position;
                    SingletonActivity.certificationid = certificationidlists.get(position);

                    startActivity(i);



                }
            });


            return rowView;

        }
    }

    public class ProfCourseHolder {
        TextView coursetxt,yeartxt,institutetxt,locationtxt;

    }

    private class CustomProfSummaryAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private ArrayList<String> companynamelists;
        private ArrayList<String> locationlists;
        private ArrayList<String> designationlists;
        private ArrayList<String> startdatelists;
        private ArrayList<String> enddatelists;
        private ArrayList<String> roledescriptionlists;
        private ArrayList<String> experienceidlists;

        private CustomProfSummaryAdap(Context mainActivity,ArrayList<String> experienceidls,ArrayList<String> companynamels,ArrayList<String> locationls,ArrayList<String> designationls,ArrayList<String> startdatels,ArrayList<String> enddatels,ArrayList<String> roledescriptionls) {
            // TODO Auto-generated constructor stub

            this.c = mainActivity;
            this.companynamelists = companynamels;
            this.locationlists = locationls;
            this.designationlists = designationls;
            this.startdatelists = startdatels;
            this.enddatelists = enddatels;
            this.roledescriptionlists = roledescriptionls;
            this.experienceidlists = experienceidls;


            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return companynamelists.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub

            ProfSummaryHolder profsummaryholder = new ProfSummaryHolder();

            View rowView;
            rowView = inflater.inflate(R.layout.prof_summary_row, parent, false);

            profsummaryholder.companynamedesctxt = (TextView) rowView.findViewById(R.id.companynamedesctxt);
            profsummaryholder.locationdesctxt = (TextView) rowView.findViewById(R.id.locationdesctxt);
            profsummaryholder.designationtxt = (TextView) rowView.findViewById(R.id.designationtxt);
            profsummaryholder.roledescriptiontxt = (TextView) rowView.findViewById(R.id.roledescriptiontxt);

            profsummaryholder.companynamedesctxt.setTypeface(roboto_light_font);
            profsummaryholder.locationdesctxt.setTypeface(roboto_light_font);
            profsummaryholder.designationtxt.setTypeface(roboto_light_font);
            profsummaryholder.roledescriptiontxt.setTypeface(roboto_light_font);

            profsummaryholder.companynamedesctxt.setText(companynamelists.get(position));
            profsummaryholder.locationdesctxt.setText(locationlists.get(position));

            System.out.println("END DATE LENGTH===="+ enddatelists.get(position).length());

            if(enddatelists.get(position).length() == 4) {

                profsummaryholder.designationtxt.setText(designationlists.get(position) + "   (" + startdatelists.get(position) + "-" + "Present" + ")");
            }
            else
            {
                profsummaryholder.designationtxt.setText(designationlists.get(position) + "   (" + startdatelists.get(position) + "-" + enddatelists.get(position) + ")");
            }

            String roledesc = roledescriptionlists.get(position);
            roledesc = roledesc.replace("\n", "<br>");
            profsummaryholder.roledescriptiontxt.setText(Html.fromHtml(roledesc));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    //Toast.makeText(CandidateDetailsActivity.this,"PROF SUMMARY ROW",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(CandidateDetailsActivity.this,EditProfessionalSummaryActivity.class);
                    SingletonActivity.profsummarypos = position;
                    SingletonActivity.experienceid = experienceidlists.get(position);

                    startActivity(i);

                }
            });

            return rowView;

        }
    }

    public class ProfSummaryHolder {
        TextView companynamedesctxt,locationdesctxt,designationtxt,roledescriptiontxt;

    }
    private class CustomCredentialsAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private ArrayList<String> credentialnamelist;
        private ArrayList<String> credentialdesclist;
        private ArrayList<String> credentialuploadlists;
        private ArrayList<String> credentialidlists;




        private CustomCredentialsAdap(Context mainActivity,ArrayList<String> credentialidlist,ArrayList<String> credentialnamelist,ArrayList<String> credentialdesclist,ArrayList<String> credentialuploadlist) {
            // TODO Auto-generated constructor stub

            this.c = mainActivity;
            this.credentialnamelist = credentialnamelist;
            this.credentialdesclist = credentialdesclist;
            this.credentialuploadlists = credentialuploadlist;
            this.credentialidlists = credentialidlist;



            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return credentialnamelist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub

            CredentialHolder credentialholder = new CredentialHolder();

            View rowView;
            rowView = inflater.inflate(R.layout.credentials_row, parent, false);

            credentialholder.credentialtitletxt = (TextView) rowView.findViewById(R.id.credentialtitletxt);
            credentialholder.credentialdesctxt = (TextView) rowView.findViewById(R.id.credentialdesctxt);


            credentialholder.credentialtitletxt.setTypeface(roboto_light_font);
            credentialholder.credentialdesctxt.setTypeface(roboto_light_font);


            credentialholder.credentialtitletxt.setText(credentialnamelist.get(position));
            credentialholder.credentialdesctxt.setText(credentialdesclist.get(position));

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    Intent i = new Intent(CandidateDetailsActivity.this,EditCredentialActivity.class);
                    SingletonActivity.credentialpos = position;
                    SingletonActivity.credentialid = credentialidlists.get(position);
                    startActivity(i);


                }
            });

            return rowView;

        }
    }

    public class CredentialHolder {
        TextView credentialtitletxt,credentialdesctxt;

    }



}
