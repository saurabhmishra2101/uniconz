package com.smtc.uniconz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 10161 on 4/11/2018.
 */

public class ReferralActivity extends AppCompatActivity {

    TextView newreferraltext,rejectedcountdesctxt,rejectedtxt,inprogresscountdesctxt,inprogresstxt,referralrequesttxtvw,approvedtitletxt,inprogresstitletxt,placedtitletxt;
    TextView referencestxtvw,firstleveltxt,firstlevelapprovedtxt,firstlevelinprogresstxt,firstlevelplacedtxt,secondleveltxt,secondlevelapprovedtxt,secondlevelinprogresstxt,secondlevelplacedtxt;
    ImageView backiv;
    Typeface bebas_font,roboto_light_font,sf_font;
    ProgressDialog pdia;
    CoordinatorLayout coordinatorLayout;
    String candidateidstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    RelativeLayout referralrqstrelative,referencerelative,newreferralrelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_coordinator);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        candidateidstr = prefs.getString("candidate_id", null);

        newreferralrelative = (RelativeLayout)findViewById(R.id.newreferralrelative);
        referencerelative = (RelativeLayout)findViewById(R.id.referencerelative);
        referralrqstrelative = (RelativeLayout)findViewById(R.id.referralrqstrelative);
        newreferraltext = (TextView)findViewById(R.id.newreferraltext);
        rejectedcountdesctxt = (TextView)findViewById(R.id.rejectedcountdesctxt);
        rejectedtxt = (TextView)findViewById(R.id.rejectedtxt);
        inprogresscountdesctxt = (TextView)findViewById(R.id.inprogresscountdesctxt);
        inprogresstxt = (TextView)findViewById(R.id.inprogresstxt);
        referralrequesttxtvw = (TextView)findViewById(R.id.referralrequesttxtvw);
        approvedtitletxt= (TextView)findViewById(R.id.approvedtitletxt);
        inprogresstitletxt= (TextView)findViewById(R.id.inprogresstitletxt);
        placedtitletxt= (TextView)findViewById(R.id.placedtitletxt);
        firstleveltxt= (TextView)findViewById(R.id.firstleveltxt);
        firstlevelapprovedtxt= (TextView)findViewById(R.id.firstlevelapprovedtxt);
        firstlevelinprogresstxt= (TextView)findViewById(R.id.firstlevelinprogresstxt);
        firstlevelplacedtxt= (TextView)findViewById(R.id.firstlevelplacedtxt);
        secondleveltxt= (TextView)findViewById(R.id.secondleveltxt);
        secondlevelapprovedtxt= (TextView)findViewById(R.id.secondlevelapprovedtxt);
        secondlevelinprogresstxt =  (TextView)findViewById(R.id.secondlevelinprogresstxt);
        secondlevelplacedtxt =  (TextView)findViewById(R.id.secondlevelplacedtxt);
        backiv = (ImageView)findViewById(R.id.backiv);
        referencestxtvw = (TextView)findViewById(R.id.referencestxtvw);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReferralActivity.this,DashboardActivity.class);
                startActivity(i);

                //  finish();
            }
        });

        newreferralrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReferralActivity.this,NewReferralActivity.class);
                startActivity(i);
            }
        });

        referencerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReferralActivity.this,FirstLevelReferenceActivity.class);
                startActivity(i);
            }
        });

        referralrqstrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReferralActivity.this,ReferralRequestActivity.class);
                startActivity(i);
            }
        });

        bebas_font = Typeface.createFromAsset(getAssets(), "bebas/BEBAS.ttf");
        roboto_light_font = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        sf_font = Typeface.createFromAsset(getAssets(), "sfuidisplay/SF.ttf");

        newreferraltext.setTypeface(roboto_light_font);
        rejectedcountdesctxt.setTypeface(bebas_font);
        rejectedtxt.setTypeface(sf_font);
        inprogresscountdesctxt.setTypeface(bebas_font);
        inprogresstxt.setTypeface(sf_font);
        referralrequesttxtvw.setTypeface(bebas_font);
        approvedtitletxt.setTypeface(sf_font);
        inprogresstitletxt.setTypeface(sf_font);
        placedtitletxt.setTypeface(sf_font);
        firstleveltxt.setTypeface(bebas_font);
        firstlevelapprovedtxt.setTypeface(bebas_font);
        firstlevelinprogresstxt.setTypeface(bebas_font);
        firstlevelplacedtxt.setTypeface(bebas_font);
        secondleveltxt.setTypeface(bebas_font);
        secondlevelapprovedtxt.setTypeface(bebas_font);
        secondlevelinprogresstxt.setTypeface(bebas_font);
        secondlevelplacedtxt.setTypeface(bebas_font);
        referencestxtvw.setTypeface(bebas_font);

        if (NetworkUtility.checkConnectivity(ReferralActivity.this)) {
            String getCandChildRefForMobileCountsURL = APIName.URL + "/candidate/getCandChildRefForMobileCounts?candidate_Id=" + candidateidstr;
            System.out.println("GET CAND CHILD REF FOR MOBILE COUNTS URL IS---" + getCandChildRefForMobileCountsURL);
            getCandChildRefForMobileCountsAPI(getCandChildRefForMobileCountsURL);

        } else {
            // util.dialog(LoginActivity.this, "Please check your internet connection.");

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "No Internet Connection!!!", Snackbar.LENGTH_LONG);

            View view = snackbar.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(Color.RED);
            snackbar.show();
        }

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ReferralActivity.this,DashboardActivity.class);
        startActivity(i);
    }

    private void getCandChildRefForMobileCountsAPI(String url) {

        pdia = new ProgressDialog(ReferralActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        System.out.println("RESPONSE OF getCandChildRefForMobileCountsAPI IS---" + response);

                        JSONObject getCandChildRefForMobileCountsAPIjson = null;
                        try {
                            getCandChildRefForMobileCountsAPIjson = new JSONObject(response);


                            rejectedcountdesctxt.setText(getCandChildRefForMobileCountsAPIjson.getString("ReferralRejected"));
                            inprogresscountdesctxt.setText(getCandChildRefForMobileCountsAPIjson.getString("ReferralPending"));
                            firstlevelapprovedtxt.setText(getCandChildRefForMobileCountsAPIjson.getString("firstApproved"));
                            firstlevelinprogresstxt.setText(getCandChildRefForMobileCountsAPIjson.getString("firstPending"));
                            firstlevelplacedtxt.setText(getCandChildRefForMobileCountsAPIjson.getString("firstRejected"));
                            secondlevelapprovedtxt.setText(getCandChildRefForMobileCountsAPIjson.getString("secondApproved"));
                            secondlevelinprogresstxt.setText(getCandChildRefForMobileCountsAPIjson.getString("secondPending"));
                            secondlevelplacedtxt.setText(getCandChildRefForMobileCountsAPIjson.getString("secondRejected"));


                        } catch (JSONException e) {
                            e.printStackTrace();

                            pdia.dismiss();

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {


                                        rejectedcountdesctxt.setText("00");
                                        inprogresscountdesctxt.setText("00");
                                        firstlevelapprovedtxt.setText("00");
                                        firstlevelinprogresstxt.setText("00");
                                        firstlevelplacedtxt.setText("00");
                                        secondlevelapprovedtxt.setText("00");
                                        secondlevelinprogresstxt.setText("00");
                                        secondlevelplacedtxt.setText("00");


                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);
                                        Snackbar snackbar = Snackbar
                                                .make(coordinatorLayout, messagestr, Snackbar.LENGTH_LONG);

                                        View view = snackbar.getView();
                                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                        tv.setTextColor(Color.RED);
                                        snackbar.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {


                                //Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, error.getMessage(), Snackbar.LENGTH_LONG);

                                View view = snackbar.getView();
                                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                tv.setTextColor(Color.RED);
                                snackbar.show();
                            }
                        } else {
                            // util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");

                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, "Some Error Occured,Please try after some time", Snackbar.LENGTH_LONG);


                            View view = snackbar.getView();
                            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            tv.setTextColor(Color.RED);
                            snackbar.show();
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                String credentials = "admin:uniconz123";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", auth);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ReferralActivity.this);
        requestQueue.add(stringRequest);
    }

}
